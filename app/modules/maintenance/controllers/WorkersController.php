<?php
class Maintenance_WorkersController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('manage work orders');
	}
	
    public function indexAction()
    {
        $this->view->all_records = MaintenanceWorker::fetchAll();
	}
    
    public function editAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->maintenance_worker->form);
        
        $id = intval($this->_getParam('id'));
        if ($id != 0)
        {
            $record = MaintenanceWorker::find($id);
            
            $record_info = $record->toArray();
            $record_info['active'] = ($record_info['active']) ? 1 : 0;
            $form->setDefaults($record_info);
        }

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (!$record)
                $record = new MaintenanceWorker();
            
            $record->name = $data['name'];
            $record->active = $data['active'];
            $record->save();
            
            $this->alert('Worker changes saved.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        }

        $this->view->form = $form;
    }
	
	public function deleteAction()
	{
		$id = intval($this->_getParam('id'));
        
		$record = MaintenanceWorker::find($id);
		if ($record)
			$record->delete();
		
		$this->alert('Worker deleted.');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}
}