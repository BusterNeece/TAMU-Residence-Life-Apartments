<?php
/**
 * Maintenance: Submit electric meter readings
 */

class Maintenance_CleaningController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed(array('submit cleaning charges', 'review cleaning charges'));
	}
	
    public function indexAction()
    {
        $query = Doctrine_Query::create()
            ->from('CleaningCharge cc')
            ->orderBy('cc.time_assigned DESC');
        
        $pager = new DF_Paginator_Doctrine($query);
        $this->view->pager = $pager;
    }

    public function editAction()
    {
    	\DF\Acl::getInstance()->checkPermission('submit cleaning charges');

    	$id = (int)$this->_getParam('id');
    	$form = new \DF\Form($this->current_module_config->forms->cleaning_charges->form);

    	if ($id != 0)
    	{
    		$record = CleaningCharge::find($id);
    		$form->setDefaults($record->toArray());
    	}

    	if (!empty($_POST) && $form->isValid($_POST))
    	{
    		$data = $form->getValues();

            if (!$record)
            {
                $record = new CleaningCharge();
                $record->status = CleaningCharge::STATUS_PENDING;
            }

    		$record->fromArray($data);
    		$record->save();

            if ($record->status == CleaningCharge::STATUS_PENDING)
            {
    			\DF\Messenger::send(array(
                    'to' => Settings::getSetting('maintenance_cleaning_contact'),
                    'subject' => 'Cleaning Charges Pending Review',
                    'template' => 'maintenance/cleaning',
                    'vars' => array('id' => $record->id),
                ));
            }

    		$this->alert('Record updated!');
    		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    		return;
    	}

    	$this->view->form = $form;

    }

    public function reviewAction()
    {
    	\DF\Acl::getInstance()->checkPermission('review cleaning charges');

    	$id = (int)$this->_getParam('id');
    	$record = CleaningCharge::find($id);

    	if (!($record instanceof CleaningCharge))
    		throw new DF_Exception('Cleaning charge not found.');
        
        if ($this->_hasParam('status'))
        {
            $new_status = $this->_getParam('status', CleaningCharge::STATUS_DECLINED);
            $record->status = $new_status;
            $record->save();
            
            if ($new_status == CleaningCharge::STATUS_APPROVED)
            {
                // Get the total amount of hours logged on the charge.
                $charge_data = $record->charge_data;
                $total_hours = (float)$charge_data['total_hours'];

                if ($total_hours < 1)
                    $total_hours = 1;
                
                $amount_due = $total_hours * CleaningCharge::RATE_PER_HR;

                // Split across occupants.
                $occupants = Occupancy::getActiveByAsset($record->asset_id);

                if ($occupants)
                {
                    $num_occupants = count($occupants);
                    $rate_per_occupant = \DF\Utilities::ceiling($amount_due / $num_occupants, 2);

                    // Post new transaction.
                    $transaction = new RegisterTransaction();
                    $transaction->posted = time();
                    $transaction->date = time();
                    $transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_DAMAGES;
                    $transaction->asset_id = $record->asset_id;
                    $transaction->memo = 'Cleaning Charges';
                    $transaction->save();
                    $transaction_id = $transaction->id;

                    $item_id = Item::fetchByName('Repair/Maintenance');

                    foreach($occupants as $occupant)
                    {
                        // Post new split for this resident.
                        $split = new Split();
                        $split->posted = time();
                        $split->transaction_id = $transaction_id;
                        $split->resident_id = $occupant['resident_id'];
                        $split->ledger_id = Split::LEDGER_STANDARD;
                        $split->credit_or_debit = 'D';
                        $split->item_id = $item_id;
                        $split->asset_id = $record->asset_id;
                        $split->split_amount = 0-$rate_per_occupant;
                        $split->save();
                    }
                }
            }

            $this->alert('Charged reviewed successfully.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'status' => NULL));
            return;
        }

        if ($record->status != CleaningCharge::STATUS_PENDING)
        {
            throw new \DF\Exception\DisplayOnly('This charge has already been reviewed. Its current status is "'.$record->getStatusText().'".');
        }

    	$form = new \DF\Form($this->current_module_config->forms->cleaning_charges->form);
    	$form->setDefaults($record->toArray());

    	$this->view->record = $record;
    	$this->view->form = $form;
    }
}