<?php
class Maintenance_ReportsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('manage work orders');
	}
	
    public function indexAction()
    {}
    
    public function completedbymonthAction()
    {
        $months = array();
        $current_month = date('m', strtotime('now'));
        
        for($i = 11; $i >= 0; $i--)
        {
            $month_start = mktime(0, 0, 0, $current_month-$i, 1);
            $month_end = mktime(0, 0, 0, $current_month-$i+1, 0);
            
            $months[date('Ym', $month_start)] = array(
                'name'      => date('M Y', $month_start),
                'start'     => $month_start,
                'end'       => $month_end,
                'total'     => 0,
                'totals'    => array(),
            );
        }
        
        $months['total'] = array(
            'name'      => 'Total',
            'total'     => 0,
            'totals'    => array(),
        );
        
        $all_categories = MaintenanceCategory::fetchSelect();
        unset($all_categories[0]);
        
        foreach($all_categories as $category)
        {
            foreach($months as $month_code => $month_info)
            {
                $months[$month_code]['totals'][$category] = 0;
            }
        }
        
        foreach($months as $month_code => $month_info)
        {
            if ($month_info['start'])
            {
                $workorders_in_period = Doctrine_Query::create()
                    ->from('MaintenanceWorkorder mwo')
                    ->innerJoin('mwo.Category mc')
                    ->where('mwo.time_done BETWEEN ? AND ?', array($month_info['start'], $month_info['end']))
                    ->andWhere('mwo.status = ?', MaintenanceWorkorder::STATUS_COMPLETED)
                    ->fetchArray();
                
                foreach((array)$workorders_in_period as $workorder)
                {
                    $category = $workorder['Category']['name'];
                    
                    $months[$month_code]['totals'][$category]++;
                    $months[$month_code]['total']++;
                    
                    $months['total']['totals'][$category]++;
                    $months['total']['total']++;
                }
            }
        }
        
        $this->view->categories = $all_categories;
        $this->view->months = $months;
	}

    public function costbybuildingAction()
    {
        if ($this->_hasParam('asset_id'))
        {
            // Load building asset.
            $building_id = (int)$this->_getParam('asset_id');
            $building = Asset::find($building_id);
            $this->view->building = $building;

            $children = $building->getChildren();

            if ($children)
            {
                $apts = array($building->id);
                foreach($children as $asset)
                {
                    $apts[] = $asset->id;
                }
            }

            $workorders_per_month = array();

            for($i = 0; $i >= -12; $i--)
            {
                $month_timestamp = mktime(12, 0, 0, date('m')+$i, 15, date('Y'));
                $start_of_month = mktime(0, 0, 0, date('m')+$i, 1, date('Y'));
                $end_of_month = mktime(23, 59, 59, date('m')+$i+1, 0, date('Y'));

                $date_code = date('Ym', $month_timestamp);

                $workorders_per_month[$date_code] = array(
                    'name'      => date('F Y', $month_timestamp),
                    'start'     => $start_of_month,
                    'end'       => $end_of_month,
                    'items'     => array(),
                    'total'     => 0.0,
                );
            }

            if ($apts)
            {
                foreach($workorders_per_month as $month_code => $month_info)
                {
                    $workorders_in_period = Doctrine_Query::create()
                        ->from('MaintenanceWorkorder mwo')
                        ->leftJoin('mwo.InventoryParts mwoip')
                        ->leftJoin('mwo.Tasks mwot')
                        ->leftJoin('mwot.InventoryParts mwotip')
                        ->where('mwo.time_done BETWEEN ? AND ?', array($month_info['start'], $month_info['end']))
                        ->andWhere('mwo.status = ?', MaintenanceWorkorder::STATUS_COMPLETED)
                        ->andWhere('mwo.asset_id IN ?', array($apts))
                        ->fetchArray();
                    
                    foreach((array)$workorders_in_period as $workorder)
                    {
                        $parts = array();

                        if ($workorder['InventoryParts'])
                        {
                            foreach($workorder['InventoryParts'] as $part)
                            {
                                $parts[] = $part;
                            }
                        }
                        if ($workorder['Tasks'])
                        {
                            foreach($workorder['Tasks'] as $task)
                            {
                                if ($task['InventoryParts'])
                                {
                                    foreach($task['InventoryParts'] as $part)
                                    {
                                        $parts[] = $part;
                                    }
                                }
                            }
                        }

                        if ($parts)
                        {
                            foreach($parts as $part_info)
                            {
                                $workorders_per_month[$month_code]['items'][] = $part_info;
                                $workorders_per_month[$month_code]['total'][] = $part_info['cost'];
                            }
                        }
                    }
                }
            }

            switch($this->_getParam('format', 'html'))
            {
                case "csv":
                    $this->doNotRender();

                    $export_data = array(
                        array($building['name'].' - Parts Cost by Month'),
                        array('Month', 'Parts Used', 'Cost'),
                    );

                    foreach($workorders_per_month as $month_code => $month_info)
                    {
                        $export_data[] = array(
                            $month_info['name'],
                            count($month_info['items']),
                            '$'.number_format($month_info['total'], 2),
                        );
                    }

                    DF_Export::exportToCSV($export_data, TRUE);
                    return;
                break;

                case "html":
                default:
                    $this->view->workorders_per_month = $workorders_per_month;
                    $this->render();
                    return;
                break;
            }
        }
        else
        {
            $complexes_raw = Doctrine_Query::create()
                ->from('Asset a')
                ->where('a.asset_parent_id IS NULL OR a.asset_parent_id = 0')
                ->orderBy('a.name ASC')
                ->fetchArray();

            $complexes = array();
            $all_buildings = array();

            $asset_tree = Asset::fetchSelect(FALSE, TRUE, TRUE, FALSE, FALSE);
            
            foreach($complexes_raw as $complex)
            {
                $complex_name = $complex['name'];
                $parent_asset_children = $asset_tree[$complex_name];
                unset($parent_asset_children[$complex['id']]);

                foreach($parent_asset_children as $building_name => $building_apts)
                {
                    // Single item.
                    if (!is_array($building_apts))
                    {
                        $building_id = (int)$building_name;
                        $building_name = $building_apts;
                    }
                    else
                    {
                        foreach($building_apts as $apt_id => $apt_name)
                        {
                            if ($apt_name == $building_name)
                                $building_id = $apt_id;
                        }
                    }

                    $complexes[$complex_name][$building_id] = $building_name;
                    $all_buildings[$building_id] = $building_name;
                }
            }
            
            $this->view->complexes = $complexes;
            $this->render('costbybuilding_select');
            return;
        }   
    }
}