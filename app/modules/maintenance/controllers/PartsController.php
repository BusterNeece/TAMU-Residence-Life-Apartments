<?php
class Maintenance_PartsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('manage work orders');
	}
	
    public function indexAction()
    {
        $sortby = ($this->_getParam('sortby')) ? $this->_getParam('sortby') : null;
        $page = ($this->_getParam('page')) ? $this->_getParam('page') : 1;
        $sortdir = ($this->_getParam('sortdir')) ? $this->_getParam('sortdir') : 'asc';
        $perpage = ($this->_getParam('perpage')) ? $this->_getParam('perpage') : 25;
        if ($perpage == 'all') $perpage = -1;

        if ($this->_hasParam('searchterms'))
        {
            $this->view->searchterms = $search_terms = $this->_getParam('searchterms');
            $this->view->append_to = '?searchterms='.$search_terms;
            $this->view->parts = MaintenanceInventoryPart::search($search_terms, $page, $perpage, $sortby, $sortdir);
        }
        else
        {
            $this->view->parts = MaintenanceInventoryPart::fetchAll($page, $perpage, $sortby, $sortdir);
        }
    }
    
    public function editAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->maintenance_part->form);
        
        $id = intval($this->_getParam('id'));
        if ($id != 0)
        {
            $record = MaintenanceInventoryPart::find($id);
            $form->setDefaults($record->toArray());
        }

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (!$record)
                $record = new MaintenanceInventoryPart();
                
            
            $record->name = $data['name'];
            $record->cost = $data['cost'];
            $record->date_added = time();
            $record->part_number = $data['part_number'];
            
            $record->save();
            
            $this->alert('Changes saved.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        }
		
        $this->view->form = $form;
    }
	
    public function deleteAction()
    {
        $this->doNotRender();
        \DF\Csrf::validateToken($this->_getParam('csrf'));

        $id = intval($this->_getParam('id'));

        $record = MaintenanceInventoryPart::find($id);

        try
        {
            $record->delete();
            $this->alert('Part deleted.');
        }
        catch(Doctrine_Exception $e)
        {
            $this->alert($e->getMessage());
        }

        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
    }
}