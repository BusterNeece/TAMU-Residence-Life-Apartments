<?php
/**
 * Maintenance: Submit electric meter readings
 */

class Maintenance_ElectricController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('manage electric meter readings');
	}
	
    public function indexAction()
    {
        if ($this->_hasParam('group_id') && $this->_hasParam('timestamp'))
        {
            // Get all assets within the given asset category.
            $timestamp = (int)$this->_getParam('timestamp');
            $this->view->timestamp = $timestamp;

            $asset_location = (int)$this->_getParam('group_id');
            
            $location = AssetLocation::find($asset_location);
            $this->view->group_name = $location->name;
            
            $assets = Doctrine_Query::create()
                ->from('Asset a')
                ->leftJoin('a.AssetType at')
                ->leftJoin('a.ElectricProvider ep')
                ->whereNotIn('at.name', array('Building', 'Complex'))
                ->addWhere('a.asset_location_id = ?', $asset_location)
                ->orderBy('a.meter_sort_order ASC')
                ->fetchArray();
            
            if ($assets)
            {
                // Handle submission.
                if ($this->_hasParam('reading'))
                {
                    // Process new records.
                    foreach($assets as $asset)
                    {
                        $existing_reading = Doctrine_Query::create()
                            ->from('Electric')
                            ->where('asset_id = ?', array($asset['id']))
                            ->addWhere('meter_date = ?', array($timestamp))
                            ->fetchOne();
                        
                        if ($existing_reading instanceof Electric)
                        {
                            $is_billed = (int)$existing_reading->is_billed;
                            $existing_reading->delete();
                        }
                        else
                        {
                            $is_billed = 0;
                        }

                        $reading = (int)$_POST['reading'][$asset['id']];
                        if ($reading != 0)
                        {
                            // Create a new record.
                            $record = new Electric();
                            $record->is_billed = $is_billed;
                            $record->meter_reading = $reading;
                            $record->meter_month = date('Ym', $timestamp);
                            $record->meter_date = $timestamp;
                            $record->reading_type = 'monthly';
                            $record->asset_id = $asset['id'];
                            $record->save();
                        }
                    }
                    
                    $this->alert('Meter readings saved!');
                    $this->redirectFromHere(array('timestamp' => NULL));
                }
                else
                {
                    foreach($assets as &$asset)
                    {
                        $asset_id = $asset['id'];
                        $asset['current'] = Electric::fetchByAssetAndTimestamp($asset_id, $timestamp);
                        $asset['previous'] = Electric::fetchPrevious($asset['current']);
                    }
                    
                    $this->view->assets = $assets;
                }
            }
			else
			{
				throw new \DF\Exception\DisplayOnly('No assets could be found underneath this asset location.');
			}
        }
        else if ($this->_hasParam('group_id'))
        {
            if ($this->_hasParam('timestamp_raw'))
            {
                $timestamp = (int)strtotime($this->_getParam('timestamp_raw').' 12:00:00');
                $this->redirectFromHere(array('timestamp' => $timestamp));
                return;
            }
            
            $group_id = (int)$this->_getParam('group_id');
            $location = AssetLocation::find($group_id);
            $this->view->group_name = $location->name;
            
            $existing_batches_raw = Doctrine_Query::create()
                ->select('e.*')
                ->from('Electric e')
                ->innerJoin('e.Asset a')
                ->innerJoin('a.AssetLocation al')
                ->where('al.id = ?', array($group_id))
                ->andWhere('e.reading_type = ?', array('monthly'))
                ->orderBy('e.meter_date DESC')
                ->fetchArray();
            
            $existing_batches = array();
            foreach((array)$existing_batches_raw as $batch)
            {
                $existing_batches[$batch['meter_date']] = date('F j, Y', $batch['meter_date']);
            }
            
            $this->view->existing_batches = $existing_batches;
            $this->render('index_select_batch');
        }
        else
        {
            // Compose a list of groups and months to choose from.
            $this->view->group_options = AssetLocation::fetchSelect();
            
            $current_timestamp = mktime(12, 0, 0, date('m')+1, 15);
            $this->view->current_month = date('Ym', $current_timestamp);
            
            $this->render('index_select');
        }
	}

    public function specialAction()
    {
        if ($this->_hasParam('asset_id'))
        {
            $asset_id = (int)$this->_getParam('asset_id');
            $asset = Asset::find($asset_id);

            $this->view->asset = $asset;

            if ($this->_hasParam('submitted'))
            {
                $timestamp = strtotime($_REQUEST['meter_date'].' 12:00:00');

                // Create a new record.
                $record = new Electric();
                $record->meter_reading = (int)$_REQUEST['meter_reading'];
                $record->meter_month = date('Ym', $timestamp);
                $record->meter_date = $timestamp;
                $record->reading_type = $_REQUEST['reading_type'];
                $record->asset_id = $asset_id;
                $record->save();

                // Immediately post charges for this apartment.
                UA_ElectricManager::generateCharges($asset_id, FALSE);
                
                $this->redirectFromHere(array('submitted' => NULL));
                return;
            }

            $existing_electric = Doctrine_Query::create()
                ->select('e.*')
                ->from('Electric e')
                ->where('e.asset_id = ?', array($asset_id))
                ->andWhere('e.meter_reading != ?', array(0))
                ->orderBy('e.meter_date DESC')
                ->fetchArray();
            
            if ($existing_electric)
            {
                $rate = $asset->ElectricProvider->cost;

                foreach($existing_electric as $offset => &$row)
                {
                    if ($offset == count($existing_electric) - 1)
                    {
                        $row['usage_prev'] = 'N/A';
                        $row['usage_diff'] = 'N/A';
                        $row['usage_cost'] = 'N/A';
                    }
                    else
                    {
                        $a_row = $existing_electric[$offset+1];
                        $row['usage_prev'] = $a_row['meter_reading'] % 10000;

                        $b_usage = $row['meter_reading'] % 10000;

                        $row['usage_diff'] = $b_usage - $row['usage_prev'];

                        if ($row['usage_diff'] < 0 && $row['usage_prev'] > 9000)
                            $row['usage_diff'] = (10000 - $row['usage_prev']) + $b_usage;

                        $cost = $row['usage_diff'] * $rate;
                        $row['usage_cost'] = '$'.number_format($cost, 2);
                    }
                }
            }
            
            $this->view->readings = $existing_electric;
            $this->render();
        }
        else
        {
            $this->view->assets = Asset::fetchSelect();

            $this->render('special_select');
            return;
        }
    }

    public function deletespecialAction()
    {
        $asset_id = (int)$this->_getParam('asset_id');
        $id = (int)$this->_getParam('id');

        $electric = Doctrine_Query::create()
            ->from('Electric e')
            ->where('e.asset_id = ?', array($asset_id))
            ->andWhere('e.id = ?', array($id))
            ->fetchOne();
        
        if ($electric instanceof Electric)
            $electric->delete();
        
        $this->alert('<b>Special electric entry deleted.</b>', 'green');
        $this->redirectFromHere(array('action' => 'special', 'id' => NULL));
        return;
    }
    
    public function historyAction()
    {
        if ($this->_hasParam('group_id'))
        {
            // Get all assets within the given asset category.
            $asset_location = (int)$this->_getParam('group_id');
            
            $location = AssetLocation::find($asset_location);
            $this->view->group_name = $location->name;
            
            $assets = Doctrine_Query::create()
                ->from('Asset a')
                ->leftJoin('a.AssetType at')
                ->leftJoin('a.ElectricProvider ep')
                ->whereNotIn('at.name', array('Building', 'Complex'))
                ->addWhere('a.asset_location_id = ?', $asset_location)
                ->orderBy('a.meter_sort_order ASC')
                ->fetchArray();
            
            if (!$assets)
                throw new \DF\Exception\DisplayOnly('No assets could be found underneath this asset location.');
            
            $months = array();
            for($i = -12; $i <= 0; $i++)
            {
                $month_timestamp = mktime(12, 0, 0, date('m')+$i, 15);
                $month = date('Ym', $month_timestamp);
                
                $months[$month] = array(
                    'timestamp'     => $month_timestamp,
                    'name'          => date('M', $month_timestamp).'&nbsp;'.date('Y', $month_timestamp),
                    'items'         => array(),
                );
            }
            
            foreach($assets as $asset)
            {
                foreach($months as $month_code => $month_info)
                {
                    $asset_id = $asset['id'];
                    $current = Electric::fetchByAssetAndMonth($asset_id, $month_code);
                    $months[$month_code]['items'][$asset_id] = (int)$current['meter_reading'];
                }
            }
            
            switch($this->_getParam('format', 'html'))
            {
                case "csv":
                    $this->doNotRender();
                    
                    $export_data = array();
                    
                    $header_row = array('Apartment');
                    foreach($months as $month_code => $month_info)
                    {
                        $header_row[] = date('M Y', $month_info['timestamp']);
                    }
                    $export_data[] = $header_row;
                    
                    foreach($assets as $asset)
                    {
                        $export_row = array($asset['name']);
                        foreach($months as $month_code => $month_info)
                        {
                            $export_row[] = $month_info['items'][$asset['id']];
                        }
                        $export_data[] = $export_row;
                    }
                    
                    DF_Export::exportToCSV($export_data, TRUE);
                    return;
                break;
                
                case "html":
                default:
                    $this->view->assets = $assets;
                    $this->view->months = $months;
                    $this->render();
                    return;
                break;
            }
        }
        else
        {
            // Compose a list of groups and months to choose from.
            $this->view->group_options = AssetLocation::fetchSelect();
            $this->render('history_select');
        }
    }
}