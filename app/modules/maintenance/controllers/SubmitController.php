<?php
class Maintenance_SubmitController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('is logged in');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
        $form_config = $this->current_module_config->forms->maintenance_submit->form->toArray();

        $form = new \DF\Form($form_config);
        
        if (\DF\Acl::getInstance()->isAllowed('create work orders'))
        {
            $form->getElement('asset_id')
                    ->setOptions(array(
                        'multiOptions' => Asset::fetchSelect(FALSE, TRUE, TRUE, TRUE),
                        ));
        }
        else
        {
            $occupancies = Occupancy::getFromCurrentUser();
            if ($occupancies)
            {
                $asset_list = array();
                foreach($occupancies as $occupancy)
                {
                    $asset = $occupancy->asset;
                    $asset_list[$asset->id] = $asset->name;
                }
				
                if (empty($asset_list))
                    throw new \DF\Exception\DisplayOnly('You are not listed as being associated with any properties.');
                
                //$form_config['elements']['asset_id'][1]['multiOptions'] = $asset_list;
                $form->getElement('asset_id')
                    ->setOptions(array(
                        'multiOptions' => $asset_list,
                        ));
            }
            else
            {
                throw new \DF\Exception\DisplayOnly('You are not listed as being associated with any properties.');
            }
        }
		
		$user = \DF\Auth::getInstance()->getLoggedInUser();
		$form->setDefaults(array(
			'submitter_name'		=> $user->firstname.' '.$user->lastname,
			'submitter_phone_number' => $user->phone,
			'submitter_email'		=> $user->email,
		));
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$record = new MaintenanceWorkorder();
			
			$record->asset_id = $data['asset_id'];
			$record->user_id = $user->id;
			$record->location = $data['location'];
			$record->origin = 'normal';
			$record->status = MaintenanceWorkorder::STATUS_UNPROCESSED;
			$record->workorder_category_id = $data['maintenance_category_id'];
			
			$record->time_reported = time();
			
			$record->description = $data['description'];
			$record->submitter_name = $data['submitter_name'];
			$record->submitter_phone_number = $data['submitter_phone_number'];
			$record->submitter_email = $data['submitter_email'];
			
			$record->save();
			
			$this->render('success');
			return;
		}
        
        $this->view->form = $form;
	}
	
	/**
	 * Submission history
	 */
	public function historyAction()
	{
		$user = \DF\Auth::getInstance()->getLoggedInUser();

                $workorders = MaintenanceWorkorder::fetchByUserId($user->id);
                
		$this->view->workorders = $workorders;
	}
	
	/**
	 * Request public view
	 */
	public function viewAction()
	{
		$id = intval($this->_getParam('id'));
		$this->view->workorder = MaintenanceWorkorder::find($id);
	}
}