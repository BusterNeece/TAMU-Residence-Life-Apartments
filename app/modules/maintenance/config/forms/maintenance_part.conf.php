<?php
/**
 * Maintenance part form.
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'name'	=> array('text', array(
				'label' => 'Part Name',
				'required' => true,
                'class' => 'full-width',
			)),
            
            'cost'	=> array('text', array(
				'label' => 'Cost',
				'required' => true,
                'description' => 'Write as a decimal with no dollar sign, such as "0.40".',
                'class' => 'half-width',
			)),
            
            'part_number'	=> array('text', array(
				'label' => 'Part Number',
				'required' => true,
                'class' => 'half-width',
			)),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);