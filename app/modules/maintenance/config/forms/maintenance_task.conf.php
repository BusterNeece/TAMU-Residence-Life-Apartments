<?php
/**
 * Maintenance task config form.
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
                    'job_description_id' => array('select', array(
                        'label' => 'Job Description',
                        'required' => true,
                        'multiOptions' => MaintenanceJobDescription::fetchSelect(),
                    )),
            
                    'status'	=> array('select', array(
				'label' => 'Task Status',
				'required' => true,
                                'multiOptions' => MaintenanceWorkorder::getStatusCodes(),
                    )),

                    'time_started' => array('unixdatetime', array(
                        'label' => 'Time Started',
                    )),

                    'time_done' => array('unixdatetime', array(
                        'label' => 'Time Completed',
                    )),

                    'time_spent' => array('text', array(
                        'label' => 'Time Spent',
                        'description' => 'Enter a decimal value, example 1.25',
                    )),
			
                    'description'	=> array('textarea', array(
				'label' => 'Task Description',
				//'required' => true,
                                'class' => 'full-width half-height',
                    )),
            
                    'comments'	    => array('textarea', array(
				'label' => 'Comments',
				//'required' => true,
                                'class' => 'full-width half-height',
                    )),
            
                    'submit'		=> array('submit', array(
                            'type'	=> 'submit',
                            'label'	=> 'Save Changes',
                            'helper' => 'formButton',
                            'class' => 'ui-button',
                    )),
		),
	),
);