<?php
/**
 * Maintenance workorder edit form.
 */

return array(
		'method'		=> 'post',
		'elements'		=> array(
			'asset_id'	=> array('select', array(
				'label' => 'Apartment or Property',
				'required' => true,
                'multiOptions' => Asset::fetchSelect(),
	        )),
            
            'status'	=> array('select', array(
				'label' => 'Job Status',
				'required' => true,
                'multiOptions' => MaintenanceWorkorder::getStatusCodes(),
	        )),
            
            'submitter_name'	=> array('text', array(
				'label' => 'Submitter Name',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_phone_number'	=> array('text', array(
				'label' => 'Submitter Phone Number',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_email'	=> array('text', array(
				'label' => 'Submitter E-mail Address',
				'required' => true,
                'class' => 'half-width',
			)),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Create Make Ready',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
);