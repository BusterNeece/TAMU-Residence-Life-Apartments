<?php
/**
 * Maintenance request submission form.
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			'asset_id'	=> array('select', array(
				'label' => 'Apartment or Property',
				'required' => true,
                'multiOptions' => array(),
	        )),
			
			'location'	=> array('text', array(
				'label' => 'Location of Issue',
				'required' => true,
                'class' => 'full-width',
			)),
            
            'maintenance_category_id'	=> array('select', array(
				'label' => 'Type of Issue',
				'required' => true,
                'multiOptions' => MaintenanceCategory::fetchSelect(true),
	        )),
			
			'description' => array('textarea', array(
				'label' => 'Description of Issue',
				'required' => true,
                'class' => 'full-width half-height',
			)),
            
            'submitter_name'	=> array('text', array(
				'label' => 'Your Name',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_phone_number'	=> array('text', array(
				'label' => 'Your Phone Number',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_email'	=> array('text', array(
				'label' => 'Your E-mail Address',
				'required' => true,
                'class' => 'half-width',
			)),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Submit Request',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);