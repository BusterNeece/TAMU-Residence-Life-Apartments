<?php
/**
 * Maintenance workorder edit form.
 */

return array(    
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			'asset_id'	=> array('select', array(
				'label' => 'Apartment or Property',
				'required' => true,
                'multiOptions' => Asset::fetchSelect(FALSE, TRUE, TRUE, TRUE),
	        )),
            
            'status'	=> array('select', array(
				'label' => 'Job Status',
				'required' => true,
                'multiOptions' => MaintenanceWorkorder::getStatusCodes(),
	        )),
            
            'deferred_to' => array('text', array(
                'label' => 'Deferred To (if applicable)',
                'class' => 'half-width',
                'description' => 'If the job was deferred to another group, enter the group information in this field.',
            )),
            
            'origin'    => array('select', array(
                'label' => 'Origin of Request',
                'required' => 'true',
                'multiOptions' => MaintenanceWorkorder::getOriginTypes(),
            )),
			
			'location'	=> array('text', array(
				'label' => 'Location of Issue',
				//'required' => true,
                                'class' => 'full-width',
			)),
            
            'workorder_category_id'	=> array('select', array(
				'label' => 'Type of Issue',
				'required' => true,
                'multiOptions' => MaintenanceCategory::fetchSelect(),
	        )),
			
			'description' => array('textarea', array(
				'label' => 'Description of Issue',
				'required' => true,
                'class' => 'full-width half-height',
			)),
            
            'submitter_name'	=> array('text', array(
				'label' => 'Submitter Name',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_phone_number'	=> array('text', array(
				'label' => 'Submitter Phone Number',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'submitter_email'	=> array('text', array(
				'label' => 'Submitter E-mail Address',
				'required' => true,
                'class' => 'half-width',
			)),
            
            'time_started' => array('unixdatetime', array(
                'label' => 'Time Started',
            )),
            
            'time_done' => array('unixdatetime', array(
                'label' => 'Time Completed',
            )),

            'time_spent' => array('text', array(
                'label' => 'Time Spent',
                'description' => 'Enter a decimal value, example 1.25',
            )),

            'notes' => array('textarea', array(
                'label' => 'Done Comments',
            )),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);