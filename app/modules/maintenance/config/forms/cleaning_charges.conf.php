<?php
/**
 * Occupancy edit form.
 */

$form_config = array(
	'method'		=> 'post',
    'elements'      => array(),
);

$form_config['elements'] += array(
    'asset_id' => array('select', array(
        'label' => 'Apartment',
        'multiOptions' => Asset::fetchSelect(),
    )),

    'worker' => array('text', array(
        'label' => 'Assigned Worker',
        'class' => 'full-width',
    )),

    'time_assigned' => array('unixdatetime', array(
        'label' => 'Date Assigned',
    )),
    
    'time_completed' => array('unixdatetime', array(
        'label' => 'Date Completed',
    )),
);

$cleaning_areas = array(
    'Range',
    'Walls',
    'Dining Table',
    'Refrigerator',
    'Commode',
    'End Table',
    'Kitchen Sink',
    'Bathtub',
    'Coffee Table',
    'Kitchen Disposal',
    'Bathroom Sink',
    'Night Table',
    'Kitchen Counter',
    'Medicine Cabinet',
    'Chest of Drawers',
    'Kitchen Cabinets',
    'Light Fixtures',
    'Mirrors',
    'AC Vents',
    'Couch',
    'Bed Frame',
    'Heater Vents',
    'Upho Chair',
    'Mattress',
    'Vent Hood Filter',
    'Occ Chair',
    'Dining Chairs',
    'Doors',
    'Bedroom Ceiling Fan',
    'Living Rm Ceiling Fan',
    'Desk',
    'Desk Chair',
    'Dishwasher',
    'Washing Machine',
    'Dryer',
    'Window Blinds',
);

foreach($cleaning_areas as $area)
{
    $element_name = str_replace(' ', '_', strtolower($area));

    $form_config['elements'] += array(
        $element_name => array('text', array(
            'label' => $area,
            'size' => 5,
            'class' => 'cleaning_area_input',
            'belongsTo' => 'charge_data',
        )),
    );
}

$form_config['elements'] += array(
    'total_hours' => array('text', array(
        'label' => 'Total Hours',
        'readonly' => 'readonly',
        'size' => 5,
        'belongsTo' => 'charge_data',
    )),

    'notes' => array('textarea', array(
        'label' => 'Additional Notes',
        'class' => 'full-width half-height',
    )),

    'submit' => array('submit', array(
        'type'	=> 'submit',
        'label'	=> 'Save Changes',
        'helper' => 'formButton',
        'class' => 'ui-button',
    )),
);

return array('form' => $form_config);