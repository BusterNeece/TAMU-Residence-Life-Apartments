<?php
class Apply_OffersController extends \DF\Controller\Action
{
    public function permissions()
    {
        \DF\Auth::getInstance()->login();
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    public function viewAction()
    {
        $id = intval($this->_getParam('id'));
        $offer = \Entity\Offer::find($id);
        
        if ($offer->response_deadline < strtotime('now'))
                throw new \DF\Exception\DisplayOnly('This offer has expired!');

        $lease_info = LeaseVersion::buildFromOffer($offer);
        $lease = $lease_info['lease'];

        $this->view->lease = $lease;
        $this->view->lease_html = $lease_info['html'];
        
        $this->view->offer = $offer;
        $this->view->resident = $offer->resident;
        $this->view->asset = Asset::find($offer->asset_id);
    }

    public function acceptAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));
        
        $id = intval($this->_getParam('id'));
        $offer = \Entity\Offer::find($id);
        $app = Application::find($offer->application_id);

        // Review contract information.
        $uin = $this->_getParam('uin');
        if ($uin != $offer->resident->uin)
            throw new \DF\Exception\DisplayOnly('The UIN you specified does not match the offer letter.');
        
        $initials = $this->_getParam('initials');
        if (empty($initials))
            throw new \DF\Exception\DisplayOnly('You did not enter your initials. Please go back and complete this step.');

        $lease_info = LeaseVersion::buildFromOffer($offer);
        $lease = $lease_info['lease'];

        $this->view->lease = $lease;
        $this->view->lease_html = $lease_info['html'];

        if (!($lease instanceof LeaseVersion))
            throw new \DF\Exception\DisplayOnly('A valid housing contract could not be found. Please contact the University Apartments staff.');

        $offer->lease_version_id = $lease->id;
        $offer->contract_initials = $initials;
        $offer->contract_initials_date = time();
        $offer->accepted_date = time();

        $app->application_status_id = 3;

        // Move the user into the apartment.
        $occupancy = new Occupancy();
        $occupancy->lease_signor = 'Y';
        $occupancy->occupancy_model_id = $offer->assetType->occupancy_model_id;
        $occupancy->resident_id = (int)$offer->resident_id;
        $occupancy->asset_id = (int)$offer->asset_id;
        $occupancy->application_id = (int)$offer->application_id;

        if ($app->is_renewal)
        {
            $occupancy->start_date = (int)$offer->move_in_date;
            $occupancy->end_date = (int)$offer->move_out_date;

            $old_occupancy = $app->occupancy;
            $old_occupancy->end_date = (int)$offer->move_in_date;
            $old_occupancy->save();
        }
        else
        {
            $occupancy->moveindatedesired = (int)$offer->move_in_date;
            $occupancy->end_date = (int)$offer->move_out_date;
        }
        
        try
        {
            $offer->save();
            $app->save();
            $occupancy->save();

            if (!$app->is_renewal)
            {
                // Post security deposit transaction and split.
                $transaction = new RegisterTransaction();
                $transaction->posted = time();
                $transaction->date = time();
                $transaction->memo = 'Security Deposit';
                $transaction->save();
                $transaction_id = $transaction->id;
                
                $split = new Split();
                $split->posted = time();
                $split->transaction_id = $transaction_id;
                $split->resident_id = $offer->resident_id;
                $split->ledger_id = Split::LEDGER_STANDARD;
                $split->credit_or_debit = 'D';
                $split->item_id = Item::fetchByName('Security Deposit');
                $split->split_amount = 0-(Settings::getSetting('application_deposit'));
                $split->save();
                
                Split::updateRunningBalance($split->resident_id, $split->ledger_id);
            }

            // Send e-mail notification to staff.
            \DF\Messenger::send(array(
                'to' => 'university-apartments@housing.tamu.edu',
                'subject' => 'Resident Signed Online Lease',
                'template' => 'admin_notifications/lease',
                'vars' => array(
                    'resident'  => $offer->resident,
                    'asset'     => Asset::find($offer->asset_id),
                ),
            ));

            if ($app->is_renewal)
            {
                $this->flash('Offer has been accepted.', 'green');
                $this->redirectToRoute(array('module' => 'default'));
            }
            else
            {
                $this->flash('Offer has been accepted. Remember to pay your security deposit.');
                $this->redirectToRoute(array('module'=>'finances','controller'=>'resident','action'=>'pay'));
            }
            return;
        }
        catch(Doctrine_Exception $e)
        {
            $this->alert($e->getMessage());
        }
    }
}