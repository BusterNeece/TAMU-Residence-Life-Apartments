<?php
class Api_IndexController extends \DF\Controller\Action_Api
{
    /**
     * Main display.
     */
    public function indexAction()
    {
		$this->output('If you are seeing this message, your API connection is working correctly!');
		return;
	}
}