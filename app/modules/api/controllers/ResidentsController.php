<?php
class Api_ResidentsController extends \DF\Controller\Action_Api
{
    /* Search */
    public function searchAction()
    {
        $q = '%'.$this->_getParam('q', 'NULL').'%';
        
        $residents = Doctrine_Query::create()
			->from('Resident r')
			->where('CONCAT(r.first_name, \' \', r.surname) LIKE ?', $q)
			->orWhere('r.uin LIKE ?', $q)
			->orWhere('r.email LIKE ?', $q)
			->execute();
        
        $results = array();
        if (count($residents) > 0)
        {
            foreach($residents as $resident)
            {
                $results[] = array(
                    'uin'       => $resident->uin,
                    'firstname' => $resident->first_name,
                    'lastname'	=> $resident->surname,
                    'email'     => $resident->email,
                    'type'      => $resident->type->name,
                );
            }
        }
        
		$this->output($results);
		return;
	}
    
    /* Emergency Contacts */
    public function contactsAction()
    {
        if ($this->_hasParam('uin'))
            $resident = Resident::fetchByUin($this->_getParam('uin'));
        else if ($this->_hasParam('id'))
            $resident = Resident::find($this->_getParam('id'));
        
        if (!($resident instanceof Resident))
            throw new DF_Exception('Resident not found!');
        
        $resident_info = array(
            'id'                => $resident->id,
            'firstname'         => $resident->first_name,
            'lastname'          => $resident->surname,
            'uin'               => $resident->uin,
            'type'              => $resident->type->name,
        );
        
        $resident_info['contact'] = array(
            'primary_phone'     => $resident->primary_phone,
            'secondary_phone'   => $resident->secondary_phone,
            'email'             => $resident->email,
        );
        
        $resident_info['mailing_address'] = $resident->getAddress('mailing');
        $resident_info['permanent_address'] = $resident->getAddress('permanent');
        $resident_info['forwarding_address'] = $resident->getAddress('forwarding');
        
        $emergency_fields = array('emergency' => 'emergency_1', 'emergency2' => 'emergency_2');
        foreach($emergency_fields as $local_field => $report_field)
        {
            $resident_info[$report_field] = array(
                'firstname' => $resident->get($local_field.'_firstname'),
                'lastname' => $resident->get($local_field.'_lastname'),
                'relationship' => $resident->get($local_field.'_relationship'),
                'phone' => $resident->get($local_field.'_phone'),
                'is_missing_person_contact' => $resident->get($local_field.'_missing_person'),
            );
            $resident_info[$report_field] += $resident->getAddress($local_field);
        }
        
        $this->output($resident_info);
        return;
    }
}