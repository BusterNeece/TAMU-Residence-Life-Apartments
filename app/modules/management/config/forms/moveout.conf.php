<?php
/**
 * Move Out Form
 */

return array(
    'method'		=> 'post',
    'elements'		=> array(

        'desired_move_out' => array('unixdate', array(
            'label' => 'Desired Move-out Date',
            'required' => true,
        )),

        'moveout_reason' => array('textarea', array(
            'label' => 'Move-out Reason',
            'required' => true,
            'style' => 'height:10em;',
        )),

        'submit' => array('submit', array(
            'type' => 'submit',
            'label' => 'Save Changes',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);