<?php
/**
 * Occupancy edit form.
 */

return array(    
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
        
		'elements'		=> array(
            
            'start_date' => array('unixdate', array(
                'label' => 'Start of Occupancy (Move-In Date)',
            )),
            
            'end_date' => array('unixdate', array(
                'label' => 'End of Occupancy (Move-Out Date)',
            )),

            'checkin_date' => array('unixdate', array(
                'label' => 'Check-In Date',
                'description' => 'If the resident physically moved in to the apartment on a different day than the Move-In Date, list the actual check-in date here; otherwise, leave this field blank.'
            )),

            'checkout_date' => array('unixdate', array(
                'label' => 'Check-Out Date',
                'description' => 'If the resident physically moved out of the apartment on a different day than the Move-Out Date, list the actual check-out date here; otherwise, leave this field blank.'
            )),
            
            'occupancy_model_id' => array('radio', array(
                'label' => 'Occupancy Model',
                'multiOptions' => OccupancyModel::fetchSelect(),
            )),
            
            'checkoutappointment' => array('unixdate', array(
                'label' => 'Check Out Appointment',
            )),
            
            'moveinappointment' => array('unixdate', array(
                'label' => 'Move-In Appointment',
            )),
            
            'moveindatedesired' => array('unixdate', array(
                'label' => 'Move-In Date Desired',
            )),
            
            'orientation' => array('unixdate', array(
                'label' => 'Orientation Date',
            )),
            
            'lease_signor' => array('select', array(
                'label' => 'Is Lease Signor',
                'multiOptions' => array(
                    'Y' => 'Yes',
                    'N' => 'No',
                ),
            )),
            
            'initial_meter_reading' => array('text', array(
                'label' => 'Initial Meter Reading',
                'filters' => array('Float'),
            )),
            
            'finalelectric' => array('text', array(
                'label' => 'Final Meter Reading',
                'filters' => array('Float'),
            )),
            
            'finalcharges' => array('text', array(
                'label' => 'Final Charges',
                'filters' => array('Float'),
            )),
            
            'finalrent' => array('text', array(
                'label' => 'Final Rent',
                'filters' => array('Float'),
            )),
            
            'finalbalance' => array('text', array(
                'label' => 'Final Balance',
                'filters' => array('Float'),
            )),
            
            'moveout_form_received' => array('unixdate', array(
                'label' => 'Signed Move Out Form Received',
            )),

            'cost'      => array('text', array(
                'label' => 'Override Rent Cost for Occupanct',
                'description' => 'To set the rent cost for this specific occupancy, enter an amount below. Otherwise, leave this field blank or set it to zero.',
                'filters' => array('Float'),
                'validators' => array('Float'),
            )),

            'is_external' => array('radio', array(
                'label' => 'Is External (GP2) Occupancy',
                'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                'description' => 'Setting this option to "Yes" will classify this resident as external, preventing rent payment and other features from being used.',
            )),
            
            'submit' => array('submit', array(
                'type'	=> 'submit',
                'label'	=> 'Save Changes',
                'helper' => 'formButton',
                'class' => 'ui-button',
            )),
        ),
    ),
);