<?php
/**
 * Asset type form
 */

return array(	
	/**
	 * Form Configuration
	 */
	 
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'name'		=> array('text', array(
				'label' => 'Name',
				'required' => true,
	        )),
			
			'description'	=> array('text', array(
				'label' => 'Description',
				'required' => true,
	        )),
            
            'asset_category_id' => array('select', array(
                'label' => 'Category',
                'multiOptions' => AssetType::getAssetTypeCodes(),
            )),

            'occupancy_model_id' => array('radio', array(
            	'label' => 'Occupancy Model',
            	'multiOptions' => OccupancyModel::fetchSelect(),
	        )),

	        'asset_waiting_list_category' => array('radio', array(
	        	'label' => 'Should this option appear on application form?',
	        	'multiOptions' => array(
	        		0 => 'No',
	        		1 => 'Yes',
	        	),
	        )),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
); 