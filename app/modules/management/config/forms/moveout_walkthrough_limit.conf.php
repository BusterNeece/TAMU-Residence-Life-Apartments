<?php
/**
 * Move Out Form
 */

return array(
    'method'		=> 'post',
    'elements'		=> array(

        'max_appointments' => array('text', array(
            'label' => 'Maximum Walk-through Appointments per hour',
            'required' => true,
            'description' => 'Set this number to the amount of walk-through appointments the office can support each hour. The number will default to 2 if no limit is set.',
        )),

        'submit' => array('submit', array(
            'type' => 'submit',
            'label' => 'Save Changes',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);