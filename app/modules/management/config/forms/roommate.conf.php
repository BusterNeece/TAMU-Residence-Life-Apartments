<?php
/*
 * Add/Edit Vehicle
 *
 */
$user = DF_Auth::getInstance()->getLoggedInUser();
$occupancies = array();
foreach(Occupancy::getActiveByResidentId($user->Resident->id) as $occupancy)
{
    $occupancies[$occupancy->Asset->id] = $occupancy->Asset->name;
}

return array(
    'method' => 'post',
    'elements' => array(
        'asset_id' => array('select', array(
            'label' => 'Apartment',
            'multiOptions' => $occupancies,
        )),
        
        'email' => array('text', array(
            'label' => 'New Roommate Email',
            'validators' => array(
                'EmailAddress',
            ),
            'required' => true,
        )),
        
        'submit' => array('submit', array(
            'label' => 'Save',
            'class' => 'ui-button',
        )),
    ),
);