<?php
/**
 * Apartment Management
 */

return array(	
	/**
	 * Form Configuration
	 */
	 
	 /* Adds null records without name, cost, or description.... */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			
            'name'		=> array('text', array(
                'label' => 'Asset Name',
                'required' => true,
                'class' => 'full-width',
            )),
            
            'description' => array('textarea', array(
                'label' => 'Description',
                'required' => true,
                'class' => 'full-width half-height',
            )),
            
            'asset_status_id' => array('select', array(
                'label' => 'Status',
                'required' => true,
                'multiOptions' => \Entity\AssetStatus::fetchSelect(),
            )),
            
            'asset_type_id' => array('select', array(
                'label' => 'Type',
                'required' => true,
                'multiOptions' => \Entity\AssetType::fetchSelect(),
            )),
            
            'asset_location_id' => array('select', array(
                'label' => 'Location',
                'multiOptions' => \Entity\AssetLocation::fetchSelect(TRUE),
            )),
            
            'asset_parent_id' => array('select', array(
                'label' => 'Parent Asset',
                'multiOptions' => \Entity\Asset::fetchSelect(TRUE, FALSE),
            )),
            
            'asset_subcode' => array('text', array(
                'label' => 'Asset Subcode',
                'filters' => array('Float'),
            )),
            
            'meter_sort_order' => array('text', array(
                'label' => 'Meter Sort Order',
                'filters' => array('Float'),
            )),
            
            'electric_provider_id' => array('select', array(
                'label' => 'Electric Provider',
                'required' => true,
                'multiOptions' => \Entity\ElectricProvider::fetchSelect(),
            )),
            
            'is_exempt_electrical' => array('checkbox', array(
                'label' => 'Asset is exempt from paying utilities',
            )),
            
            'mailbox_code' => array('text', array(
                'label' => 'Mailbox Code',
            )),
            
            'phone_type' => array('radio', array(
                'label' => 'Phone Type',
                'multiOptions' => array(
                    'none'      => 'None',
                    'landline'  => 'Standard Landline',
                    'voip'      => 'VoIP Line',
                ), 
            )),
            
            'phone_number' => array('text', array(
                'label' => 'Phone Number',
            )),

            'num_bedrooms' => array('text', array(
                'label' => 'Number of Bedrooms (for per-bedroom assignment)',
                'filters' => array('Int'),
            )),

            'notes' => array('textarea', array(
                'label' => 'Notes',
                'class' => 'full-width half-height',
            )),
            
            'submit'		=> array('submit', array(
                    'type'	=> 'submit',
                    'label'	=> 'Save Changes',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
            )),
		),
	),
);