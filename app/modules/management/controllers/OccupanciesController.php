<?php
use \Entity\Occupancy;

class Management_OccupanciesController extends \DF\Controller\Action
{
    // (Not used, as page is only accessed from apartment or resident view).
    public function indexAction()
    {
    }
    
    // Special add page, as addition requires both an asset ID and resident ID.
    public function addAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();
        $asset_id = (int)$this->_getParam('asset_id');
        $resident_id = (int)$this->_getParam('resident_id');
        
        if ($asset_id == 0 || $resident_id == 0)
            throw new \DF\Exception\DisplayOnly('Not enough information specified: you must specify both the apartment ID and the resident ID to edit an occupancy.');
        
        $record = new Occupancy();
        $record->asset_id = $asset_id;
        $record->resident_id = $resident_id;
        $record->save();
        
        $origin = ($this->_hasParam('origin')) ? $this->_getParam('origin') : 'apartment';
        $this->redirectFromHere(array('action' => 'edit', 'id' => $record->id, 'asset_id' => NULL, 'resident_id' => NULL, 'origin' => $origin));
    }
    
    public function editAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        $id = (int)$this->_getParam('id');
        
        $record = Occupancy::find($id);
        if (!$record)
            throw new \DF\Exception\DisplayOnly('Record not found!');
        
		$form = new \DF\Form($this->current_module_config->forms->occupancy->form);
		$form->setDefaults($record->toArray());
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
            
            $record->fromArray($data);
			$record->save();
			
			$this->alert('Occupancy updated.');
			
			$origin = $this->_getParam('origin', 'apartment');
			if ($origin == "apartment")
				$this->redirectToRoute(array('module' => 'management', 'controller' => 'apartments', 'action' => 'view', 'id' => $record->asset_id));
			else if ($origin == "moveout")
                $this->redirectToRoute(array('module' => 'management', 'controller' => 'occupancies', 'action' => 'moveout'));
            else
				$this->redirectToRoute(array('module' => 'management', 'controller' => 'residents', 'action' => 'view', 'id' => $record->resident_id));
		}
		
		$this->view->form = $form;
    }
	
	public function deleteAction()
	{
        $this->acl->checkPermission('manage apartments');

		\DF\Csrf::validateToken($_REQUEST['csrf']);
		
		$record = Occupancy::find($this->_getParam('id'));
		if (!$record)
			throw new \DF\Exception\DisplayOnly('Occupancy record not found!');
		
		$asset_id = $record->asset_id;
		$resident_id = $record->resident_id;
		
		$record->delete();
        
        $this->alert('Record deleted.');
		
		$origin = ($this->_hasParam('origin')) ? $this->_getParam('origin') : 'apartment';
		if ($origin == "apartment")
			$this->redirectToRoute(array('module' => 'management', 'controller' => 'apartments', 'action' => 'view', 'id' => $record->asset_id));
		else
			$this->redirectToRoute(array('module' => 'management', 'controller' => 'residents', 'action' => 'view', 'id' => $record->resident_id));
	}
	
	public function toggleleaseAction()
	{
        $this->acl->checkPermission('manage apartments');

		$id = (int)$this->_getParam('id');
        $record = Occupancy::find($id);
        if (!$record)
            throw new \DF\Exception\DisplayOnly('Record not found!');
		
		$record->lease_signor = ($record->lease_signor == "Y") ? "N" : "Y";
		$record->save();
		
		$this->alert('Lease signor status toggled!');
		$this->redirectToRoute(array('module' => 'management', 'controller' => 'apartments', 'action' => 'view', 'id' => $record->asset_id, '#target' => 'occupants'));
	}

    public function moveoutAction()
    {
        /*
         * List all desired move-outs
         */

        if (!\DF\Acl::getInstance()->isAllowed('view moveouts'))
            throw new \DF\Exception\PermissionDenied();

        $filter_form = new \DF\Form($this->current_module_config->forms->filter_daterange);

        if( ($_POST) && ($filter_form->isValid($_POST)) )
        {
            $values = $filter_form->getValues();

            $values = array_merge($this->getRequest()->getUserParams(), $values);

            if( !$values['start'] )
                unset($values['start']);

            if( !$values['end'] )
                unset($values['end']);

            $this->redirect(\DF\Url::route($values));
        }

        $filters = array(
            'start' => $this->_getParam('start'),
            'end' => $this->_getParam('end'),
        );
        // set defaults
        if (empty($filters['start']))
            $filters['start'] = strtotime('today');
        if (empty($filters['end']))
            $filters['end'] = strtotime('+30 days');
        if ($this->_hasParam('searchterms'))
            unset($filters['start'], $filters['end']); // don't restrict date on keyword searches
            $this->view->searchterms = $filters['searchterms'] = $this->_getParam('searchterms');
        
        $filter_form->setDefaults($filters);

        $all_records = Occupancy::fetchMoveOuts($filters);

        $paginator = \Zend_Paginator::factory((array)$all_records);
        $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);

        $this->view->pager = $paginator;
        $this->view->num_records = count($all_records);
        $this->view->filter_form = $filter_form;
    }

    public function approvemoveoutAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));
        $this->doNotRender();

        $user = \DF\Auth::getInstance()->getLoggedInUser();

        $id = $this->_getParam('id');
        $occupancy = Occupancy::find($id);
        if ($occupancy instanceof Occupancy)
        {
            try
            {
                $occupancy->end_date = intval($occupancy->moveoutdate);
                $occupancy->save();
                
                $this->flash('Move-out approved.');
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        $this->redirectToReferrer();
    }

    public function walkthroughAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('view moveouts'))
                throw new \DF\Exception\PermissionDenied();

        if ($this->_hasParam('date'))
        {
            $start_date = intval($this->_getParam('date'));
            $end_date = strtotime('+6 days', $start_date);
        }
        else
        {
            $start_date = strtotime('today');
            $end_date = strtotime('+6 days');
        }

        $all_appointments = MoveoutAppointment::fetchByDate($start_date, $end_date);

        $dates = array();
        while($start_date <= $end_date)
        {
            $dates[] = $start_date;
            $start_date = strtotime('+1 day', $start_date);
        }

        $this->view->appointments = $all_appointments;
        $this->view->dates = $dates;
    }

    public function editmaxappointmentsAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        $form = new \DF\Form($this->current_module_config->forms->moveout_walkthrough_limit);

        $date = $this->_getParam('date');
        $max = MoveoutAppointmentLimit::fetchByDate((int)$date);

        if ($max->id)
        {
            $form->setDefaults($max->toArray());
        }
        else
        {
            $max = new MoveoutAppointmentLimit();
            $max->date = (int)$date;
        }

        if (($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();

            $max->max_appointments = $data['max_appointments'];

            try
            {
                $max->save();
                $this->flash('Maximum appointments limit updated for '.date('m/d/Y', (int)$date));
                $this->redirectToReferrer();
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert('Error setting max appointments: '. $e->getMessage());
            }
        }

        if ($this->_getParam('ajax') == true)
        {
            $this->doNotRender();

            $form->setAction(str_replace('ajax/true','',\DF\Url::current(false)));

            $this->getResponse()->setHeader('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT');
            $this->getResponse()->setHeader('Pragma', 'no-cache');
            $this->getResponse()->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
            $this->getResponse()->setHeader('Cache-Control', 'post-check=0, pre-check=0', false);
            $this->getResponse()->setHeader('content-type', 'application/json');
            $this->getResponse()->appendBody(Zend_Json::encode($form->render()));
        }
        else
        {
            $this->view->form = $form;
        }
    }

    public function addwalkthroughAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        $date = $this->_getParam('date');
        $slot = $this->_getParam('slot');
        $movein = $this->_getParam('movein');
        $resident_id = $this->_getParam('resident_id');
        $resident = Resident::find($resident_id);
        $occupancies = Occupancy::getActiveByResidentId($resident_id);
        foreach($occupancies as $occupancy)
        {
            $occupancy_id = $occupancy->id;
        }
        $appt = new MoveoutAppointment();
        $appt->resident_id = $resident_id;
        $appt->occupancy_id = $occupancy_id;
        $appt->date = $date;
        $appt->slot = $slot;
        $appt->movein = $movein;

        try
        {
            $appt->save();
            $this->flash('Appointment successfully scheduled');
            $this->redirectToReferrer();
        }
        catch(Doctrine_Exception $e)
        {
            $this->alert('Error: Could not schedule appointment');
        }
    }

    public function deletewalkthroughAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));

        $id = $this->_getParam('id');
        $appt = MoveoutAppointment::find($id);

        if (!$appt->id)
        {
            throw new \DF\Exception\DisplayOnly('Appointment id not found');
        }
        else
        {
            try
            {
                $appt->delete();
                $this->flash('Appointment deleted successfully');
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert('Error deleting appointment: '. $e->getMessage());
            }
        }
        $this->redirectToReferrer();
    }
    
    public function moveincalendarAction()
    {
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('manage apartments'))
        {
            if ($this->_hasParam('date'))
            {
                $start_date = intval($this->_getParam('date'));
                if ($this->_getParam('view') != 'day')
                    $end_date = strtotime('+6 days', $start_date);
                else
                    $end_date = $start_date;
            }
            else
            {
                // Gardens Phase II dates 8/20-8/28
                $start_date = strtotime('2011-08-20');
                $end_date = strtotime('2011-08-28');
            }
            
            $all_appointments = MoveoutAppointment::fetchByDate($start_date, $end_date, true, true);

            $dates = array();
            while($start_date <= $end_date)
            {
                $dates[] = $start_date;
                $start_date = strtotime('+1 day', $start_date);
            }
            
            $this->view->start_time = 800;
            $this->view->end_time = 1930;
            $this->view->limit = 6;
            $this->view->dates = $dates;
            $this->view->appointments = $all_appointments;
            $this->view->mode = $this->_getParam('view');
        }
    }
    
    public function noshowAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));
        
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('manage apartments'))
        {
            $id = $this->_getParam('id');
            $appt = MoveoutAppointment::find($id);

            if (!$appt->id)
            {
                throw new \DF\Exception\DisplayOnly('Appointment id not found');
            }
            else
            {
                $appt->noshow_log = strtotime('now');
                try
                {
                    $appt->save();
                    $this->alert('Appointment flagged as no-show');
                }
                catch(Doctrine_Exception $e)
                {
                    $this->alert('Error flagging appointment: '. $e->getMessage());
                }
            }
        }
        $this->redirectToReferrer();
    }
    
    public function undonoshowAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));
        
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('manage apartments'))
        {
            $id = $this->_getParam('id');
            $appt = MoveoutAppointment::find($id);

            if (!$appt->id)
            {
                throw new \DF\Exception\DisplayOnly('Appointment id not found');
            }
            else
            {
                $appt->noshow_log = 0;
                try
                {
                    $appt->save();
                    $this->alert('Appointment unflagged as no-show');
                }
                catch(Doctrine_Exception $e)
                {
                    $this->alert('Error unflagging appointment: '. $e->getMessage());
                }
            }
        }
        $this->redirectToReferrer();
    }
    
    public function rescheduleAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage apartments'))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));
        
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('manage apartments'))
        {
            if(!empty($_POST))
            {
                $id = $this->_getParam('id');
                $appt = MoveoutAppointment::find($id);

                if (!$appt->id)
                {
                    throw new \DF\Exception\DisplayOnly('Appointment id not found');
                }
                else
                {
                    $appt->date = strtotime($this->_getParam('date'));
                    $appt->slot = $this->_getParam('slot');
                    try
                    {
                        $appt->save();
                        $this->alert('Appointment rescheduled successfully');
                    }
                    catch(Doctrine_Exception $e)
                    {
                        $this->alert('Error rescheduling appointment: '. $e->getMessage());
                    }
                }
            }
        }
        $this->redirectToReferrer();
    }
}