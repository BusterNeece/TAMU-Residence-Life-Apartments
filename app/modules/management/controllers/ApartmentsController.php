<?php
use \Entity\Asset;

class Management_ApartmentsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed(array('manage apartments', 'view apartments'));
	}

    public function indexAction()
    {
        $buildings_raw = $this->em->createQuery('SELECT a.id, a.name FROM Entity\Asset a WHERE (a.asset_parent_id IS NULL OR a.asset_parent_id = 0) ORDER BY a.name ASC')
            ->getArrayResult();
        
        $buildings = array();
        foreach($buildings_raw as $asset)
        {
            $buildings[$asset['id']] = $asset['name'];
        }
        $this->view->complexes = $buildings;

        // Show sub-assets if applicable.
        if ($this->_hasParam('parent_asset'))
        {
            $parent_asset_id = (int)$this->_getParam('parent_asset');
            $parent_asset = Asset::find($parent_asset_id);
            $cache_name = 'buildings_in_asset_'.$parent_asset_id;
            
            // Support for caching building information.
            if (\DF\Cache::test($cache_name))
            {
                $buildings = \DF\Cache::load($cache_name);
            }
            else
            {
                $asset_tree = Asset::fetchSelect(FALSE, TRUE, TRUE, FALSE, FALSE);
                $parent_asset_children = $asset_tree[$parent_asset->name];
                
                unset($parent_asset_children[$parent_asset_id]);
                
                $buildings = array();
                
                foreach($parent_asset_children as $building_name => $building_apts)
                {
                    // Single item.
                    if (!is_array($building_apts))
                    {
                        $building_id = (int)$building_name;
                        $building_name = $building_apts;
                        $building_apts = array($building_id => $building_apts);
                    }
                    
                    $filtered_name = preg_replace('/[^a-zA-Z0-9]/', '', $building_name);
                    $buildings[$filtered_name] = array(
                        'name'      => $building_name,
                        'apts'      => array(),
                    );
                    
                    foreach($building_apts as $asset_id => $asset_name)
                    {
                        
                        // Speedier than fetchById for the data being retrieved below.
                        $asset = $this->em->createQuery('SELECT a, s, al, at, o FROM Entity\Asset a LEFT JOIN a.status s LEFT JOIN a.location al LEFT JOIN a.type at LEFT JOIN a.occupancies o WHERE a.id = :id')
                            ->setParameter('id', $asset_id)
                            ->execute();

                        $asset = $asset[0];
                        
                        // Pull current cost.
                        $asset_cost = $asset->getCost();
                        
                        // Pull current residents.
                        $residents_raw = $asset->getPrincipalResidents();
                        $residents = array();
                        
                        if ($residents_raw)
                        {
                            foreach($residents_raw as $resident)
                            {
                                $residents[] = $resident->toArray();
                            }
                        }
                        
                        // Add to display.
                        $apartment_row = array(
                            'name'      => $asset['name'],
                            'status'    => $asset['status']['name'],
                            'type'      => $asset['type']['name'],
                            'cost'      => $asset_cost,
                            'residents' => $residents,
                            'notes'     => $asset['notes'],
                        );
                        
                        $buildings[$filtered_name]['apts'][$asset_id] = $apartment_row;
                    }
                }
                
                \DF\Cache::save($buildings, $cache_name, array(), 3600);
            }
            
            $this->view->buildings = $buildings;
            $this->view->complex_id = $parent_asset->id;
            $this->view->complex = $parent_asset->name;
        }
	}
	
	public function viewAction()
	{
		if ($this->_hasParam('id'))
			$asset = Asset::find($this->_getParam('id'));
		else if ($this->_hasParam('name'))
			$asset = Asset::getRepository()->findOneByName($this->_getParam('name'));
		
		if (!$asset)
			throw new \DF\Exception\DisplayOnly('Asset not found!');
            
        $form = new \DF\Form($this->current_module_config->forms->apartment->form);
        $form->populate($asset->toArray());
        
        $this->view->asset = $asset;
        $this->view->asset_info_form = $form;
        $this->view->cost_history = $asset->getCostHistory();
	}
	
	public function editAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->apartment->form);
        
        $id = intval($this->_getParam('id'));
		
		if ($id != 0)
		{
			$record = Asset::find($id);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			if (!$record)
				$record = new Asset();

            $record->fromArray($data);
            $record->save();
            
            $this->alert('Apartment updated.', 'green');
			$this->redirectFromHere(array('action' => 'view', 'id' => $record->id));
            return;
		}

        $this->view->headTitle('Add/Update Apartment');
        $this->renderForm($form);
	}
	
	public function findresidentAction()
	{
        $this->doNotRender();
        
        $query = $this->_getParam('query');
        $results = array();
        
        if (strlen($query) > 2)
        {
			$results_array = Doctrine_Query::create()
                ->from('Resident r')
                ->orWhere('r.first_name LIKE ?', '%'.$query.'%')
                ->orWhere('r.surname LIKE ?', '%'.$query.'%')
                ->orWhere('r.uin LIKE ?', '%'.$query.'%')
                ->orWhere('r.email LIKE ?', '%'.$query.'%')
                ->orderBy('r.surname ASC')
                ->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
			
            if ($results_array)
            {
                foreach($results_array as $result)
                {
                    if (!$result['deleted_at'])
                    {
                    $results[] = array(
                        'id' => $result['id'],
                        'text' => $result['surname'].', '.join(' ',array($result['first_name'],$result['middle_name'])).' ('.$result['uin'].')',
					);
                    }
                }
            }
        }
        
        echo \Zend_Json::encode(array('status' => 'success', 'results' => $results, 'results_num' => count($results)));
	}
	
	public function deleteAction()
	{
		\DF\Csrf::validateToken($_REQUEST['csrf']);
		
		$record = Asset::find($this->_getParam('id'));
        if ($record)
            $record->delete();
        
        $this->alert('Record deleted.');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}

	public function vacantAction()
	{
		$vacant = Asset::fetchAllVacantByMonth();
        
		$now = $vacant['now'];
		unset($vacant['now']);
		if (is_array($now))
			array_unshift($vacant, $now);
        
		$this->view->vacant = $vacant;
	}
}