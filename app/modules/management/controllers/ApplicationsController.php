<?php
use \Entity\Application;
use \Entity\Resident;
use \Entity\Asset;
use \Entity\Split;
use \Entity\Offer;
use \Entity\Settings;

class Management_ApplicationsController extends \DF\Controller\Action
{
    public function indexAction()
    {
        $this->acl->checkPermission(array('view applications', 'manage applications'));

        $page = ($this->_getParam('page')) ? $this->_getParam('page') : 1;
        
        $vacant_ids = array();
        foreach(Asset::fetchAllVacantByMonth() as $month => $vacant)
        {
            foreach($vacant as $v)
            {
                $vacant_types[(string)$month][] = $v['type']['id'];
            }
        }
        
        $this->view->status_id = $status_id = ($this->_getParam('status')) ? $this->_getParam('status') : 1;
        $this->view->vacant = $vacant_types;

        $qb = $this->em->createQueryBuilder()
            ->select('a, r')
            ->from('Entity\Application', 'a')
            ->leftJoin('a.resident', 'r');

        if ($this->_hasParam('searchterms'))
        {
            $this->view->searchterms = $search_terms = $this->_getParam('searchterms');
            
            $qb->where('(r.first_name LIKE :q OR r.surname LIKE :q OR r.uin LIKE :q OR r.email LIKE :q')
                ->setParameter('q', '%'.$search_terms.'%');
        }
        else
        {
            $qb->where('a.application_status_id = :status_id')
                ->setParameter('status_id', $status_id);
        }

        $pager = new \DF\Paginator\Doctrine($qb);
        $this->view->pager = $pager;

    }

    public function deleteAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage applications'))
            throw new \DF\Exception\PermissionDenied();
        
        $this->doNotRender();
        $this->validateToken($this->_getParam('csrf'));

        $app = Application::find($this->_getParam('id'));
        $app->delete();

        $this->flash('Application deleted successfully', 'green');
        $this->redirectToRoute(array('module'=>'management','controller'=>'applications'));
        return;
    }

    public function editAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage applications'))
            throw new \DF\Exception\PermissionDenied;
        
        $id = intval($this->_getParam('id'));
        $this->view->id = $id;
        
        if ($id != 0) $app = Application::find($id);
        
        $form = new \DF\Form($this->current_module_config->forms->application_edit);
        
        if ($app->id)
        {
            $form->setDefaults(array(
                'application_date' => ((trim($app->application_date) != '') ? date('m/d/Y h:i a', $app->application_date) : ''),
                'desired_move_in' => $app->desired_move_in,
                'requested_lease_length' => $app->requested_lease_length,
                'floor_preference' => $app->floor_preference,
                'special_needs' => $app->special_needs,
                'military_vet' => $app->military_vet,
                'police_fireprotection' => ($app->police_fireprotection == 1 ? 1 : 0),
                'application_status_id' => ($app->application_status_id == 1 ? 1 : 0),
            ));
            
            foreach($app->waiting_list as $choice)
            {
                $form->setDefault('type_'.$choice->choice_asset_type_id, $choice->choice_rank);
            }
            
            $offer = Offer::fetchByApplicationId($app->id);
            if ($offer->id)
                $form->setDefault('response_deadline', $offer->response_deadline);
            else
                $form->removeElement('response_deadline');
        }
        else
        {
            $app = new Application();
            $form->addElement('text', 'applicant_uin', array(
                'label' => 'Applicant UIN',
                'required' => true,
            ));
            $form->getElement('applicant_uin')->setOrder(-1);
        }

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (($id == 0) && (intval($data['applicant_uin']) > 0))
            {
                $resident = Resident::getByUin($data['applicant_uin']);
                if (!$resident)
                {
                    $user = User::getOrCreate($data['applicant_uin']);
                    $resident = new Resident();
                    $resident->uin = $data['applicant_uin'];
                    $resident->user_id = $user->id;
                    $resident->save();
                }
                $app->resident_id = $resident->id;
            }
            
            $app->application_date = (trim($data['application_date']) != '') ? strtotime($data['application_date']) : null;
            $app->desired_move_in = $data['desired_move_in'];
            $app->requested_lease_length = $data['requested_lease_length'];
            $app->floor_preference = $data['floor_preference'];
            $app->special_needs = $data['special_needs'];
            $app->military_vet = ((int)$data['military_vet'] == 1) ? 1 : 0;
            $app->police_fireprotection = ((int)$data['police_fireprotection'] == 1) ? 1 : 0;
            $app->application_status_id = (trim($data['application_status_id']) != '') ? (int)$data['application_status_id'] : null;
            
            // Apartment Style choices
            $asset_types = array();
            foreach($form->getDisplayGroup('apartment_types')->getElements() as $elem)
            {
                $type_parts = explode('_', $elem->getName());
                $value = $elem->getValue(); 
                if (((int)$type_parts[1] > 0) && ((int)$value > 0))
                    $asset_types[(int)$value] = (int)$type_parts[1];
            }

            $app->waiting_list->clear();
            foreach($asset_types as $choice_rank => $asset_type_id)
            {
                $app->waiting_list[$choice_rank - 1]->choice_asset_type_id = $asset_type_id;
                $app->waiting_list[$choice_rank - 1]->choice_rank = $choice_rank;
            }

            if ($offer->id && !empty($data['response_deadline']))
                $offer->response_deadline = $data['response_deadline'];

            try
            {
                $app->save();
                if ($offer->id)
                    $offer->save();

                if ($id == 0 && trim($data['applicant_uin'])!='')
                {
                    $this->flash('Application saved successfully. Please complete resident profile.');
                    $this->redirectToRoute(array('module'=>'management', 'controller'=>'residents', 'action' => 'edit', 'id' => $app->resident_id));
                }
                else
                {
                    $this->flash('Changes to the application made successfully');
                    $this->redirectToRoute(array('module'=>'management', 'controller'=>'applications'));
                }
            }
            catch(\Exception $e)
            {
                $this->alert($e->getMessage(), 'red');
            }
        }

        $this->view->headTitle('Add/Update Application');
        $this->renderForm($form);
    }

    public function addAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed('manage applications'))
            throw new \DF\Exception\PermissionDenied();
        
        $this->redirectToRoute(array('module'=>'management','controller'=>'applications','action'=>'edit','id'=>0));
    }

    public function offerAction()
    {
        $id = intval($this->_getParam('id'));
        $app = Application::find($id);
        
        $resident = Resident::find($app->resident_id);
        
        $compass = new \DF\Service\DoitApi;
        $student_record = $compass->getStudentById($resident->uin);
        
        // Eligibility Check
        if (!\DF\Acl::getInstance()->isAllowed('manage applications') && !$app->is_renewal)
        {
            if (isset($student_record['classification']))
            {
                $required_classification = Settings::getSetting('application_classification', 'X0');
                $required_year = intval(substr($required_classification, 1));
                
                if ($required_year != 0)
                {
                    $student_year = intval(substr($student_record['classification'], 1));
                    
                    if ($student_year < $required_year)
                        throw new \DF\Exception\DisplayOnly('Not Eligible for Offer: Classification does not meet the minimum requirements.');
                }
                
                if (substr($student_record['classification'],0,1) == 'U')
                {
                    $semester_gpa = floatval($student_record['gpa']);
                    $cumul_gpa = floatval($student_record['cumul']);
                    $grade_cutoff = Settings::getSetting('minimum_gpa');

                    if (($semester_gpa < $grade_cutoff) && ($cumul_gpa < $grade_cutoff))
                        throw new \DF\Exception\DisplayOnly('Not Eligible for Offer: GPA does not meet the minimum requirements.');
                }

                // TODO: conduct check
            }
            else
            {
                throw new \DF\Exception\DisplayOnly('Not Eligible for Offer: Applicant does not currently have a student record.');
            }
        }
        
        $form = new \DF\Form();

        if ($app->is_renewal)
        {
            $offerable = TRUE;

            $current_asset_id = $app->occupancy->asset->id;
            $current_asset_name = $app->occupancy->asset->name;

            $form->addElement('hidden', 'asset_id', array(
                'value' => 'id-'.$current_asset_id,
            ));
            $form->addElement('markup', 'asset_info', array(
                'label' => 'Apartment to Offer',
                'markup' => 'This offer is based on a renewal application and will apply to apartment <b>'.$current_asset_name.'</b>.',
            ));
        }
        else
        {
            if ($this->_getParam('match') === 'false')
            {
                $vacant = Asset::fetchAllVacant($app->desired_move_in);
                if (count($vacant) > 0)
                {
                    $available = array();
                    foreach($vacant as $v_asset)
                    {
                        $available_date = $v_asset->getAvailableDate();
                        $available[$available_date][] = $v_asset;
                    }
                    
                    asort($available);
                    foreach($available['now'] as $asset)
                    {
                        $occupancy_model = ($asset['AssetType']['occupancy_model_id'] == 2) ? 'Room ' : 'Apt ';
                        $note = (trim($asset['notes']) != '') ? ' Note: '.$asset['notes'] : '';
                        $offerable['id-'.$asset['id']] = $occupancy_model.'Available now: '.$asset['name'].' ('.$asset['AssetType']['description'].')'.$note;
                    }
                    unset($available['now']);
                    foreach($available as $available_date => $assets)
                    {
                        foreach($assets as $asset)
                        {
                            $occupancy_model = ($asset['AssetType']['occupancy_model_id'] == 2) ? 'Room ' : 'Apt ';
                            $note = (trim($asset['notes']) != '') ? ' Note: '.$asset['notes'] : '';
                            $offerable['id-'.$asset['id']] = $occupancy_model.'Available '.date('m/d/Y',strtotime($available_date)).': '.$asset['name'].' ('.$asset['AssetType']['description'].')'.$note;
                        }
                    }
                    $form->addElement('select', 'asset_id', array(
                        'label' => 'Available Units (listed by available date)',
                        'multiOptions' => $offerable,
                        'required' => true,
                    ));
                    // build app prefs chart for reference
                    $choices = $app->fetchWaitingListTypes();
                    ksort($choices);
                    $ranks = array();
                    foreach($choices as $pref => $type)
                    {
                        $asset_type = AssetType::find($type);
                        $occupancy_model = ($asset_type->occupancyModel->description) ? $asset_type->occupancyModel->description : '';
                        $ranks[$pref] = array(
                            'rank' => $app->calculateRank($type),
                            'description' => $asset_type->description,
                            'occupancy_model' => $occupancy_model,
                        );
                    }
                    $this->view->ranks = $ranks;
                }
                else
                {
                    $offerable = false;
                }
            }
            else
            {
                $offerable = $app->fetchWaitingListMatches();
                $form->addElement('radio', 'asset_id', array(
                    'label' => 'Available Units (listed by ranking)',
                    'multiOptions' => $offerable,
                    'required' => true,
                ));
            }
        }
        
        if (!$offerable)
            throw new \DF\Exception\DisplayOnly('No vacant units match applicant preferences.');
        
        $form->addElement('unixdate', 'move_in_date', array(
            'label' => 'Move-In Date',
            'required' => 'true',
        ));
        
        $form->addElement('unixdate', 'move_out_date', array(
            'label' => 'Move-Out Date',
            'description' => 'Enter a Move Out Date when offering a 9 or 12-month lease.',
        ));

        // Add contract data.
        $form->addElement('textarea', 'family_members', array(
            'label' => 'Family Members Occupying Apartment with Student Resident',
            'class' => 'full-width half-height',
        ));

        $form->addElement('text', 'rent_first_month', array(
            'label' => 'Pro-rated Housing Charge for First Month (due at occupancy)',
            'description' => 'Enter the full amount, including dollar sign (i.e. $123.45)',
        ));

        $form->addElement('radio', 'is_vacant', array(
            'label' => 'Is the apartment being offered currently vacant?',
            'multiOptions' => array(0 => 'No', 1 => 'Yes'),
            'description' => 'The apartment name will only be shown to the user if the apartment is currently vacant.',
        ));

        $form->addElement('submit', 'submit', array(
            'class' => 'ui-button',
            'helper' => 'formButton',
            'type' => 'submit',
            'label' => 'Send Offer',
        ));
        
        if ((!empty($_POST)) && $form->isValid($_POST))
        {
            $data = $form->getValues();
            
            $offer = new Offer();
            $asset_id_parts = explode('-',$data['asset_id']);

            $offer->asset_id = (int)$asset_id_parts[1];
            $offer->resident_id = $app->resident_id;
            $offer->offer_date = DF_TIME;
            $offer->move_in_date = $data['move_in_date'];
            $offer->move_out_date = $data['move_out_date'];
            $offer->response_deadline = strtotime('+2 days 5pm');
            $offer->application_id = $app->id;
            $offer->contract_data = $data;
            
            try
            {
                $offer->save();
                $app->application_status_id = 6;
                $app->save();
                
                // email offer to applicant
                \DF\Messenger::send(array(
                    'to' => array($app->resident->email, 'university-apartments@housing.tamu.edu'),
                    'subject' => 'Available unit in University Apartments',
                    'template' => 'apply/offer',
                    'vars' => array(
                        'resident' => $app->resident,
                        'asset' => Asset::find($offer->asset_id),
                        'move_in_date' => $offer->move_in_date,
                        'move_out_date' => $offer->move_out_date,
                        'response_deadline' => $offer->response_deadline,
                    ),
                ));
                
                $this->flash('Offer sent successfully');
                $this->redirectToRoute(array('module'=>'management', 'controller'=>'applications'));
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        
        $this->view->app = $app;
        $this->view->form = $form;
    }

    public function searchAction()
    {
        $searchterms = trim($this->getRequest()->getParam('searchterms'));
        
        $sortby = ($this->_getParam('sortby')) ? $this->_getParam('sortby') : null;
        $page = ($this->_getParam('page')) ? $this->_getParam('page') : 1;
        $sortdir = ($this->_getParam('sortdir')) ? $this->_getParam('sortdir') : 'asc';
        $perpage = ($this->_getParam('perpage')) ? $this->_getParam('perpage') : 25;
        if ($perpage == 'all') $perpage = -1;
        
        $this->_helper->viewRenderer('index');
        $this->view->searchterms = $searchterms;
        $this->view->residents = Application::search($page, $perpage, $sortby, $sortdir);
    }

    public function viewAction()
    {
        $id = $this->_getParam('id');
        $app = Application::find($id);
        if ($app instanceof Application)
        {
            $choices = $app->fetchWaitingListTypes();
            ksort($choices);
            $ranks = array();
            foreach($choices as $pref => $type)
            {
                $asset_type = AssetType::find($type);
                $occupancy_model = ($asset_type->occupancyModel->description) ? $asset_type->occupancyModel->description : '';
                $ranks[$pref] = array(
                    'rank' => $app->calculateRank($type),
                    'description' => $asset_type->description,
                    'occupancy_model' => $occupancy_model,
                );
            }
        }
        
        $vacant = Asset::fetchAllVacant($app->desired_move_in);
        if ($app->ApplicationFee->id)
        {
            if ((count($choices) > 0) && (is_array($vacant)) && (array_intersect($choices, $vacant)))
                $match = true;
            else
                $match = false;
        }
        
        $split_items = Split::fetchByResidentId($app->resident->id, Split::LEDGER_STANDARD);
        $has_paid_app_fee = FALSE;
        
        if ($split_items)
        {
            foreach($split_items as $split)
            {
                if ($split->Item->name == "Application Fee" && $split->matching_split_id)
                    $has_paid_app_fee = TRUE;
            }
        }
        $this->view->has_paid_app_fee = $has_paid_app_fee;
        
        $this->view->app = $app;
        $this->view->ranks = $ranks;
        $this->view->offer = \Entity\Offer::fetchByApplicationId($app->id);
        $this->view->match = $match;
    }
}