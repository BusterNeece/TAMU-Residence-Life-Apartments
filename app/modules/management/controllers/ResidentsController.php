<?php
use \Entity\Resident;
use \Entity\Occupancy;

class Management_ResidentsController extends \DF\Controller\Action
{
    public function indexAction()
    {
        $this->acl->checkPermission(array('view residents', 'manage residents'));

        // Handle search queries.
        if ($_GET)
            $this->redirectFromHere($_GET);

        $qb = $this->em->createQueryBuilder()
            ->select('r')
            ->from('Entity\Resident', 'r');

        if ($this->_hasParam('searchterms'))
        {
            $q = $this->_getParam('searchterms');
            $this->view->searchterms = $q;

            $qb->andWhere('(CONCAT(r.first_name, CONCAT(\' \', r.surname)) LIKE :q) OR (r.uin LIKE :q) OR (r.email LIKE :q)')->setParameter('q', '%'.$q.'%');
        }

        $page = $this->_getParam('page', 1);
        $pager = new \DF\Paginator\Doctrine($qb, $page);

        $this->view->pager = $pager;
    }

    public function deleteAction()
    {
        $this->acl->checkPermission(array('manage residents'));

        $this->doNotRender();
        $this->validateToken($this->_getParam('csrf'));

        $resident = Resident::find($this->_getParam('id'));
        $resident->delete();
        $this->flash('Resident deleted successfully');

        $this->redirectToRoute(array('module'=>'management','controller'=>'residents'));
        return;
    }

    public function viewAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();

        if ($user->resident->id != $resident->id)
            $this->acl->checkPermission(array('view residents', 'manage residents'));
        
        $occupancies = Occupancy::getActiveByResidentId($resident->id);

        if (!($resident instanceof Resident))
            throw new \DF\Exception\DisplayOnly('Resident not found!');

        $family = $resident->getFamilyMembers();
        if ($resident->isExternal())
            $this->view->external = true;

        $this->view->resident = $resident;
        $this->view->occupancies = $occupancies;
        $this->view->family = $family;
    }

    public function editAction()
    {
        $this->acl->checkPermission('manage residents');

        $id = intval($this->_getParam('id'));
        $this->view->id = $id;
        
        if ($id != 0)
            $resident = Resident::find($id);
        
        if (!$resident instanceof Resident)
            $resident = new Resident();

        $form = new \UA\Form\ResidentEdit($resident);

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            if( $form->save() )
            {
                $this->flash('Changes to the resident made successfully');
                $this->redirectToRoute(array('module'=>'management', 'controller'=>'residents', 'action'=>'view', 'id'=> $form->getResident()->id));
            }
            else
            {
                foreach( $form->getErrorMessages() as $error_msg )
                {
                    $this->alert($error_msg);
                }
            }
        }

        $this->view->form = $form;
    }

    public function addAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('manage residents')))
                throw new \DF\Exception\PermissionDenied();

        $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'edit','id'=>0));
    }

    public function editresidentinfoAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('resident_info_subform');
            foreach($subform->getElements() as $elem)
            {
                if ($elem instanceof Zend_Form_Element_Hidden)
                {
                    $elem->removeDecorator('label');
                    $elem->removeDecorator('htmlTag');
                }
            }

            $form = new \DF\Form();
            $form->addSubForm($subform, 'resident_info_subform')
                ->addElement('submit', 'submit', array(
                    'label' => 'Save Changes',
                    'attribs' => array(
                        'class' => 'ui-button',
                    ),
                ));

            if(!empty($_POST) && $form->isValid($_POST))
            {
                $data = $form->getValues();

                $resident->principal_resident = ($data['resident_info_subform']['principal_resident'] == '1') ? true : false;
                $resident->resident_type_id = $data['resident_info_subform']['resident_type_id'];
                $resident->laundry_card = $data['resident_info_subform']['laundry_card'];
                $resident->first_name = $data['resident_info_subform']['first_name'];
                $resident->middle_name = $data['resident_info_subform']['middle_name'];
                $resident->surname = $data['resident_info_subform']['surname'];
                $resident->suffix = $data['resident_info_subform']['suffix'];
                $resident->uin = $data['resident_info_subform']['uin'];
                $resident->uin_sub_id = $data['resident_info_subform']['uin_sub_id'];
                $resident->birth_date = (trim($data['resident_info_subform']['birth_date']) != '' ? $data['resident_info_subform']['birth_date'] : null);

                try
                {
                    $resident->save();
                    $this->flash('Resident info saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editfamilyinfoAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);
        $fid = intval($this->_getParam('fid'));
        $family_member = Resident::find($fid);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            if ($fid == 0) {
                $family_member = new Resident();
                $family_member->uin_sub_id = $resident->generateSubId();
                $family_member->uin = $resident->uin;
                $family_member->principal_resident = 0;
            }

            $bigform = new \UA\Form\ResidentEdit($family_member);
            $subform = $bigform->getSubForm('resident_info_subform');
            $subform->getElement('resident_type_id')
                ->setOptions(array(
                    'multiOptions' => array(
                        4 => 'Spouse',
                        6 => 'Daughter',
                        5 => 'Son',
                )));

            foreach($subform->getElements() as $elem)
            {
                if ($elem instanceof \Zend_Form_Element_Hidden)
                {
                    $elem->removeDecorator('label');
                    $elem->removeDecorator('htmlTag');
                }
            }

            $subform->addElement('text', 'secondary_uin', array(
                'label' => 'Family member UIN (optional, for non-lease holders)',
            ));

            $subform->setDefaults($family_member->toArray());
            $form = new \DF\Form();
            $form->addSubForm($subform, 'resident_info_subform')
                    ->addElement('submit', 'submit', array(
                        'label' => 'Save Changes',
                        'attribs' => array(
                            'class' => 'ui-button',
                        ),
                    ));

            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();

                $family_member->resident_type_id = $data['resident_info_subform']['resident_type_id'];

                $family_member->first_name = $data['resident_info_subform']['first_name'];
                $family_member->middle_name = $data['resident_info_subform']['middle_name'];
                $family_member->surname = $data['resident_info_subform']['surname'];
                $family_member->suffix = $data['resident_info_subform']['suffix'];
                $family_member->uin_sub_id = $data['resident_info_subform']['uin_sub_id'];
                $family_member->secondary_uin = $data['resident_info_subform']['secondary_uin'];
                $family_member->birth_date = (trim($data['resident_info_subform']['birth_date']) != '' ? $data['resident_info_subform']['birth_date'] : null);

                try
                {
                    $family_member->save();
                    if ($family_member->User->isActive())
                    {
                        $family_member->User->active = 0;
                        $family_member->User->save();
                    }
                    $this->flash('Family member info saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function deletefamilymemberAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));
        $this->doNotRender();

        $id = $this->_getParam('fid');
        $family_member = Resident::find($id);

        if ($family_member instanceof Resident)
        {
            try
            {
                $family_member->delete();
                $this->flash('Family member removed successfully');
            }
            catch(\Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        $this->redirectToReferrer();
    }

    public function editschoolinfoAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if (!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents')))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('school_info_subform');
            $form = new \DF\Form();
            $form->addSubForm($subform, 'school_info_subform')
                    ->addElement('submit', 'submit', array(
                        'label' => 'Save Changes',
                        'attribs' => array(
                            'class' => 'ui-button',
                        ),
                    ));

            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();

                $resident->classification = $data['school_info_subform']['classification'];
                $resident->college_id = ($data['school_info_subform']['college_id'] != 'null') ? $data['school_info_subform']['college_id'] : null;
                $resident->major_id = ($data['school_info_subform']['major_id'] != 'null') ? $data['school_info_subform']['major_id'] : null;

                try
                {
                    $resident->save();
                    $this->flash('School info saved successfully');
                    $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'view','id'=>$id));
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editcontactinfoAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('contact_info_subform');
            foreach($subform->getElements() as $elem)
            {
                if ($elem instanceof Zend_Form_Element_Hidden)
                {
                    $elem->removeDecorator('label');
                    $elem->removeDecorator('htmlTag');
                }
            }
            $form = new \DF\Form();
            $form->addSubForm($subform, 'contact_info_subform')
                    ->addElement('submit', 'submit', array(
                        'label' => 'Save Changes',
                        'attribs' => array(
                            'class' => 'ui-button',
                        ),
                    ));

            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();

                $resident->primary_phone = $data['contact_info_subform']['primary_phone'];
                $resident->secondary_phone = $data['contact_info_subform']['secondary_phone'];
                $resident->voip_phone = $data['contact_info_subform']['voip_phone'];
                $resident->email = $data['contact_info_subform']['email'];
                $resident->preferred_email = $data['contact_info_subform']['preferred_email'];

                try
                {
                    $resident->save();
                    $this->flash('Phone/email info saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editaddressAction()
    {
        $id = intval($this->_getParam('id'));
        $type = $this->_getParam('type');
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            switch($type)
            {
                case 'permanent':
                    $subform_name = 'permanent_address_subform';
                    break;
                case 'forwarding':
                    $subform_name = 'forwarding_address_subform';
                    break;
                case 'emergency':
                    $subform_name = 'emergency_contact_info_subform';
                    break;
                case 'emergency2':
                    $subform_name = 'emergency2_contact_info_subform';
                    break;
                default:
                case 'mailing':
                    $subform_name = 'mailing_address_subform';
            }
            
            $subform = $bigform->getSubForm($subform_name);
            $form = new \DF\Form();
            $form->addSubForm($subform, $subform_name)
                    ->addElement('submit', 'submit', array(
                        'label' => 'Save Changes',
                        'attribs' => array(
                            'class' => 'ui-button',
                        ),
                    ));

            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();

                switch($type)
                {
                    case 'permanent':
                        $resident->permanent_address = $data['permanent_address_subform']['permanent_address'];
                        $resident->permanent_address2 = $data['permanent_address_subform']['permanent_address2'];
                        $resident->permanent_city = $data['permanent_address_subform']['permanent_city'];
                        $resident->permanent_state = $data['permanent_address_subform']['permanent_state'];
                        $resident->permanent_zip = $data['permanent_address_subform']['permanent_zip'];
                        $resident->permanent_country = ($data['permanent_address_subform']['permanent_country'] != 'null') ? $data['permanent_address_subform']['permanent_country'] : null;
                        break;
                    case 'forwarding':
                        $resident->forwarding_address = $data['forwarding_address_subform']['forwarding_address'];
                        $resident->forwarding_address2 = $data['forwarding_address_subform']['forwarding_address2'];
                        $resident->forwarding_city = $data['forwarding_address_subform']['forwarding_city'];
                        $resident->forwarding_state = $data['forwarding_address_subform']['forwarding_state'];
                        $resident->forwarding_zip = $data['forwarding_address_subform']['forwarding_zip'];
                        $resident->forwarding_country = ($data['forwarding_address_subform']['forwarding_country'] != 'null') ? $data['forwarding_address_subform']['forwarding_country'] : null;
                        $resident->forwarding_comments = $data['forwarding_address_subform']['forwarding_comments'];
                        break;
                    case 'emergency':
                        $resident->emergency_missing_person = (trim($data['emergency_contact_info_subform']['emergency_missing_person'])!='') ? 1 : 0;
                        $resident->emergency_firstname = $data['emergency_contact_info_subform']['emergency_firstname'];
                        $resident->emergency_middlename = $data['emergency_contact_info_subform']['emergency_middlename'];
                        $resident->emergency_lastname = $data['emergency_contact_info_subform']['emergency_lastname'];
                        $resident->emergency_address = $data['emergency_contact_info_subform']['emergency_address'];
                        $resident->emergency_address2 = $data['emergency_contact_info_subform']['emergency_address2'];
                        $resident->emergency_city = $data['emergency_contact_info_subform']['emergency_city'];
                        $resident->emergency_state = $data['emergency_contact_info_subform']['emergency_state'];
                        $resident->emergency_zip = $data['emergency_contact_info_subform']['emergency_zip'];
                        $resident->emergency_country = ($data['emergency_contact_info_subform']['emergency_country'] != 'null') ? $data['emergency_contact_info_subform']['emergency_country'] : null;
                        $resident->emergency_phone = $data['emergency_contact_info_subform']['emergency_phone'];
                        $resident->emergency_relationship = $data['emergency_contact_info_subform']['emergency_relationship'];
                        break;
                    case 'emergency2':
                        $resident->emergency2_missing_person = (trim($data['emergency2_contact_info_subform']['emergency2_missing_person'])!='') ? 1 : 0;
                        $resident->emergency2_firstname = $data['emergency2_contact_info_subform']['emergency2_firstname'];
                        $resident->emergency2_middlename = $data['emergency2_contact_info_subform']['emergency2_middlename'];
                        $resident->emergency2_lastname = $data['emergency2_contact_info_subform']['emergency2_lastname'];
                        $resident->emergency2_address = $data['emergency2_contact_info_subform']['emergency2_address'];
                        $resident->emergency2_address2 = $data['emergency2_contact_info_subform']['emergency2_address2'];
                        $resident->emergency2_city = $data['emergency2_contact_info_subform']['emergency2_city'];
                        $resident->emergency2_state = $data['emergency2_contact_info_subform']['emergency2_state'];
                        $resident->emergency2_zip = $data['emergency2_contact_info_subform']['emergency2_zip'];
                        $resident->emergency2_country = ($data['emergency2_contact_info_subform']['emergency2_country'] != 'null') ? $data['emergency2_contact_info_subform']['emergency2_country'] : null;
                        $resident->emergency2_phone = $data['emergency2_contact_info_subform']['emergency2_phone'];
                        $resident->emergency2_relationship = $data['emergency2_contact_info_subform']['emergency2_relationship'];
                        break;
                    case 'mailing':
                    default:
                        $resident->mailing_address = $data['mailing_address_subform']['mailing_address'];
                        $resident->mailing_address2 = $data['mailing_address_subform']['mailing_address2'];
                        $resident->mailing_city = $data['mailing_address_subform']['mailing_city'];
                        $resident->mailing_state = $data['mailing_address_subform']['mailing_state'];
                        $resident->mailing_zip = $data['mailing_address_subform']['mailing_zip'];
                        $resident->mailing_country = ($data['mailing_address_subform']['mailing_country'] != 'null') ? $data['mailing_address_subform']['mailing_country'] : null;
                }

                try
                {
                    $resident->save();
                    $this->flash('Address saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editdemographicinfoAction()
    {
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();

        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('demographic_info_subform');
            foreach($subform->getElements() as $elem)
            {
                if ($elem instanceof Zend_Form_Element_Hidden)
                {
                    $elem->removeDecorator('label');
                    $elem->removeDecorator('htmlTag');
                }
            }

            $form = new \DF\Form();
            $form->addSubForm($subform, 'demographic_info_subform')
                ->addElement('submit', 'submit', array(
                    'label' => 'Save Changes',
                    'attribs' => array(
                        'class' => 'ui-button',
                    ),
                ));

            if(!empty($_POST) && $form->isValid($_POST))
            {
                $data = $form->getValues();

                $resident->birth_country = ($data['demographic_info_subform']['birth_country'] != 'null') ? $data['demographic_info_subform']['birth_country'] : null;
                $resident->gender = $data['demographic_info_subform']['gender'];
                $resident->marital_status = $data['demographic_info_subform']['marital_status'];
                $resident->number_of_children = $data['demographic_info_subform']['number_of_children'];
                $resident->marriage_certificate = (trim($data['demographic_info_subform']['marriage_certificate']) != '') ? $data['demographic_info_subform']['marriage_certificate'] : null;
                $resident->birth_certificate = (trim($data['demographic_info_subform']['birth_certificate']) != '') ? $data['demographic_info_subform']['birth_certificate'] : null;
                $resident->international_student = (trim($data['demographic_info_subform']['international_student'] != '')) ? 1: 0;

                try
                {
                    $resident->save();
                    $this->flash('Demographic info saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editmedicalinfoAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('manage residents')))
            throw new \DF\Exception\PermissionDenied();
        
        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);
        
        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('medical_info_subform');
            
            $form = new \DF\Form();
            $form->addSubForm($subform, 'medical_info_subform')
                ->addElement('submit', 'submit', array(
                    'label' => 'Save Changes',
                    'attribs' => array(
                        'class' => 'ui-button',
                    ),
                ));
                
            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();
                $section = $data['medical_info_subform'];
                
                $resident->fromArray($section);
                
                try
                {
                    $resident->save();
                    $this->flash('Medical info saved successfully');
                    $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'view','id'=>$id));
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }

    public function editnotesAction()
    {
        $this->acl->checkPermission('manage residents');

        $id = intval($this->_getParam('id'));
        $resident = Resident::find($id);
        
        if ($resident instanceof Resident)
        {
            $bigform = new \UA\Form\ResidentEdit($resident);
            $subform = $bigform->getSubForm('notes_subform');
            $form = new \DF\Form();
            $form->addSubForm($subform, 'notes_subform')
                ->addElement('submit', 'submit', array(
                    'label' => 'Save Changes',
                    'attribs' => array(
                        'class' => 'ui-button',
                    ),
                ));

            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();
                $resident->notes = $data['notes_subform']['notes'];
                try
                {
                    $resident->save();
                    $this->flash('Notes saved successfully');
                    $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'view','id'=>$id));
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }
    
    public function editvehicleAction()
    {
        $id = intval($this->_getParam('id'));
        $vid = intval($this->_getParam('vid'));
        $resident = Resident::find($id);

        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if ((!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents'))) && ($user->resident->id != $resident->id))
                throw new \DF\Exception\PermissionDenied();
        
        if ($resident instanceof Resident)
        {
            $form = new \DF\Form($this->current_module_config->forms->vehicle_edit);
            
            if ($vid > 0)
            {
                $vehicle = Vehicle::find($vid);
            }
            else
            {
                $vehicle = new Vehicle();
                $vehicle->resident_id = $id;
            }
            
            if ($vehicle instanceof Vehicle)
            {
                $form->setDefaults(array(
                    'make' => $vehicle->make,
                    'model' => $vehicle->model,
                    'color' => $vehicle->color,
                    'license_plate' => $vehicle->license_plate,
                    'registration_state' => $vehicle->registration_state,
                    'permit' => $vehicle->permit,
                ));
            }
            
            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();
                
                $vehicle->make = $data['make'];
                $vehicle->model = $data['model'];
                $vehicle->color = $data['color'];
                $vehicle->license_plate = $data['license_plate'];
                $vehicle->registration_state = $data['registration_date'];
                $vehicle->permit = $data['permit'];
                
                try
                {
                    $vehicle->save();
                    $this->flash('Vehicle saved successfully');
                    $this->redirectToReferrer();
                }
                catch(\Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }

        $this->renderForm($form);
    }
    
    public function deletevehicleAction()
    {
        $this->doNotRender();
        $this->validateToken($this->_getParam('csrf'));
        
        $vehicle = Vehicle::find($this->_getParam('vid'));

        try
        {
            $vehicle->delete();
            $this->flash('Vehicle deleted successfully');
        }
        catch(\Exception $e)
        {
            $this->alert('Error trying to delete vehicle: ' . $e->getMessage());
        }

        $this->redirectToReferrer();
    }

    public function blockaccountAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('manage residents')))
                throw new \DF\Exception\PermissionDenied();

        \DF\Csrf::validateToken($this->_getParam('csrf'));
        $this->doNotRender();

        $id = $this->_getParam('id');
        $resident = Resident::find($id);
        if ($resident instanceof Resident)
        {
            $user = $resident->User;
            if (!$resident->block_account)
            {
                $resident->block_account = DF_TIME;
                $user->active = 0;
                $msg = 'Resident account flagged for block';
            }
            else
            {
                $resident->block_account = 0;
                $user->active = 1;
                $msg = 'Block flag removed from resident account';
            }

            try
            {
                $user->save();
                $resident->save();
                $this->flash($msg);
            }
            catch(\Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }

        $this->redirectToReferrer();
    }

    public function applicationsAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('manage residents')))
                throw new \DF\Exception\PermissionDenied();

        $id = $this->_getParam('id');
        $resident = Resident::find($id);
        if ($resident instanceof Resident)
        {
            $all_apps = $this->em->createQuery('SELECT a, af FROM Entity\Application a LEFT JOIN a.application_fee af WHERE a.resident_id = :resident_id ORDER BY a.application_date DESC')
                ->setParameter('resident_id', $resident->id)
                ->getArrayResult();

            $paginator = \Zend_Paginator::factory((array)$all_apps);
            $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);

            $this->view->pager = $paginator;
            $this->view->num_records = count($all_apps);
            $this->view->resident = $resident;
        }
    }

    public function assetsAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('view residents', 'manage residents')))
                throw new \DF\Exception\PermissionDenied();

        $id = $this->_getParam('id');
        $resident = Resident::find($id);
        if ($resident instanceof Resident)
        {
            $all_records = $this->em->createQuery('SELECT o, a FROM Entity\Occupancy o LEFT JOIN o.asset a WHERE o.resident_id = :resident_id ORDER BY o.start_date DESC, o.end_date DESC')
                ->setParameter('resident_id', $resident->id)
                ->getArrayResult();

            $paginator = Zend_Paginator::factory((array)$all_records);
            $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);

            $this->view->pager = $paginator;
            $this->view->num_records = count($all_records);
            $this->view->resident = $resident;
        }
    }

    public function editquizAction()
    {
        if (!\DF\Acl::getInstance()->isAllowed(array('manage residents')))
                throw new \DF\Exception\PermissionDenied();
        
        $id = $this->_getParam('id');
        $resident = Resident::find($id);
        $quiz_id = $this->_getParam('quiz_id');
        $quiz = QuizResult::find($quiz_id);

        $form = new \DF\Form($this->current_module_config->forms->quiz_result);

        $occupancies = Occupancy::getActiveByResidentId($resident->id);
        foreach($occupancies as $occupancy)
        {
            if (!$occupancy->end_date)
                $current_occupancy = $occupancy;
        }

        if ($quiz instanceof QuizResult)
        {
            $form->setDefaults($quiz->toArray());
            $form->setDefault('occupancy_id', $current_occupancy->id);
        }
        else
        {
            $quiz = new QuizResult();
            $form->setDefaults(array(
                'resident_id' => $resident->id,
                'occupancy_id' => $current_occupancy->id,
                'timestamp' => DF_TIME,
            ));
        }

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();

            $quiz->quiz_id = $data['quiz_id'];
            $quiz->timestamp = $data['timestamp'];
            $quiz->resident_id = $data['resident_id'];
            
            try
            {
                $quiz->save();
                if ($current_occupancy instanceof Occupancy)
                {
                    $current_occupancy->orientation_date = $data['timestamp'];
                    $current_occupancy->save();
                }
                $this->flash('Quiz saved successfully');
                $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'view','id'=>$id));
            }
            catch(\Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }

        $this->renderForm($form);
    }

    public function deletequizAction()
    {
        $this->doNotRender();
        $this->validateToken($this->_getParam('csrf'));

        $quiz = QuizResult::find($this->_getParam('quiz_id'));

        try
        {
            $quiz->delete();
            $this->flash('Quiz Result deleted successfully');
        }
        catch(\Exception $e)
        {
            $this->alert('Error trying to delete Quiz Result: ' . $e->getMessage());
        }

        $this->redirectToRoute(array('module'=>'management','controller'=>'residents','action'=>'view','id'=>$this->_getParam('id')));
        return;
    }
}