<?php
use \Entity\Occupancy;
use \Entity\MaintenanceWorkorder;
use \Entity\Split;

class IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return true;
    }

    /**
     * Main display.
     */
    public function indexAction()
    {
        $available_views = array();
        $view_names = array(
            'admin'     => 'Administration',
            'resident'  => 'Resident',
            'applicant' => 'Applicant',
            'anonymous' => 'Guest',
        );
        
        if ($this->acl->isAllowed('is logged in'))
        {
            $user = $this->auth->getLoggedInUser();
            
            if ($this->acl->isAllowed('view administration items'))
                $available_views[] = 'admin';
            
            if ($user->resident_id)
            {
                $occupancies = Occupancy::getActiveByResidentId($user->resident_id);
                
                if ($occupancies)
                    $available_views[] = 'resident';
                else
                    $available_views[] = 'applicant';
            }
            else
            {
                $available_views[] = 'applicant';
            }
        }
        else
        {
            $available_views[] = 'anonymous';
        }
       
        $current_view = $this->_getParam('view', current($available_views));
        
        if (!in_array($current_view, $available_views))
            $current_view = current($available_views);
        
        // Show tabs at the top (if applicable)
        if (count($available_views) > 1)
        {
            $tab_info = array(
                'available_views'       => $available_views,
                'current_view'          => $current_view,
                'view_names'            => $view_names,
            );
            $this->view->assign($tab_info);
            
            $tabs_path = $this->getViewScript('index_tabs');
            $this->view->layout()->tabs = $this->view->render($tabs_path);
        }
        
        switch($current_view)
        {
            case "admin":
                // Pull maintenance requests.
                $maintenance_by_status = MaintenanceWorkorder::fetchTotalsByStatus();
				$this->view->maintenance_requests = $maintenance_by_status[MaintenanceWorkorder::STATUS_UNPROCESSED];
                $this->view->maintenance_active = $maintenance_by_status['active'];
                
                // Get move-out requests
                $move_outs = Occupancy::fetchMoveOuts(array('start' => strtotime('today'), 'end' => strtotime('+30 days')));
                $move_outs_today = Occupancy::fetchMoveOuts(array('start' => strtotime('today'), 'end' => strtotime('tomorrow')));
                $this->view->move_out_total = count($move_outs);
                $this->view->move_outs_today = count($move_outs_today);
                
				$this->render('index_administrator');
                return;
                
            break;
            
            case "resident":
                $this->view->resident = $user->resident;
                $this->view->external = $user->resident->isExternal();
                
                // Get current balance due.
                $this->view->balance = Split::getBalanceByResidentId($user->resident_id, Split::LEDGER_STANDARD);
                $this->view->block_account = $user->resident->block_account;
                
                $this->view->occupancies = $occupancies;
                
                // Assemble a list of work orders.
                $workorders = array();
                foreach($occupancies as $occupancy)
                {
                    $asset = $occupancy->asset;
                    $asset_workorders = MaintenanceWorkorder::fetchByAssetId($asset_id, MaintenanceWorkorder::getActiveStatusCodes());
                    
                    if ($asset_workorders)
                        $workorders = array_merge($workorders, $asset_workorders->toArray());
                }
                $this->view->workorders = $workorders;
                
                // look for roommate requests
                $this->view->roommates = $user->resident->getRoommateRequests();

                // look for offers
                $this->view->offer = \Entity\Offer::fetchMostRecentByResidentId($user->resident->id);
                
                $this->render('index_resident');
                return;
                
            break;
            
            case "applicant":
                
                if ($user)
                {
                    $this->view->resident = $user->resident;
                    $this->view->external = $user->resident->isExternal();
                    
                    // Get current balance due.
                    if ($user->resident->id)
                    {
						$this->view->balance = Split::getBalanceByResidentId($user->resident->id, Split::LEDGER_STANDARD);
						$this->view->block_account = $user->resident->block_account;
					}
                    
                    if ($user->resident->Applications->count() > 0)
                    {
                        $this->view->applications = $user->resident->Applications;
                        $this->view->deposit_balance = abs(Split::getBalanceByResidentId($user->resident->id));
                        $this->view->orientation_date = $user->resident->orientation_date;
                    }
                }
                
                $this->render('index_applicant');
                return;
                
            break;
            
            case "anonymous":
            default:
                
                $this->render();
                return;
                
            break;
        }
    }
	
	public function searchAction()
	{
		$this->doNotRender();
		
		$query = $this->_getParam('term');
		
		$result = array();
		
		if ($query)
		{
			$assets = Doctrine_Query::create()
				->from('Asset a')
                                ->addWhere('a.asset_status_id = ?', 1)
				->addWhere('a.name LIKE ?', $query.'%')
				->fetchArray();
			
			if ($assets)
			{
				foreach($assets as $asset)
				{
					$result[] = $asset['name'];
				}
			}
		}
		
		echo Zend_Json::encode($result);
		return;
	}

    public function moveoutAction()
    {
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in'))
        {
            $user = $auth->getLoggedInUser();
            $form = new \DF\Form($this->current_module_config->forms->moveout);

            if (($_POST) && $form->isValid($_POST))
            {
                $data = $form->getValues();

                $moveout = $data['desired_move_out'];
                $threshold = strtotime('+30 days');
                if ($moveout < $threshold)
                {
                    $form->getElement('desired_move_out')
                            ->addError('Move out day must be at least 30 days in the future');
                }
                else
                {
                    $occupancy = Occupancy::find($this->_getParam('id'));
                    if ($occupancy instanceof Occupancy)
                    {
                        $occupancy->moveoutdate = $moveout;
                        $occupancy->moveout_reason = $data['moveout_reason'];
                        $occupancy->end_date = $moveout;
                        try
                        {
                            $occupancy->save();
                            $this->flash('Move Out notification saved successfully. Please print and sign the following form and bring it to the University Apartments office.');

                            // email confirmation
                            \DF\Messenger::send(array(
                                'to' => array($user->resident->email, 'university-apartments@housing.tamu.edu'),
                                'subject' => 'Thank you submitting your Intent to Vacate',
                                'template' => 'moveout/reminders',
                                'vars' => array(
                                    'resident' => $user->resident,
                                    'asset' => Asset::find($occupancy->asset_id),
                                ),
                            ));

                            $this->redirectFromHere(array('action' => 'printmoveout', 'id' => $this->_getParam('id')));
                        }
                        catch(Doctrine_Exception $e)
                        {
                            $this->alert($e->getMessage());
                        }
                    }
                }
            }

            $this->view->form = $form;
        }
    }

    public function printmoveoutAction()
    {
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in'))
        {
            $user = $auth->getLoggedInUser();
            $resident = Resident::getFromCurrentUser();
            $occupancy = Occupancy::find($this->_getParam('id'));
            
            $this->view->resident = $resident;
            $this->view->occupancy = $occupancy;
            $this->view->mode = 'edit_profile'; // to provide access to forwarding address
        }
    }

    public function moveoutappointmentAction()
    {
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in'))
        {
            $user = $auth->getLoggedInUser();
            $resident = Resident::getFromCurrentUser();
            $occupancy = Occupancy::find($this->_getParam('id'));
            $end_date = intval($occupancy->end_date);
            $limit = MoveoutAppointmentLimit::fetchLimitByDate($end_date);
            $all_appointments = MoveoutAppointment::fetchByDate($end_date);

            $this->view->limit = $limit;
            $this->view->appointments = $all_appointments;
            $this->view->resident = $resident;
            $this->view->occupancy = $occupancy;
            $this->view->date = $end_date;

            $this->view->is48hrsahead = ($end_date - strtotime('today') < 172800) ? false : true;
        }
    }
    
    public function moveinAction()
    {
        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in'))
        {
            $user = $auth->getLoggedInUser();
            $resident = Resident::getFromCurrentUser();
            if ($resident->quiz_results->count() < 1)
            {
                $this->alert('You must complete the orientation quizzes before scheduling a move in appointment');
                $this->redirectHome();
            }
            
            $occupancy = Occupancy::find($this->_getParam('id'));
            $movein = MoveoutAppointment::fetchMoveinByOccupancyId($occupancy->id);
            
            if ($movein->id)
            {
                $this->view->movein = $movein;
                $date = $movein->date;
                $this->view->is48hrsahead = ($date - strtotime('today') < 172800) ? false : true;
                $this->view->mydate = $date;
                $this->view->myslot = $movein->slot;
            }
            else
            {
                $this->view->is48hrsahead = true;
            }
            
            if ($this->_hasParam('date'))
            {
                $start_date = intval($this->_getParam('date'));
                $end_date = strtotime('+6 days', $start_date);
            }
            else
            {
                //$start_date = strtotime('today');
                //$end_date = strtotime('+6 days');
                
                // Gardens Phase II dates 8/20-8/28
                $start_date = strtotime('2011-08-20');
                $end_date = strtotime('2011-08-28');
            }
            
            $all_appointments = MoveoutAppointment::fetchByDate($start_date, $end_date, true, true);

            $dates = array();
            while($start_date <= $end_date)
            {
                $dates[] = $start_date;
                $start_date = strtotime('+1 day', $start_date);
            }
            
            $this->view->start_time = 800;
            $this->view->end_time = 1930;
            $this->view->limit = 6;
            $this->view->dates = $dates;
            $this->view->appointments = $all_appointments;
            $this->view->resident = $resident;
            $this->view->occupancy = $occupancy;            
        }
    }

    public function saveappointmentAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));

        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in') && $this->_hasParam('date'))
        {
            $date = $this->_getParam('date');
            $slot = $this->_getParam('slot');
            $user = $auth->getLoggedInUser();
            $resident = Resident::getFromCurrentUser();
            $occupancy_id = $this->_getParam('occupancy_id');

            $appt = new MoveoutAppointment();
            $appt->resident_id = $resident->id;
            $appt->occupancy_id = $occupancy_id;
            $appt->date = $date;
            $appt->slot = $slot;

            try
            {
                $appt->save();
                $this->flash('Appointment successfully scheduled');
                $this->redirectHome();
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert('Error: Could not schedule appointment');
            }
        }
    }
    
    public function savemoveinAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));

        $auth = \DF\Auth::getInstance();
        $acl = \DF\Acl::getInstance();

        if ($acl->isAllowed('is logged in') && $this->_hasParam('date'))
        {
            $date = (int)$this->_getParam('date');
            $slot = $this->_getParam('slot');
            $resident_id = (int)$this->_getParam('resident_id');
            $occupancy_id = (int)$this->_getParam('occupancy_id');
            $movein = (int)$this->_getParam('movein');
            $movein_id = ($this->_hasParam('movein_id')) ? $this->_getParam('movein_id') : null;
            
            // check against limit
            $appts = MoveoutAppointment::fetchByDate($date, null, true, true);
            if (count($appts[(string)$date][str_pad($slot, 4, '0', STR_PAD_LEFT)]) >= 6)
            {
                $this->alert('Error: That appointment slot is full, please select another');
            }
            else
            {
                // look for existing appointment
                if ($movein_id)
                    $appt = MoveoutAppointment::find($movein_id);
                // otherwise make new appointment
                if (!$appt->id)
                    $appt = new MoveoutAppointment();
                
                $appt->resident_id = $resident_id;
                $appt->occupancy_id = $occupancy_id;
                $appt->date = $date;
                $appt->slot = $slot;
                $appt->movein = $movein;
                
                try
                {
                    $appt->save();
                    $this->flash('Appointment successfully scheduled');
                    $this->redirectToReferrer();
                }
                catch(Doctrine_Exception $e)
                {
                    $this->alert('Error: '.$e->getMessage());
                }
            }
        }
    }

    public function skipwalkthroughAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));
        $occupancy = Occupancy::find($this->_getParam('id'));
        $occupancy->skip_walkthrough = 1;
        try
        {
            $occupancy->save();
            $this->flash('You have opted out of the walkthrough step');
            $this->redirectHome();
        }
        catch(Doctrine_Exception $e)
        {
            $this->alert('Error: '.$e->getMessage());
        }
    }
	
	public function bugreportAction()
	{
		$bc = new DF_Service_Basecamp();

		// Locate the "Bug Reports" to-do list.
		$response = $bc->getTodoListsForProject($bc->getProjectId());
		
		foreach($response['body']->{'todo-list'} as $list)
		{
			$list_name = (string)$list->name;
		
			if ($list_name == 'Bug Reports')
				$bug_report_list_id = (int)$list->id;
		}
		
		if ($bug_report_list_id)
		{
			$user = \DF\Auth::getInstance()->getLoggedInUser();
			
			$reporter_name = ($user instanceof User) ? $user->firstname.' '.$user->lastname.' ('.$user->username.')' : 'Anonymous User';
                        if (trim($user->email) != '') $reporter_name .= ' '.$user->email;
			$bug_report_name = date('m/d/Y g:ia').' Bug Report ('.$reporter_name.'): '.$_REQUEST['description'];
			
			$bug_report = array();
			$bug_report[] = 'Submitter: '.$reporter_name;
			$bug_report[] = 'Description of Bug: '.$_REQUEST['description'];
			$bug_report[] = 'Referring URL: '.$_SERVER['HTTP_REFERER'];
			$bug_report[] = 'Submitter IP: '.$_SERVER['REMOTE_ADDR'];
			
			$bug_report_object = $bc->createTodoItemForList($bug_report_list_id, $bug_report_name,'');
			
			if ($bug_report_object['id'])
			{
				$bc->createCommentForTodoItem($bug_report_object['id'], implode("<br />", $bug_report));
			}
		}
		
		$this->alert('Web Issue Report posted! Thank you for your assistance.');
		$this->redirectFromHere(array('action' => 'index'));
	}
}