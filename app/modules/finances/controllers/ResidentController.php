<?php
/**
 * Resident finance viewer
 */

class Finances_ResidentController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    /**
     * Main display.
     */
    public function indexAction()
    {
        $this->redirectFromHere(array('action' => 'ledger'));
    }
    
    /**
     * Transaction history (ledger) view
     */
    public function ledgerAction()
    {
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if (!$user->resident->id)
            throw new \DF\Exception\DisplayOnly('You are not currently listed as a resident!');
        
        $this->view->ledger_id = $ledger_id = ($this->_hasParam('ledger')) ? (int)$this->_getParam('ledger') : Split::LEDGER_STANDARD;
        $this->view->resident = $user->resident;
        
        $transactions = Split::fetchByResidentId($user->resident->id, $ledger_id);
        $this->view->transactions = $transactions;
    }
    
    /**
     * Pay rent/deposits online view
     */
    public function payAction()
    {
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        if (!$user->resident->exists())
            throw new \DF\Exception\DisplayOnly('You are not currently listed as a resident!');
        
        $resident = $user->resident;
        
		/* Handle return from the TouchNet payment system. */
        $ext_trans_id = $this->_getParam('EXT_TRANS_ID');
        if ($ext_trans_id)
        {
            if ($TN_transaction->payment_status === 'success' || $this->_getParam('origin') == "success")
			{	
				$this->flash('Payment received! Thank you.');
				$this->redirectToRoute(array('module' => 'default', 'controller' => 'index'));
				return;
            }
        }
		
		/*
		 * Require the user to choose a payment method.
		 
		if (!$this->_hasParam('payment_method'))
		{
			$this->render('pay_select');
			return;
		}
		 */

        if ((int)$resident->block_account != 0)
            throw new \DF\Exception\DisplayOnly('Your account has a pending block that requires your attention. Please contact the University Apartments main office for more information.');
        
        /* Check for external user status. */
        if ($resident->isExternal())
            throw new \DF\Exception\DisplayOnly('You are currently listed as an external resident. Your billing is processed through your student account. For more information on any outstanding charges, visit <a href="http://howdy.tamu.edu" target="_blank">the Howdy Portal</a>.');
        
        /* Generate balance due. */
		$current_transactions = Split::fetchByResidentId($user->resident->id, Split::LEDGER_STANDARD, TRUE);
		$this->view->current_transactions = $current_transactions;
		
		$current_balance = Split::getBalanceForItems($current_transactions);
		if ($current_balance >= 0)
			throw new \DF\Exception\DisplayOnly('You do not currently have any outstanding charges.');
        
        /* Check for NSF or otherwise special-processed fees in the balance. */
        $nsf_item = Item::fetchByName('NSF Check Fee');
        
        $exempt_fees = array();
        $security_deposit_item = Item::fetchByName('Security Deposit');
        $exempt_fees[] = $security_deposit_item->id;
        $application_fee_item = Item::fetchByName('Application Fee');
        $exempt_fees[] = $application_fee_item->id;
        
        $has_exempt_fee = FALSE;
        
		foreach($current_transactions as $split)
		{
			if ($split->item_id == $nsf_item->id)
				throw new \DF\Exception\DisplayOnly('You currently have an NSF Fee on your account. You cannot pay this charge online. Please visit the University Apartments office.');
            
            if (in_array($split->item_id, $exempt_fees))
                $has_exempt_fee = TRUE;
		}
        
        /* Check for at least one active occupancy. */
        $active_occupancy = Occupancy::getActiveByResidentId($user->resident->id);
        
        if(count($active_occupancy) == 0 && !$has_exempt_fee)
            throw new \DF\Exception\DisplayOnly('You are not currently listed as an active resident of an apartment. Contact the University Apartments office for information about paying your outstanding balance.');
        
        // Add to amount due depending on payment method.
		$amount_to_pay = 0-$current_balance;
		
		// $payment_method = $this->_getParam('payment_method');
		$payment_method = 'check';
		
		if ($payment_method == "credit")
		{
			$service_charges = (float)Settings::getSetting('touchnet_service_charge', 0) + (float)Settings::getSetting('touchnet_credit_charge', 0);
			$amount_to_pay += $service_charges;
		}
		else
		{
			$service_charges = (float)Settings::getSetting('touchnet_service_charge', 0);
			$amount_to_pay += $service_charges;
		}
		
		$this->view->service_charges = $service_charges;
		$this->view->amount_to_pay = $amount_to_pay;
		
		$touchnet_data = DF_Service_TouchNet::createTransaction($amount_to_pay, $user->resident->id);
		$this->view->transaction = $touchnet_data;
		
		// Assign the outstanding transactions to the payment.
		$splits = array();
		foreach($current_transactions as $split)
		{
			$touchnet_split = new TouchnetSplits();
			$touchnet_split->touchnet_id = $touchnet_data['record']->id;
			$touchnet_split->split_id = $split->id;
			$touchnet_split->save();
		}
    }

    public function blockedAction()
    {
        $filter_form = new \DF\Form($this->current_module_config->forms->filter_daterange);

        if( ($_POST) && ($filter_form->isValid($_POST)) )
        {
            $values = $filter_form->getValues();

            $values = array_merge($this->getRequest()->getUserParams(), $values);

            if( !$values['start'] )
                unset($values['start']);

            if( !$values['end'] )
                unset($values['end']);

            $this->redirect(\DF\Url::route($values));
        }
        $filters = array(
            'start' => $this->_getParam('start'),
            'end' => $this->_getParam('end'),
        );
        $filter_form->setDefaults($filters);

        $all_records = Resident::fetchBlocked($filters);

        if (strtolower($this->getRequest()->getParam('format')) == "csv")
        {
            $this->doNotRender();
            DF_Export::exportToCSV($all_records, TRUE);
            return;
        }
        else
        {
            $paginator = Zend_Paginator::factory((array)$all_records);
            $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);

            $this->view->pager = $paginator;
            $this->view->num_records = count($all_records);
            $this->view->filter_form = $filter_form;
        }
    }
}