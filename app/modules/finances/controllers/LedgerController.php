<?php
class Finances_LedgerController extends \DF\Controller\Action
{
    public function permissions()
    {
        if ($this->_getParam('action') == "post")
            $permission_name = ($this->_getParam('credit_or_debit') == "C") ? "create credits" : "create debits";
        else
            $permission_name = 'view ledgers';
        
        return \DF\Acl::getInstance()->isAllowed($permission_name);
    }

    /* Main display. */
    public function indexAction()
    {
        $id = $this->_getParam('id');
        $ledger_id = ($this->_hasParam('ledger')) ? (int)$this->_getParam('ledger') : Split::LEDGER_STANDARD;
        
        $this->view->ledger_id = $ledger_id;        
        $this->view->resident = Resident::find($id);
        
        $transactions = Split::fetchByResidentId($id, $ledger_id);
        
        $rolling_balance = 0;
        $totals = array();
        foreach($transactions as $split)
        {
            $rolling_balance += $split->split_amount;
            $totals[$split->id] = $rolling_balance;
        }
        $this->view->totals = $totals;

        $this->view->transactions = $transactions;
    }
    
    /* Post an item to the ledger. */
    public function postAction()
    {
        if (!$this->_hasParam('credit_or_debit'))
        {
            throw new \DF\Exception\DisplayOnly('Credit or debit not specified! You must specify this option before visiting this page.');
        }
        
        if (!$this->_hasParam('resident_id') || !$this->_hasParam('ledger_id'))
        {
            $this->view->residents = Resident::fetchSelect();
            $this->view->ledger_types = Split::getLedgerIds();
            
            $this->render('post_select');
        }
        else
        {
            $resident_id = (int)$this->_getParam('resident_id');
            $credit_or_debit = ($this->_getParam('credit_or_debit') == "C") ? "C" : "D";
            $ledger_id = (int)$this->_getParam('ledger_id');
			
			$this->view->resident = Resident::find($resident_id);
            
            $form_config = $this->current_module_config->forms->transaction_post->form->toArray();
            
            if ($credit_or_debit == "C")
            {
                unset($form_config['elements']['recurring_type']);
                unset($form_config['elements']['recurring_active']);
                
                unset($form_config['elements']['due_date']);
                unset($form_config['elements']['asset_id']);
                
                if ($ledger_id == Split::LEDGER_DEPOSITS)
                {
                    unset($form_config['elements']['matching_split_ids']);
                }
                else
                {
                    unset($form_config['elements']['split_amount']);
                    unset($form_config['elements']['item_id']);
                    
                    // Get all splits that this item can cover.
                    $eligible_splits = Doctrine_Query::create()
                        ->from('Split s')
                        ->leftJoin('s.Transaction t')
                        ->addWhere('s.ledger_id = ?', $ledger_id)
                        ->addWhere('s.credit_or_debit = ?', 'D')
                        ->addWhere('s.resident_id = ?', $resident_id)
                        ->addWhere('s.matching_split_id = 0 OR s.matching_split_id IS NULL')
                        ->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
                    
                    if ($eligible_splits)
                    {
                        $split_list = array();
                        foreach($eligible_splits as $split)
                        {
                            $split_id = $split['id'];
                            $split_name = $split['Transaction']['memo'];
                            $split_amount = '$'.number_format($split['split_amount'], 2);
                            
                            $split_list[$split_id] = $split_amount.' - '.$split_name;
                        }
                        
                        $form_config['elements']['matching_split_ids'][1]['multiOptions'] = $split_list;
                    }
                    else
                    {
                        throw new \DF\Exception\DisplayOnly('There are no existing transactions that this credit can cover.');
                    }
                }
            }
            else
            {
                unset($form_config['elements']['matching_split_ids']);
                unset($form_config['elements']['check_reference_number']);
                unset($form_config['elements']['register_id']);
                unset($form_config['elements']['payment_method_id']);
            }
			
			$form_defaults = array(
				'date' => date('m/d/Y'),
                'recurring_type' => Split::RECURRING_INACTIVE,
                'recurring_active' => 0,
			);
			
			// Set register ID if applicable.
			$user = \DF\Auth::getInstance()->getLoggedInUser();
			if ($user->register_id)
			{
				$form_defaults['register_id'] = (int)$user->register_id;
			}
            
            $form = new \DF\Form($form_config);
            $form->setDefaults($form_defaults);
            
            $this->view->form = $form;
            
            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();
                
                // Post the new transaction first.
                $transaction = new RegisterTransaction();
                $transaction->posted = time();
                
                $transaction->date = $data['date'];
                $transaction->memo = $data['memo'];
                
                $transaction->check_reference_number = $data['check_reference_number'];
                
                if (isset($data['register_id']) && $data['register_id'] !== "null")
                    $transaction->register_id = $data['register_id'];
                
                if (isset($data['transaction_source_id']) && $data['transaction_source_id'] !== "null")
                    $transaction->transaction_source_id = $data['transaction_source_id'];
				
				if (isset($data['asset_id']) && $data['asset_id'] !== "null")
					$transaction->asset_id = $data['asset_id'];
                
                $transaction->save();
                $transaction_id = $transaction->id;
                
                $user = \DF\Auth::getInstance()->getLoggedInUser();
                
                $splits = array();
                $total_amount = 0;
                
                // Post the splits as determined by the transaction debit/credit type.
                if ($credit_or_debit == "C" && $ledger_id != Split::LEDGER_DEPOSITS)
                {
                    foreach($data['matching_split_ids'] as $matching_split_id)
                    {
                        $matching_split = Split::find($matching_split_id);
                        
                        $split = new Split();
                        $split->transaction_id = $transaction_id;
                        $split->resident_id = $resident_id;
                        $split->user_id = $user->id;
                        $split->ledger_id = $ledger_id;
                        $split->credit_or_debit = $credit_or_debit;
                        $split->item_id = $matching_split->item_id;
                        $split->matching_split_id = $matching_split_id;
                        $split->split_amount = 0-$matching_split->split_amount;
						
						if ($matching_split->asset_id)
							$split->asset_id = $matching_split->asset_id;
                        
                        if (isset($data['payment_method_id']))
                            $split->payment_method_id = $data['payment_method_id'];
                        
                        $split->save();
                        
                        $splits[] = $split;
                        $total_amount += $split->split_amount;
                        
                        $new_split_id = $split->id;
                        
                        // Re-fetch the matching split because of the new floating balance.
                        $matching_split->matching_split_id = $new_split_id;
                        $matching_split->save();
                    }
                }
                else
                {
                    $split = new Split();
                    $split->transaction_id = $transaction_id;
                    $split->resident_id = $resident_id;
                    $split->ledger_id = $ledger_id;
                    $split->user_id = $user->id;
                    $split->credit_or_debit = $credit_or_debit;
                    $split->item_id = (int)$data['item_id'];
					
					if ($transaction->asset_id)
						$split->asset_id = $transaction->asset_id;
					
                    if ($data['due_date'])
                        $split->due_date = $data['due_date'];
                    
                    if ($credit_or_debit == "C")
                        $split->split_amount = $data['split_amount'];
                    else
                        $split->split_amount = 0-$data['split_amount'];
                    
                    if ($data['payment_method_id'])
                        $split->payment_method_id = $data['payment_method_id'];
                    
                    if ($data['recurring_active'] == 1)
                    {
                        $split->recurring_active = 1;
                        $split->recurring_type = $data['recurring_type'];
                    }
                    else
                    {
                        $split->recurring_active = 0;
                        $split->recurring_type = Split::RECURRING_INACTIVE;
                    }
                    
                    $split->save();
                    
                    $splits[] = $split;
                    $total_amount += $split->split_amount;
                }
                
                Split::updateRunningBalance($resident_id, $ledger_id);
                
                $this->view->resident = Resident::find($resident_id);
                $this->view->transaction = $transaction;
                
                $this->view->total_amount = $total_amount;
                $this->view->splits = $splits;
                
                $this->view->ledger_id = $ledger_id;
                
                $this->view->record = $data;
                $this->render('post_receipt');
            }
            
            $this->view->form = $form;
        }
    }
}