<?php
/**
 * Register log controller
 */

class Finances_RegisterController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('create credits');
    }
    
    public function indexAction()
    {
        if (!$this->_hasParam('register_id'))
        {
            // Select all registers except the Internet register.
            $registers = Register::fetchSelect();
            unset($registers[0]);
            $this->view->registers = $registers;
            
            $this->render('index_select');
        }
        else
        {
            $register_id = (int)$this->_getParam('register_id');
            
            $form = new \DF\Form($this->current_module_config->forms->register_log->form);
            
            if( !empty($_POST) && $form->isValid($_POST) )
            {
                $data = $form->getValues();
                
                $record = new RegisterReport();
                $record->register_id = $register_id;
                $record->register_batch = 'O';
                $record->timestamp = time();
                $record->amount_cash = (float)$data['amount_cash'];
                $record->amount_check = (float)$data['amount_check'];
                $record->notes = $data['notes'];
                
                try
                {
                    $record->save();
                    $this->flash('Register log update submitted.');
                }
                catch(Doctrine_Exception $e)
                {
                    $this->alert($e->getMessage());
                }
				
                $this->redirectToRoute(array('module' => 'default', 'controller' => 'index'));
				return;
            }
            
            // Get the most recent register post.
            $most_recent_log = Doctrine_Query::create()
                ->from('RegisterReport rr')
                ->addWhere('rr.register_id = ?', $register_id)
                ->orderBy('rr.timestamp DESC')
                ->fetchOne();
            
            if ($most_recent_log)
            {
                $timestamp = date('F j, Y g:ia', $most_recent_log->timestamp);
                $this->alert('Your most recent register batch was posted on '.$timestamp.'.');
            }
            
            $this->view->form = $form;
        }
    }
}