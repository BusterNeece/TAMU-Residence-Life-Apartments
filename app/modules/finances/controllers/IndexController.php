<?php
class Finances_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    /**
     * Main display.
     */
    public function indexAction()
    {
        $acl = \DF\Acl::getInstance();
        
        if ($acl->isAllowed('manage transactions'))
            $this->redirectFromHere(array('controller' => 'transaction'));
        else
            $this->redirectFromHere(array('controller' => 'resident', 'action' => 'ledger'));
    }
}