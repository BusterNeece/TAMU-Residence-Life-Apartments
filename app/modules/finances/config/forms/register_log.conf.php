<?php
/**
 * Register report log entry form.
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'amount_cash'		=> array('text', array(
				'label' => 'Total Cash Deposited',
                'filters' => array('Float'),
                'description' => 'Enter without commas or dollar signs (i.e. 0.50).'
	        )),
            
            'amount_check'		=> array('text', array(
				'label' => 'Total Checks Deposited',
                'filters' => array('Float'),
                'description' => 'Enter without commas or dollar signs (i.e. 0.50).'
	        )),
            
            'notes'         => array('textarea', array(
                'label' => 'Additional Notes',
                'class' => 'full-width half-height',
                'description' => 'If there is a known reason for any discrepancy between this amount and the expected total, note it here for accounting purposes.'
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);