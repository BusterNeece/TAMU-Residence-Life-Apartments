<?php
/**
 * Transaction edit form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'transaction_type' => array('select', array(
                'label' => 'Transaction Type',
                'multiOptions' => RegisterTransaction::getTypeCodes(),
            )),
            
            'date'     => array('unixdate', array(
                'label' => 'Transaction Date',
                'required' => true,
                'default' => time(),
            )),
			
            'memo'      => array('textarea', array(
                'label' => 'Memo',
                'class' => 'full-width half-height',
            )),
            
            'check_reference_number' => array('text', array(
                'label' => 'Check Reference Number',
            )),
            
            'register_id' => array('select', array(
                'label' => 'Register',
                'multiOptions' => Register::fetchSelect(TRUE),
            )),
            
            'transaction_source_id' => array('select', array(
                'label' => 'Transaction Source',
                'multiOptions' => TransactionSource::fetchSelect(TRUE),
            )),
			
            'item_id' => array('select', array(
                'label' => 'Item',
                'multiOptions' => Item::fetchSelect(TRUE),
				'required' => true,
            )),
			
			'asset_id' => array('select', array(
				'label' => 'Apartment or Property',
				'multiOptions' => Asset::fetchSelect(TRUE),
				'description' => 'Only select if this item relates to an apartment (such as Rent, Electric, Maintenance, etc). Leave as N/A for general items like security deposits.',
			)),
            
            'payment_method_id' => array('radio', array(
				'required' => TRUE,
                'label' => 'Payment Method',
                'multiOptions' => PaymentMethod::fetchSelect(),
            )),
            
            'split_amount' => array('text', array(
                'label' => 'Item Amount',
                'filters' => array('Float'),
                'description' => 'Enter a dollar amount, without the dollar sign (i.e. 0.50)',
            )),
            
            'matching_split_ids' => array('multicheckbox', array(
                'label' => 'Transactions Paid by Credit',
                'multiOptions' => array(),
                'required' => true,
            )),
            
            'due_date' => array('unixdate', array(
                'label' => 'Due Date',
                'description' => 'Format: MM/DD/YYYY. Leave blank if not applicable.',
            )),
            
            'recurring_type' => array('radio', array(
                'label' => 'Recurring Charge',
                'multiOptions' => array(
                    Split::RECURRING_INACTIVE => 'No',
                    Split::RECURRING_DAILY => 'Repeats Daily',
                    Split::RECURRING_MONTHLY => 'Repeats Monthly',
                ),
            )),
            
            'recurring_active' => array('radio', array(
                'label' => 'Recurring Active',
                'description' => 'Set to "No" to prevent more recurring charges from posting.',
                'multiOptions' => array(
                    0       => 'No',
                    1       => 'Yes',
                ),
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);