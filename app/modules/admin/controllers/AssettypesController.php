<?php
use \Entity\AssetType;

class Admin_AssettypesController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer all');
    }
    
    public function indexAction()
    {
		$this->view->all_records = AssetType::fetchAll();
    }
	
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $form = new \DF\Form($this->current_module_config->forms->assettype->form);
        
        if (trim($id) != '')
        {
            $record = AssetType::find((int)$id);
            $form->setDefaults($record->toArray());
        }
        
        if(!empty($_POST) && $form->isValid($_POST))
		{
            $data = $form->getValues();
            
            if (!$record)
                $record = new AssetType();

            $record->fromArray($data);
            $record->save();
            
            $this->alert('Record updated!', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
		}

        $this->view->headTitle('Add/Update Apartment Type');
        $this->renderForm($form);
    }
	
	public function deleteAction()
	{
        $this->validateToken($this->_getParam('csrf'));
		
		$record = AssetType::find($this->_getParam('id'));
		if ($record)
			$record->delete();
		
		$this->alert('Record deleted!', 'green');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
        return;
	}
}