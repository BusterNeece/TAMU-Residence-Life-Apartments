<?php
use \Entity\LeaseVersion;

class Admin_LeasesController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer blocks');
    }
    
    public function indexAction()
    {
        $lease_types = LeaseVersion::getTypes();
        $this->view->lease_types = $lease_types;
    }
	
	public function updateAction()
	{
        $type = $this->_getParam('type');

        $form = new \DF\Form($this->current_module_config->forms->lease->form);

        $latest = LeaseVersion::fetchLatest($type);
        if ($latest instanceof LeaseVersion)
        	$content = $latest->getContent();
        else
        	$content = '';
        
        $form->setDefaults(array(
        	'content'		=> $content,
        	'is_new_revision' => 0,
	    ));

        if ($_POST && $form->isValid($_POST) )
        {
            $data = $form->getValues();

            $new_rev = ($data['is_new_revision'] == 1);
            LeaseVersion::update($type, $data['content'], $new_rev);
			
			$this->alert('Lease updated.', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
        }

        $this->view->headTitle('Add/Update Lease Version');
        $this->view->tinymce();
        $this->renderForm($form);
	}
}