<?php
use \Entity\ResidentType;

class Admin_ResidenttypesController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('administer all');
    }
    
    public function indexAction()
    {
		$this->view->all_records = ResidentType::fetchAll();
    }
	
	public function addAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->residenttype_add->form);
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$record = new ResidentType();
			$record->fromArray($data);
			$record->save();
			
			$this->alert('New record created!', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
		}

		$this->view->headTitle('Add New Resident Type');
		$this->renderForm($form);
	}
	
	public function deleteAction()
	{
        $this->validateToken($this->_getParam('csrf'));
		
		$record = ResidentType::find($this->_getParam('id'));
		if ($record)
			$record->delete();
		
		$this->alert('Record deleted!');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}