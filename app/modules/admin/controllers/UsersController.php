<?php
use \Entity\User;

class Admin_UsersController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed('manage users');
    }

    public function indexAction()
    {
        // Handle search queries.
        if ($_GET)
            $this->redirectFromHere($_GET);

        $qb = $this->em->createQueryBuilder()
            ->select('u')
            ->from('Entity\User', 'u')
            ->orderBy('u.lastname', 'ASC');

        if ($this->_hasParam('searchterms'))
        {
            $q = $this->_getParam('searchterms');
            $this->view->searchterms = $q;

            $qb->andWhere('(u.uin LIKE :q OR u.username LIKE :q OR CONCAT(u.firstname, CONCAT(\' \', u.lastname)) LIKE :q)')->setParameter('q', '%'.$q.'%');
        }

        $pager = new \DF\Paginator\Doctrine($qb, $this->_getParam('page', 1));
        $this->view->pager = $pager;
    }

    public function addAction()
    {
		$form = new \DF\Form($this->current_module_config->forms->user_new->form);

        if( !empty($_POST) && $form->isValid($_POST) )
        {
			$data = $form->getValues();
			$uins_raw = explode("\n", $data['uin']);
			
			foreach((array)$uins_raw as $uin)
			{
				$uin = trim($uin);
				
				$user = User::getOrCreate($uin);
                $user->fromArray(array('roles' => $data['roles']));
                $user->active = true;
                $user->save();
				
				$this->alert('User <a href="'.\DF\Url::route(array('module' => 'admin', 'controller' => 'users', 'action' => 'edit', 'id' => $user->id)).'" title="Edit User">'.$user->lastname.', '.$user->firstname.'</a> successfully updated/added.');
			}
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
            return;
        }

        $this->view->headTitle('Add User');
        $this->renderForm($form);
    }
    
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		$form = new \DF\Form($this->current_module_config->forms->user_edit->form);
		
		if ($user instanceof User)
            $form->setDefaults($user->toArray(TRUE, TRUE));
	
		if( !empty($_POST) && $form->isValid($_POST) )
		{
            $data = $form->getValues();

            $user->fromArray($data);
            $user->save();

            $this->flash('User updated!', 'green');
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
            return;
		}

        $this->view->headTitle('Edit User');
        $this->renderForm($form);
    }

    public function toggleuserAction()
    {
        $this->doNotRender();
		
		$this->validateToken($this->_getParam('csrf'));
		
        $id = $this->_getParam('id');
        $user = User::find($id);

        if ($user instanceof User)
        {
            if ($user->active)
            {
                $user->active = false;
                $msg = 'User '.$user->lastname.', '.$user->firstname.' has been deactivated';
            }
            else
            {
                $user->active = true;
                $msg = 'User '.$user->lastname.', '.$user->firstname.' has been reactivated';
            }

            $user->save();
            $this->flash($msg, 'green');
        }

        $this->redirectToRoute(array('module' => 'admin', 'controller' => 'users', 'action' => 'index'));
        return;
    }
}