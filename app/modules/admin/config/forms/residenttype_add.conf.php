<?php
/**
 * Add Resident Type
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
					
			'name'		=> array('text', array(
				'label' => 'Resident Type Name',
				'required' => true,
	        )),
			 
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);