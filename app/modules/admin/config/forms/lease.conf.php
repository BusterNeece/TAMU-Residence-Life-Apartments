<?php
/**
 * Lease Form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
			
			'content'	=> array('textarea', array(
				'label' => 'Lease Content',
				'id'	=> 'textarea-content',
				'description' => '<b>If pasting from Word:</b> paste the text into Notepad first, or you will encounter unexpected formatting problems.',
			)),

			'is_new_revision' => array('radio', array(
				'label'	=> 'Create New Lease Version?',
				'description' => 'If the change to the lease is significant enough to indicate a new lease version.',
				'multiOptions' => array(0 => 'No', 1 => 'Yes'),
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);