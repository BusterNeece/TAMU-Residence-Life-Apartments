<?php
/**
 * Settings form.
 */

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
        
        'groups'        => array(
            
            // Billing settings
            'billing'           => array(
                'legend'            => 'Billing Settings',
                'elements'          => array(
                    
                    'late_fee'       => array('text', array(
                        'label'     => 'Late Fee Per Day',
                        'filters'   => array('Float'),
                        'description' => 'Enter a decimal amount, without dollar sign (i.e. 0.012).',
                    )),
                    
                    'late_fee_threshold' => array('text', array(
                        'label'     => 'Days Until Late Fee Charged',
                        'description' => 'Number of days until the late fee begins being charged.',
                    )),
					
					'touchnet_vendor_id' => array('text', array(
						'label'		=> 'TouchNet FAMIS Vendor ID',
						'filters'	=> array('Int'),
					)),
					
					'touchnet_service_charge' => array('text', array(
						'label'     => 'TouchNet Service Charge',
                        'filters'   => array('Float'),
                        'description' => 'Enter a decimal amount, without dollar sign (i.e. 0.012).',
					)),

					'touchnet_credit_charge' => array('text', array(
						'label'     => 'TouchNet Credit Card Convenience Fee',
                        'filters'   => array('Float'),
                        'description' => 'Enter a decimal amount, without dollar sign (i.e. 0.012).',
					)),                    
                ),
            ),

            // Maintenance settings
            'maintenance'           => array(
                'legend'            => 'Maintenance Settings',
                'elements'          => array(

                    'maintenance_cleaning_contact' => array('text', array(
                        'label' => 'Cleaning Charges Reviewer E-mail Address',
                        'description' => 'The recipient of cleaning charge pending review messages.',
                    )),
                    
                ),
            ),
            
            // Application settings
            'application'       => array(
                'legend'            => 'Application Settings',
                'elements'          => array(
                    
                    'application_classification' => array('select', array(
                        'label'     => 'Minimum Classification for Applicants',
                        'description' => 'Applicants who do not meet this minimum classification will not be allowed to submit an application.',
                        'multiOptions' => array(
                            'X0'         => 'No Requirement',
                            'U1'        => 'Undergraduate, Year 1 (U1)',
                            'U2'        => 'Undergraduate, Year 2 (U2)',
                            'U3'        => 'Undergraduate, Year 3 (U3)',
                            'U4'        => 'Undergraduate, Year 4 (U4)',
                            'G5'        => 'Graduate, Year 1 (G5)',
                            'G6'        => 'Graduate, Year 2 (G6)',
                            'G7'        => 'Graduate, Year 3 (G7)',
                        ),
                    )),

                    'minimum_gpa' => array('text', array(
                        'label' => 'Minimum GPA required for residence',
                        'description' => 'Enter a decimal amount, i.e. "2.75".',
                        'filters'     => array('Float'),
                    )),

                    'application_fee' => array('text', array(
                        'label' => 'Amount of Application Fee',
                        'description' => 'Enter a decimal amount, without dollar sign, i.e. "101.50".',
                        'filters'     => array('Float'),
                    )),
                    
                    'application_deposit' => array('text', array(
                        'label'     => 'Amount of Deposit',
                        'description' => 'Enter a decimal amount, without dollar sign, i.e. "101.50".',
                        'filters'     => array('Float'),
                    )),
                    
                    'application_contract_version' => array('text', array(
                        'label'     => 'Contract Version',
                        'description' => 'Enter the current contract version number',
                    )),

                    'enable_renewal' => array('radio', array(
                        'label'     => 'Enable Lease Renewal',
                        'multiOptions' => array(
                            0   => 'No',
                            1   => 'Yes',
                        ),
                    )),
                    
                ),
            ),
            
            'submit'            => array(
                'legend'            => '',
                'elements'          => array(
                    'submit'		=> array('submit', array(
                        'type'	=> 'submit',
                        'label'	=> 'Save Changes',
                        'helper' => 'formButton',
                        'class' => 'ui-button',
                    )),
                ),
            ),
            
        ),
	),
);