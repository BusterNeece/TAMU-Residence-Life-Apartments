<?php
/**
 * Edit Role Form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
					
			'name'		=> array('text', array(
				'label' => 'Role Name',
				'class'	=> 'half-width',
				'required' => true,
	        )),
			
			'actions' => array('multicheckbox', array(
				'label'	=> 'Actions',
				'multiOptions' => \Entity\Action::fetchSelect(),
			)),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);