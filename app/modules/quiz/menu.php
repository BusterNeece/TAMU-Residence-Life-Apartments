<?php
/**
 * Orientation module menu configuration
 */

$quiz_pages = array();
$quizzes = \Entity\Quiz::fetchSelect();

foreach($quizzes as $quiz_id => $quiz_name)
{
    $quiz_pages[] = array(
        'label'     => $quiz_name,
        'module'    => 'quiz',
        'controller' => 'index',
        'action'    => 'index',
        'params'    => array(
            'quiz'        => $quiz_id,
        ),
    );
}

$quiz_pages[] = array(
    'label' => 'Manage Quizzes',
    'module' => 'quiz',
    'controller' => 'manage',
    'permission' => 'administer all',
);

return array(
    'default' => array(
		'quiz'	=> array(
			'label'		=> 'Orientation',
			'module'	=> 'quiz',
            'permission' => 'is logged in',
            
            'pages'     => $quiz_pages,
		),
    ),
);