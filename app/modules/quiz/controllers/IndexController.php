<?php
use \Entity\Quiz;

class Quiz_IndexController extends \DF\Controller\Action
{
	public function permission()
	{
		return $this->acl->isAllowed('is logged in');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		if (!$this->_hasParam('quiz'))
		{
			$this->view->quiz_types = Quiz::fetchArray();
			$this->render('select');
			return;
		}
		else
		{
			// Convert from the new database-driven quiz format into the older one used by this page.
			$quiz = Quiz::find($this->_getParam('quiz'));
			
			if (count($quiz->questions) > 0)
			{
				$quiz_questions = array();
				
				foreach($quiz->questions as $question)
				{
					if ($question->options->count() > 0)
					{
						$quiz_question = array(
							'question'	=> $question->question,
							'options'	=> array(),
						);
						
						$option_ids = array();
						$i = 1;
						foreach($question->options as $option)
						{
							$option_letter = chr(64+$i);
							$quiz_question['options'][$option_letter] = $option->option_text;
							
							$option_ids[$option->id] = $option_letter;
							$i++;
						}
						
						if ($question->correct_answer_id)
						{
							$quiz_question['correct'] = $option_ids[$question->correct_answer_id];
						}
						
						$quiz_questions[] = $quiz_question;
					}
				}
			}
			else
			{
				throw new \DF\Exception('This quiz has no questions listed!');
			}
			
			$this->view->quiz_name = $quiz->name;
			
			if(isset($_REQUEST['submitted']))
			{
				$passed_quiz = true;
				
				foreach($quiz_questions as $question_num => &$quiz_item)
				{
					$quiz_item['selected'] = $_REQUEST['q'.$question_num];
					$quiz_item['is_correct'] = (strcmp($_REQUEST['q'.$question_num], $quiz_item['correct']) == 0) ? true : false;
					$quiz_item['was_responded_to'] = (isset($_REQUEST['q'.$question_num])) ? true : false;
					
					if ($quiz_item['was_responded_to'])
					{
						if ($quiz_item['is_correct'])
						{
							$quiz_item['options'][$quiz_item['selected']] .= ' (Correct)';
						}
						else
						{
							$quiz_item['options'][$quiz_item['selected']] .= ' (Incorrect)';
							$passed_quiz = false;
						}
					}
					else
					{
						$passed_quiz = false;
					}
				}
				
				if ($passed_quiz)
				{
					$user = \DF\Auth::getInstance()->getLoggedInUser();
					
					if ($user->resident->id)
					{
						$user->resident->orientation_date = time();
						$user->save();
						
						$occupancies = Occupancy::getActiveByResidentId($user->resident->id);
						if ($occupancies)
						{
							foreach($occupancies as $occupancy)
							{
								$occupancy->orientation = time();
								$occupancy->save();
							}
						}
						
						// Create new record for quiz completion.
						$record = new QuizResult();
						$record->quiz_id = $this->_getParam('quiz');
						$record->resident_id = $user->resident->id;
						$record->timestamp = time();
						$record->save();
						
						// Send confirmation e-mail.
						\DF\Messenger::send(array(
							'to'		=> array($user->email, $user->email2),
							'subject'	=> 'Confirmation of Orientation Completion',
							'template'	=> 'quiz/complete',
							'vars'		=> array(
								'quiz'		=> $quiz,
							),
						));
					}
					
					$this->render('success');
					return;
				}
				else
				{
					$this->alert('<b>Some questions were answered incorrectly.</b><br />Incorrect questions have been highlighted below. Please correct your responses.', 'red');
				}
			}
		
			$this->view->quiz = $quiz_questions;
			$this->render();
			return;
		}
	}
}