<?php
/**
 * Reports manager for University Apartments
 */

class UA_ReportManager
{
    // Generate report for Transportation Services.
    public static function generateTransportationReport()
    {
        $occupancy_by_uin = array();
        $uins = array();
        
        // Get all active occupants.
        $occupancies = Doctrine_Query::create()
            ->from('Occupancy o')
            ->innerJoin('o.Resident r')
            ->innerJoin('o.Asset a')
            ->addWhere('(o.end_date IS NULL OR o.end_date = 0 OR o.end_date >= ?)', time())
            ->fetchArray();
        
            // ->addWhere('(o.start_date <= ?)', time())
            // ->addWhere('(r.is_external = 0 OR r.is_external IS NULL)')
        
        foreach($occupancies as $occupancy)
        {
            $uin = $occupancy['Resident']['uin'];
            
            $occupancy_by_uin[$uin] = $occupancy;
            $uins[] = $uin;
        }
        
        $export_headers = array(array(
            'UIN',
            'Last Name',
            'First Name',
			'LeaseHolder',
			'Tenant',
            'Apartment',
            'Start Date',
            'End Date',
        ));
        $export_primary = array();
        $export_nonprimary = array();
        
        // Get all residents associated with the active occupants.
        $residents = Doctrine_Query::create()
            ->from('Resident r')
            ->whereIn('r.uin', $uins)
            ->fetchArray();
        
        foreach($residents as $resident)
        {
			$sub_id = trim($resident['uin_sub_id']);
			$resident_types_to_ignore = array(5, 6);
            
            if(!in_array($resident['resident_type_id'], $resident_types_to_ignore))
			{
                $export_row = array();
                
				if (!empty($sub_id))
				{
                    $export_row[] = ($resident['secondary_uin']) ? $resident['secondary_uin'] : '';
                    $export_row[] = $resident['surname'];
                    $export_row[] = $resident['first_name'];
                    $export_row[] = 'F';
                    $export_row[] = 'T';
				}
				else
				{
                    $export_row[] = $resident['uin'];
                    $export_row[] = $resident['surname'];
                    $export_row[] = $resident['first_name'];
                    $export_row[] = 'T';
                    $export_row[] = 'T';
				}
                
                $occupancy = $occupancy_by_uin[$resident['uin']];
                $export_row[] = $occupancy['Asset']['name'];
                $export_row[] = ($occupancy['start_date'] != 0) ? date('m/d/Y', $occupancy['start_date']) : 'N/A';
                $export_row[] = ($occupancy['end_date'] != 0) ? date('m/d/Y', $occupancy['end_date']) : 'N/A';
                
                $export_primary[] = $export_row;
			}
        }
        
        $export_data = array_merge($export_headers, $export_primary, $export_nonprimary);
        
        // Create temporary file.
        $csv = DF_Export::convertToCSV($export_data, TRUE);
        $temp_filename = DF_INCLUDE_TEMP.'/transportation_report.csv';
        file_put_contents($temp_filename, $csv);
        
        // Push through FTP.
        $ch = curl_init();
		$fp = fopen($temp_filename, 'r');
        
		curl_setopt($ch, CURLOPT_URL, 'ftps://student:G7ecD38s@TSFTP.tamu.edu:990/'.basename($temp_filename));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
		curl_setopt($ch, CURLOPT_FTPSSLAUTH, 1);
		curl_setopt($ch, CURLOPT_UPLOAD, 1);
		curl_setopt($ch, CURLOPT_INFILE, $fp);
		curl_setopt($ch, CURLOPT_TRANSFERTEXT, 1);
		curl_setopt($ch, CURLOPT_INFILESIZE, filesize($temp_filename));
        
        if (curl_exec($ch) === FALSE)
        {
            throw new DF_Exception(curl_error($ch));
        }
        else
        {
            curl_close($ch);
            fclose($fp);
            return true;
        }
		
		return ($error_no == 0);
    }
}