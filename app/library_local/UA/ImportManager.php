<?php
/**
 * External User Import Manager
 */

class UA_ImportManager
{
    public static function syncExternalResidents()
    {
        $return_stats = array();
        
        $start_date = strtotime('08/20/2011 00:00:00');
        $end_date = strtotime('05/15/2012 00:00:00');
        
        $upload_path = 'D:/Upload/Apartments';
        $dump_files = glob($upload_path.'/gardens2_*');
        
        $report_location = NULL;
        
        if (count($dump_files) > 0)
        {
            // Pull the most recently modified file.
            $files_by_mtime = array();
            foreach($dump_files as $dump_file)
            {
                $mod_time = filemtime($dump_file);
                $files_by_mtime[$mod_time] = $dump_file;
            }
            ksort($files_by_mtime);
            
            $report_location = array_pop($files_by_mtime);
            
            // Delete all other files.
            foreach($files_by_mtime as $delete_file_path)
            {
                @unlink($delete_file_path);
            }
        }
        
        if ($report_location)
        {
            // Pull all residents listed in the feed.
            $csv = DF_File::getCSV($report_location);
            
            $residents = array();
            foreach($csv as $csv_row)
            {
                if ($csv_row && substr($csv_row[5], 0, 1) == "G")
                {
                    $residents[] = array(
                        'info'          => array(
                            'uin'			=> $csv_row[2],
                            'first_name'	=> $csv_row[0],
                            'surname'		=> $csv_row[1],
                            'email'			=> $csv_row[4],
                        ),
                        'apartment'		=> substr($csv_row[5], 1, 1).substr($csv_row[6], 0, 3),
                    );
                }
            }
            
            // Process additions.
            $resident_ids = array();
            
            foreach($residents as $resident_raw)
            {
                $resident = Resident::fetchByUin($resident_raw['info']['uin']);
                
                if (!($resident instanceof Resident))
                {
                    $resident = new Resident();
                    
                    $return_stats['new_residents'][] = $resident_raw['info']['uin'];
                    $return_stats['residents_created']++;
                }
                else
                {
                    $return_stats['residents_updated']++;
                }
                
                $resident->principal_resident = 1;

                // Update the resident's info.
                $resident->fromArray($resident_raw['info']);
                $resident->save();
                
                $resident_ids[] = $resident->id;
                
                // Find an asset by the same name as the imported data.
                $asset = Asset::fetchByName($resident_raw['apartment']);
                
                if ($asset instanceof Asset)
                {
                    $is_already_imported = FALSE;
                    
                    if ($resident->Occupancies->count() != 0)
                    {
                        foreach($resident->Occupancies as $current_occ)
                        {
                            if ($current_occ->is_external == 1)
                            {
                                // Flag as already imported, or delete an old occupancy.
                                if ($current_occ->asset_id == $asset->id)
                                {
                                    $is_already_imported = TRUE;
                                }
                                else
                                {
                                    $return_stats['occupancies_moved_out']++;
                                    $current_occ->delete();
                                }
                            }
                        }
                    }
                    
                    if ($is_already_imported)
                    {
                        $return_stats['occupancies_already_imported']++;
                    }
                    else
                    {
                        $occupancy = new Occupancy();
                        $occupancy->is_external = 1;
                        $occupancy->resident_id = $resident->id;
                        $occupancy->asset_id = $asset->id;
                        $occupancy->start_date = $start_date;
                        $occupancy->end_date = $end_date;
                        $occupancy->save();
                        
                        // Update any moveout appointments.
                        if ($resident->MoveoutAppointment->count() > 0)
                        {
                            foreach($resident->MoveoutAppointment as $appt)
                            {
                                $appt->occupancy_id = $occupancy->id;
                                $appt->save();
                            }
                        }
                        
                        $return_stats['occupancies_created']++;
                    }
                }
                else
                {
                    $return_stats['assets_not_found'][] = $resident_raw['apartment'];
                }
            }
            
            // Process deletions.
            $deleted_rows = Doctrine_Query::create()
                ->update('Occupancy o')
                ->set('o.deleted_at', '?', date('Y-m-d h:i:s'))
                ->where('o.is_external = ?', 1)
                ->andWhere('o.deleted_at IS NULL')
                ->andWhereNotIn('o.resident_id', $resident_ids)
                ->execute();
            
            $return_stats['occupancies_deleted'] = (int)$deleted_rows;
        }
        
        return $return_stats;
    }
}