<?php
/**
 * Utility class to facilitate pushing transactions over to FAMIS.
 */

class UA_FamisManager
{
	public static function generateDailyReport($base_timestamp = NULL)
	{
        if (!$base_timestamp)
            $base_timestamp = time();
        
		$start_of_day = strtotime('Yesterday 00:00:00', $base_timestamp);
		$end_of_day = strtotime('Yesterday 23:59:59', $base_timestamp);
		return self::generateReportForTimeRange($start_of_day, $end_of_day);
	}
    
    public static function generateReportForId($id, $is_refund = FALSE)
    {
        $touchnet = Touchnet::fetchById($id);
        
        if ($touchnet instanceof Touchnet)
        {
            return self::generateReportForTimeRange((int)$touchnet->time_updated, (int)$touchnet->time_updated, TRUE, $is_refund);
        }
    }
	
	public static function generateReportForTimeRange($start_of_day, $end_of_day, $write_files = TRUE, $refund_mode = FALSE)
	{
        // Used for special security deposit handling later.
        $security_deposit = Item::fetchByName('Security Deposit');
        $security_deposit_amount = 100 * Settings::getSetting('application_deposit', 0);
        
        // Fetch list of transactinos.
		$transactions = self::getReportForTimeRange($start_of_day, $end_of_day);
		$trans_report = array();
        
        $transaction_log = array();
        $manual_log = array();
		
		if ($transactions)
		{
			foreach($transactions as $trans)
			{
				$trans_string = '';
				$trans_report_row = array();
				
				$trans_report_row['resident_id'] = $trans['local_record_id'];
				
				$vendor_id = Settings::getSetting('touchnet_vendor_id', 'NOVENDOR');
				$trans_report_row['vendor_id'] = $vendor_id;
				$trans_string .= str_pad($vendor_id, 10, '0', STR_PAD_LEFT);
				
				$trans_report_row['order_id'] = (int)$trans['payment_order_id'];
				$trans_string .= str_pad((int)$trans['payment_order_id'], 20, '0', STR_PAD_LEFT);
				
				$trans_report_row['date'] = date('m/d/Y h:ia', $trans['time_created']);
				$trans_string .= date('YmdHis', $trans['time_created']);
				
				$trans_report_row['amount'] = $trans['payment_amount'];
				$payment_amount = round($trans['payment_amount'] * 100);
                
                if ($refund_mode)
                    $payment_amount = 0-$payment_amount;
                
                $trans_string .= self::padAmount($payment_amount, 12);
				
				$total_amount = 0;
                $splits_by_support_account = array();
                
                // Special handling for security deposits.
                foreach($trans['Splits'] as $split)
                {
                    $amount = 0-round($split['split_amount'] * 100);
                    
                    if ($split['Asset']['AssetLocation']['support_account'])
                        $support_account = (int)$split['Asset']['AssetLocation']['support_account'];
                    else
                        $support_account = 0;
                    
                    if ($split['Item']['object_code'])
                        $object_code = (int)$split['Item']['object_code'];
                    else
                        $object_code = 1;
                    
                    // Special exception for deposits.
                    if ($object_code == 2515)
                        $primary_account = '030071';
                    else
                        $primary_account = '300710';
                    
                    $account = $primary_account.'-'.str_pad($support_account, 5, '0', STR_PAD_LEFT).'-'.str_pad($object_code, 4, '0', STR_PAD_LEFT);
                    
                    $splits_by_support_account[$account] += $amount;
                    $total_amount += $amount;
                }
				
				$num_splits = count($splits_by_support_account);
				$trans_splits = array();
				
				$service_fee = 0;
                
                $service_fee_change_date = strtotime('August 1, 2011 00:00:00');
                $service_fee_due = ($end_of_day >= $service_fee_change_date) ? 40 : 50;
				
				foreach($splits_by_support_account as $account => $amount)
				{
					// Assess proportional amount of service fee.
					if ($service_fee < $service_fee_due)
					{
						$service_fee_component = floor($service_fee_due / $num_splits);
						
						if ($amount >= $service_fee_component)
						{
							$amount -= $service_fee_component;
							$service_fee += $service_fee_component;
						}
						else
						{
							$service_fee += $amount;
                            $amount = 0;
						}
					}
					
					$trans_splits[] = array(
						'account'	=> $account,
						'amount'	=> $amount,
					);
				}
				
				// Even out unusual amounts for service fee.
				if ($service_fee < $service_fee_due)
				{
                    $service_fee_remaining = $service_fee_due - $service_fee;
                    
                    foreach($trans_splits as $split_offset => &$split_info)
                    {
                        if ($split_info['amount'] >= $service_fee_remaining)
                        {
                            $split_info['amount'] -= $service_fee_remaining;
                            $service_fee = $service_fee_due;
                            break;
                        }
                    }
				}
				
				// Charge service fee to account.
				$trans_splits[] = array(
					'account'		=> '215050-00000-0611',
					'amount'		=> $service_fee,
				);
				
				// Fill in remaining spots with blanks.
				for($i = count($trans_splits); $i < 20; $i++)
				{
					$trans_splits[] = array(
						'account'	=> '',
						'amount'	=> 0,
					);
				}
				
				// Log all items to the visual report.
				foreach($trans_splits as $trans_item)
				{
					$split_item = array(
						'account'		=> $trans_item['account'],
						'amount'		=> $trans_item['amount'] / 100,
					);
					$trans_report_row['items'][] = $split_item;
				}
				
				$split_total = 0;
				foreach($trans_splits as $trans_item)
				{
                    $trans_string .= str_pad(str_replace('-', '', $trans_item['account']), 20, ' ', STR_PAD_RIGHT);
                    
                    if ($refund_mode)
                        $trans_amount = 0-abs($trans_item['amount']);
                    else
                        $trans_amount = abs($trans_item['amount']);
                    
                    $trans_string .= self::padAmount($trans_amount, 12);
                    $split_total += $trans_amount;
				}
				
				if (strlen($trans_string) == 696)
				{
					if ($payment_amount == $split_total)
					{
						$transaction_log[] = $trans_string;
						$trans_report_row['error'] = FALSE;
					}
					else
					{
                        $manual_log[] = 'Error: Amounts do not match ('.$payment_amount.' from TouchNet vs '.$split_total.' from Splits)';
						$manual_log[] = $trans_string;
						$trans_report_row['error'] = TRUE;
					}
				}
                else
                {
                    $manual_log[] = 'Error: String length is incorrect ('.strlen($trans_string).')';
                    $manual_log[] = $trans_string;
					$trans_report_row['error'] = TRUE;
                }
                
                $trans_report[] = $trans_report_row;
			}
			
			if ($write_files)
			{
				$written_files = array();
				
				// Write transaction string.
				$transaction_string = implode("\n", (array)$transaction_log);
                
				if (!empty($transaction_string))
				{
					$file_path = DF_INCLUDE_TEMP.'/FamisReport_'.date('YmdHis', $start_of_day).'-'.date('YmdHis', $end_of_day).'.txt';
					$written_files[] = $file_path;
					file_put_contents($file_path, $transaction_string);
					
					// Upload via FTP.
					self::uploadFile($file_path);
				}
				
				// Write failed transaction string.
				$manual_string = implode("\n", (array)$manual_log);
                
				if (!empty($manual_string))
				{
					$file_path = DF_INCLUDE_TEMP.'/FamisReport_'.date('YmdHis', $start_of_day).'-'.date('YmdHis', $end_of_day).'_Failed.txt';
					$written_files[] = $file_path;
					file_put_contents($file_path, $manual_string);
				}
				
				// Send both files as debug info to IT.
				$attachments = array();	
				$body = array();
				
				foreach((array)$written_files as $written_file_path)
				{
					$body[] = basename($written_file_path);
					$attachments[] = DF_Messenger::attachFile($written_file_path, basename($written_file_path, '.txt').'.txt');
				}
				
				DF_Messenger::send(array(
					'to'			=> 'bneece@stuact.tamu.edu',
					'subject'		=> 'Apartments FAMIS Log Files',
					'body'			=> implode("\r\n", $body),
					'attachments'	=> $attachments,
					'text_only'		=> TRUE,
				));
			}
		}
		
		return $trans_report;
	}
    
    public static function padAmount($number, $pad_length = 12)
    {
        $number = (int)$number;
        
        if ($number < 0)
            return '-'.str_pad(abs($number), $pad_length-1, '0', STR_PAD_LEFT);
        else
            return str_pad(abs($number), $pad_length, '0', STR_PAD_LEFT);
    }
	
	public static function uploadFile($file_path)
	{
		$config = Zend_Registry::get('config');
		$touchnet_config = $config->services->touchnet;
		
		$ch = curl_init();
		$fp = fopen($file_path, 'r');

		curl_setopt($ch, CURLOPT_URL, $touchnet_config->ftp_path.basename($file_path));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
		curl_setopt($ch, CURLOPT_FTPSSLAUTH, 1);
		curl_setopt($ch, CURLOPT_UPLOAD, 1);
		curl_setopt($ch, CURLOPT_INFILE, $fp);
		curl_setopt($ch, CURLOPT_TRANSFERTEXT, 1);
		curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file_path));
		
		$result = curl_exec($ch);
		$error_no = curl_errno($ch);
		curl_close($ch);
		fclose($fp);
				
		return ($error_no == 0);
	}
	
	public static function getReportForTimeRange($start_time, $end_time)
	{
		// Pull transactions from entire previous day.
		$transactions = Doctrine_Query::create()
			->from('Touchnet t')
			->leftJoin('t.Splits s')
			->leftJoin('s.Transaction st')
			->leftJoin('s.Item i')
			->leftJoin('s.Asset a')
			->leftJoin('a.AssetLocation al')
            ->addWhere('(s.deleted_at = s.deleted_at OR s.deleted_at IS NULL)') // Include deleted splits.
			->addWhere('t.time_updated >= ?', $start_time)
			->addWhere('t.time_updated <= ?', $end_time)
			->addWhere('t.payment_status = ?', 'success')
			->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
		
		if ($transactions)
		{
			foreach($transactions as &$transaction)
			{
                $transaction_hash = md5($transaction['local_record_id'].$transaction['payment_amount']);
				$transaction['hash'] = $transaction_hash;
                
				$resident = Doctrine_Query::create()
					->from('Resident r')
					->leftJoin('r.Occupancies o')
					->leftJoin('o.Asset a')
					->addWhere('r.id = ?', $transaction['local_record_id'])
					->fetchOne();
                
                if ($resident instanceof Resident)
                    $transaction['Resident'] = $resident->toArray();
                else
                    $transaction['Resident'] = array();
			}
		}
		
		return $transactions;
	}
}