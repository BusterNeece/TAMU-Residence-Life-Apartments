<?php
namespace UA\Form;

use \Entity\Resident;
use \Entity\ResidentType;
use \Entity\College;
use \Entity\Major;
use \Entity\User;

class ResidentEdit extends \DF\Form\Custom
{
    protected $_resident;

    public function __construct(Resident $resident, $options = null)
    {
        $this->_resident = $resident;
        parent::__construct($options);
    }
    
    public function getResident()
    {
        return $this->_resident;
    }

    public function save()
    {
        try
        {
            $resident = $this->_resident;
            $data = $this->getValues();

            // Resident info
            $resident->principal_resident = (trim($data['resident_info_subform']['principal_resident'])!='') ? true : false;
            $resident->resident_type_id = $data['resident_info_subform']['resident_type_id'];

            $resident->first_name = $data['resident_info_subform']['first_name'];
            $resident->middle_name = $data['resident_info_subform']['middle_name'];
            $resident->surname = $data['resident_info_subform']['surname'];
            $resident->suffix = $data['resident_info_subform']['suffix'];
            $resident->uin = $data['resident_info_subform']['uin'];
            $resident->uin_sub_id = (trim($data['resident_info_subform']['uin_sub_id']) != '' ? $data['resident_info_subform']['uin_sub_id'] : null);
            $resident->birth_date = (trim($data['resident_info_subform']['birth_date']) != '' ? $data['resident_info_subform']['birth_date'] : null);

            // Contact info
            $resident->primary_phone = $data['contact_info_subform']['primary_phone'];
            $resident->secondary_phone = $data['contact_info_subform']['secondary_phone'];
            $resident->voip_phone = $data['contact_info_subform']['voip_phone'];
            $resident->email = $data['contact_info_subform']['email'];
            $resident->preferred_email = $data['contact_info_subform']['preferred_email'];

            // Mailing address
            $resident->mailing_address = $data['mailing_address_subform']['mailing_address'];
            $resident->mailing_address2 = $data['mailing_address_subform']['mailing_address2'];
            $resident->mailing_city = $data['mailing_address_subform']['mailing_city'];
            $resident->mailing_state = $data['mailing_address_subform']['mailing_state'];
            $resident->mailing_zip = $data['mailing_address_subform']['mailing_zip'];
            $resident->mailing_country = ($data['mailing_address_subform']['mailing_country'] != 'null') ? $data['mailing_address_subform']['mailing_country'] : NULL;

            // Permanent address
            $resident->permanent_address = $data['permanent_address_subform']['permanent_address'];
            $resident->permanent_address2 = $data['permanent_address_subform']['permanent_address2'];
            $resident->permanent_city = $data['permanent_address_subform']['permanent_city'];
            $resident->permanent_state = $data['permanent_address_subform']['permanent_state'];
            $resident->permanent_zip = $data['permanent_address_subform']['permanent_zip'];
            $resident->permanent_country = ($data['permanent_address_subform']['permanent_country'] != 'null') ? $data['permanent_address_subform']['permanent_country'] : NULL;

            // Forwarding address
            $resident->forwarding_address = $data['forwarding_address_subform']['forwarding_address'];
            $resident->forwarding_address2 = $data['forwarding_address_subform']['forwarding_address2'];
            $resident->forwarding_city = $data['forwarding_address_subform']['forwarding_city'];
            $resident->forwarding_state = $data['forwarding_address_subform']['forwarding_state'];
            $resident->forwarding_zip = $data['forwarding_address_subform']['forwarding_zip'];
            $resident->forwarding_country = ($data['forwarding_address_subform']['forwarding_country'] != 'null') ? $data['forwarding_address_subform']['forwarding_country'] : NULL;
            $resident->forwarding_comments = $data['forwarding_address_subform']['forwarding_comments'];

            // School info
            $resident->classification = $data['school_info_subform']['classification'];
            $resident->college_id = ($data['school_info_subform']['college_id'] != 'null') ? $data['school_info_subform']['college_id'] : NULL;
            $resident->major_id = ($data['school_info_subform']['major_id'] != 'null') ? $data['school_info_subform']['major_id'] : NULL;

            // Medical info
            $resident->meningitis_vaccination = $data['medical_info_subform']['meningitis_vaccination'];
            $resident->meningitis_vacc_proof = $data['medical_info_subform']['meningitis_vacc_proo'];
            $resident->meningitis_vacc_action = $data['medical_info_subform']['meningitis_vacc_acti'];

            // Demographic info
            $resident->birth_country = ($data['demographic_info_subform']['birth_country'] != 'null') ? $data['demographic_info_subform']['birth_country'] : NULL;
            $resident->gender = $data['demographic_info_subform']['gender'];
            $resident->marital_status = $data['demographic_info_subform']['marital_status'];
            $resident->number_of_children = $data['demographic_info_subform']['number_of_children'];
            $resident->marriage_certificate = $data['demographic_info_subform']['marriage_certificate'];
            $resident->birth_certificate = $data['demographic_info_subform']['birth_certificate'];
            $resident->laundry_card = $data['demographic_info_subform']['laundry_card'];
            $resident->orientation_date = $data['demographic_info_subform']['orientation_date'];
            $resident->international_student = $data['demographic_info_subform']['international_student'];

            // Emergency Contact 1
            $resident->emergency_missing_person = (trim($data['emergency_contact_info_subform']['emergency_missing_person'])!='') ? 1 : 0;
            $resident->emergency_firstname = $data['emergency_contact_info_subform']['emergency_firstname'];
            $resident->emergency_middlename = $data['emergency_contact_info_subform']['emergency_middlename'];
            $resident->emergency_lastname = $data['emergency_contact_info_subform']['emergency_lastname'];
            $resident->emergency_address = $data['emergency_contact_info_subform']['emergency_address'];
            $resident->emergency_address2 = $data['emergency_contact_info_subform']['emergency_address2'];
            $resident->emergency_city = $data['emergency_contact_info_subform']['emergency_city'];
            $resident->emergency_state = $data['emergency_contact_info_subform']['emergency_state'];
            $resident->emergency_zip = $data['emergency_contact_info_subform']['emergency_zip'];
            $resident->emergency_country = ($data['emergency_contact_info_subform']['emergency_country'] != 'null') ? $data['emergency_contact_info_subform']['emergency_country'] : NULL;
            $resident->emergency_phone = $data['emergency_contact_info_subform']['emergency_phone'];
            $resident->emergency_relationship = $data['emergency_contact_info_subform']['emergency_relationship'];
            
            // Emergency Contact 2
            $resident->emergency2_missing_person = (trim($data['emergency2_contact_info_subform']['emergency2_missing_person'])!='') ? 1 : 0;
            $resident->emergency2_firstname = $data['emergency2_contact_info_subform']['emergency2_firstname'];
            $resident->emergency2_middlename = $data['emergency2_contact_info_subform']['emergency2_middlename'];
            $resident->emergency2_lastname = $data['emergency2_contact_info_subform']['emergency2_lastname'];
            $resident->emergency2_address = $data['emergency2_contact_info_subform']['emergency2_address'];
            $resident->emergency2_address2 = $data['emergency2_contact_info_subform']['emergency2_address2'];
            $resident->emergency2_city = $data['emergency2_contact_info_subform']['emergency2_city'];
            $resident->emergency2_state = $data['emergency2_contact_info_subform']['emergency2_state'];
            $resident->emergency2_zip = $data['emergency2_contact_info_subform']['emergency2_zip'];
            $resident->emergency2_country = ($data['emergency2_contact_info_subform']['emergency2_country'] != 'null') ? $data['emergency2_contact_info_subform']['emergency2_country'] : NULL;
            $resident->emergency2_phone = $data['emergency2_contact_info_subform']['emergency2_phone'];
            $resident->emergency2_relationship = $data['emergency2_contact_info_subform']['emergency2_relationship'];
            
            $resident->notes = $data['notes_subform']['notes'];

            $resident->save();
            
            return true;
        }
        catch(\Exception $e)
        {
            $this->setErrors(array(
                $e->getMessage()
            ));
            
            return false;
        }
    }

    public function init()
    {
        $manage = \DF\Acl::getInstance()->isAllowed('manage residents');

        $this->setName('edit_resident_form');

        // Resident Info
        $resident_info = new \Zend_Form_SubForm('resident_info_subform');
        $resident_info->setLegend('Resident Info');
        $this->addSubForm($resident_info, 'resident_info_subform');
            
		if ($manage)
		{
			// Principal Resident
			$resident_info->addElement('checkbox', 'principal_resident', array(
				'label' => 'Principal Resident',
			));
		}
		else
		{
			// Principal Resident
			$resident_info->addElement('hidden', 'principal_resident');
		}

		if (($manage) || (trim($this->_resident->uin_sub_id) != ''))
		{
			$types = array();
			foreach( ResidentType::fetchAll() as $type )
			{
				$types[$type->id] = $type->name;
			}
			// Resident Type
			$resident_info->addElement('select', 'resident_type_id', array(
				'label' => 'Resident Type',
				'multiOptions' => $types,
			));
		}
		else
		{
			// Resident Type
			$resident_info->addElement('hidden', 'resident_type_id');
		}
		

		// First Name
		$resident_info->addElement('text','first_name', array(
			'label' => 'First Name',
			'required' => true,
		));

		// Middle Name
		$resident_info->addElement('text','middle_name', array(
			'label' => 'Middle Name',
		));

		// Last Name
		$resident_info->addElement('text','surname', array(
			'label' => 'Last Name',
			'required' => true,
		));

		// Suffix
		$resident_info->addElement('text','suffix', array(
			'label' => 'Suffix',
		));

		if ($manage)
		{
			// UIN
			$resident_info->addElement('text','uin', array(
				'label' => 'UIN',
				'required' => true,
			));

			// UIN sub id
			$resident_info->addElement('text','uin_sub_id', array(
				'label' => 'UIN sub id',
			));

			// Laundry Card
			$resident_info->addElement('text','laundry_card', array(
				'label' => 'Laundry Card',
			));
		}
		else
		{
			// UIN
			$resident_info->addElement('hidden','uin');

			// UIN sub id
			$resident_info->addElement('hidden','uin_sub_id');

			// Laundry Card
			$resident_info->addElement('hidden','laundry_card');
		}

		// Birth Date
		$resident_info->addElement('unixdate','birth_date', array(
			'label' => 'Birth Date',
		));

        // Contact Info
        $contact_info = new \Zend_Form_SubForm('contact_info_subform');
        $contact_info->setLegend('Contact Info');
        $this->addSubForm($contact_info, 'contact_info_subform');

		// Primary Phone
		$contact_info->addElement('text','primary_phone', array(
			'label' => 'Primary Phone',
			'filters' => array(
				'Phone',
				'StringTrim',
			),
			'validators' => array(
				'Phone',
			),
		));
		
		// Secondary Phone
		$contact_info->addElement('text','secondary_phone', array(
			'label' => 'Secondary Phone',
			'filters' => array(
				'Phone',
				'StringTrim',
			),
			'validators' => array(
				'Phone',
			),
		));
		
		// VOIP Phone
		$contact_info->addElement('text','voip_phone', array(
			'label' => 'VOIP Phone',
			'filters' => array(
				'Phone',
				'StringTrim',
			),
			'validators' => array(
				'Phone',
			),
		));

		// Email
		if ($manage)
		{
			$contact_info->addElement('text','email', array(
				'label' => 'Email',
				'filters' => array(
					'StringTrim',
				),
				'validators' => array(
					'EmailAddress',
				),
				'required' => true,
			));
		}
		else
		{
			$resident_info->addElement('hidden','email');
		}

		// Preferred Email
		$contact_info->addElement('text','preferred_email', array(
			'label' => 'Preferred Email',
			'filters' => array(
				'StringTrim',
			),
			'validators' => array(
				'EmailAddress',
			),
		));

        // Country select options
        $countries = \DF\Service\Geography::getCountries();

        // Mailing Address
        $mailing_address = new \Zend_Form_SubForm('mailing_address_subform');
        $mailing_address->setLegend('Mailing Address');
        $this->addSubForm($mailing_address, 'mailing_address_subform');
        
            $mailing_address->addElement('text', 'mailing_address', array(
                'label' => 'Mailing Address',
                'filters' => array(
                    'StringTrim',
                ),
            ));
            
            $mailing_address->addElement('text', 'mailing_address2', array(
                'label' => 'Mailing Address 2',
                'filters' => array(
                    'StringTrim',
                ),
            ));
            
            $mailing_address->addElement('text', 'mailing_city', array(
                'label' => 'Mailing City',
                'filters' => array(
                    'StringTrim',
                ),
            ));
            
            $mailing_address->addElement('text', 'mailing_state', array(
                'label' => 'Mailing State/Province',
                'filters' => array(
                    'StringTrim',
                ),
            ));
            
            $mailing_address->addElement('text', 'mailing_zip', array(
                'label' => 'Mailing Zip/Postal Code',
                'filters' => array(
                    'StringTrim',
                ),
            ));
            
            $mailing_address->addElement('select', 'mailing_country', array(
                'label' => 'Mailing Country',
                'multiOptions' => $countries,
            ));

        // Permanent Address
        $permanent_address = new \Zend_Form_SubForm('permanent_address_subform');
        $permanent_address->setLegend('Permanent Address');
        $this->addSubForm($permanent_address, 'permanent_address_subform');

            $permanent_address->addElement('text', 'permanent_address', array(
                'label' => 'Permanent Address',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $permanent_address->addElement('text', 'permanent_address2', array(
                'label' => 'Permanent Address 2',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $permanent_address->addElement('text', 'permanent_city', array(
                'label' => 'Permanent City',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $permanent_address->addElement('text', 'permanent_state', array(
                'label' => 'Permanent State/Province',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $permanent_address->addElement('text', 'permanent_zip', array(
                'label' => 'Permanent Zip/Postal Code',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $permanent_address->addElement('select', 'permanent_country', array(
                'label' => 'Permanent Country',
                'multiOptions' => $countries,
            ));

        // Forwarding Address
        $forwarding_address = new \Zend_Form_SubForm('forwarding_address_subform');
        $forwarding_address->setLegend('Forwarding Address');
        $this->addSubForm($forwarding_address, 'forwarding_address_subform');

            $forwarding_address->addElement('text', 'forwarding_address', array(
                'label' => 'Forwarding Address',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $forwarding_address->addElement('text', 'forwarding_address2', array(
                'label' => 'Forwarding Address 2',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $forwarding_address->addElement('text', 'forwarding_city', array(
                'label' => 'Forwarding City',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $forwarding_address->addElement('text', 'forwarding_state', array(
                'label' => 'Forwarding State/Province',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $forwarding_address->addElement('text', 'forwarding_zip', array(
                'label' => 'Forwarding Zip/Postal Code',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $forwarding_address->addElement('select', 'forwarding_country', array(
                'label' => 'Forwarding Country',
                'multiOptions' => $countries,
            ));

            $forwarding_address->addElement('textarea', 'forwarding_comments', array(
                'label' => 'Forwarding Comments',
                'filters' => array(
                    'StringTrim',
                ),
                'attribs' => array(
                    'style' => 'height: 15em;',
                ),
            ));

        // School Info
        $school_info = new \Zend_Form_SubForm('school_info_subform');
        $school_info->setLegend('School Info');
        $this->addSubForm($school_info, 'school_info_subform');

            $school_info->addElement('select', 'classification', array(
                'label' => 'Classification',
                'multiOptions' => array(
                    '' => '',
                    'U1' => 'Freshman',
                    'U2' => 'Sophomore',
                    'U3' => 'Junior',
                    'U4' => 'Senior',
                    'U0' => 'UG NDS',
                    'U5' => 'PB UG',
                    'G7' => 'Masters',
                    'G8' => 'Doctoral',
                    'G9' => 'Grad. Cond.',
                    'V1' => 'Vet-1',
                    'V2' => 'Vet-2',
                    'V3' => 'Vet-3',
                    'V4' => 'Vet-4',
                    'M1' => 'Med-1',
                    'M2' => 'Med-2',
                    'M3' => 'Med-3',
                    'M4' => 'Med-4',
                    'P' => 'Post-Doctoral',
                ),
            ));

            $school_info->addElement('select', 'college_id', array(
                'label' => 'College',
                'multiOptions' => College::fetchSelect(),
            ));

            $school_info->addElement('select', 'major_id', array(
                'label' => 'Major',
                'multiOptions' => Major::fetchSelect(),
            ));

        // Medical Info
        $medical_info = new \Zend_Form_SubForm('medical_info_subform');
        $medical_info->setLegend('Medical Info');
        $this->addSubForm($medical_info, 'medical_info_subform');

            $medical_info->addElement('select', 'meningitis_vaccination', array(
                'label' => 'Meningitis Vaccination',
                'multiOptions' => array(
                    'N' => 'No',
                    'Y' => 'Yes',
                    'M' => 'Medical',
                    'C' => 'Conscientious Objector',
                    'A' => 'Administrative Exemption',
                ),
            ));

            $medical_info->addElement('unixdate', 'meningitis_vacc_proof', array(
                'label' => 'Meningitis Vaccination Proof',
            ));

            $medical_info->addElement('unixdate', 'meningitis_vacc_action', array(
                'label' => 'Meningitis Vaccination Action',
            ));

        // Demographic Info
        $demographic_info = new \Zend_Form_SubForm('demographic_info_subform');
        $demographic_info->setLegend('Demographic Info');
        $this->addSubForm($demographic_info, 'demographic_info_subform');

            $demographic_info->addElement('select', 'birth_country', array(
                'label' => 'Country of Origin',
                'multiOptions' => $countries,
            ));

            if ($manage)
            {
                $demographic_info->addElement('checkbox', 'international_student', array(
                    'label' => 'International Student',
                ));
            }
            else
            {
                $demographic_info->addElement('hidden', 'international_student');
            }

            $demographic_info->addElement('select', 'gender', array(
                'label' => 'Gender',
                'multiOptions' => array(
                    'F' => 'Female',
                    'M' => 'Male',
                    'O' => 'Other',
                ),
            ));

            $demographic_info->addElement('select', 'marital_status', array(
                'label' => 'Marital Status',
                'multiOptions' => array(
                    '' => '',
                    'S' => 'Single',
                    'M' => 'Married',
                    'P' => 'Single Parent',
                ),
            ));

            $demographic_info->addElement('text', 'number_of_children', array(
                'label' => 'Number of Children',
                'attribs' => array(
                    'size' => '1',
                ),
            ));

            if ($manage)
            {
                $demographic_info->addElement('unixdate', 'marriage_certificate', array(
                    'label' => 'Marriage Certificate',
                ));

                $demographic_info->addElement('unixdate', 'birth_certificate', array(
                    'label' => 'Birth Certificate',
                ));
            }
            else
            {
                $demographic_info->addElement('hidden', 'marriage_certificate');

                $demographic_info->addElement('hidden', 'birth_certificate');
            }
            
        // Emergency Contact 1
        $emergency_contact_info = new \Zend_Form_SubForm('emergency_contact_info_subform');
        $emergency_contact_info->setLegend('Emergency Contact 1');
        $emergency_contact_info->setDescription('Emergency Contact Information: During the housing application process, a student living on campus is obligated to provide at least one emergency contact to the Department of Residence Life. This information will be kept confidential and will only be utilized in the case of an emergency involving the student such as death or a life threatening injury. This information will only be accessed by Department of Residence Life staff or other TAMU staff members with emergency response job responsibilities.');
        $this->addSubForm($emergency_contact_info, 'emergency_contact_info_subform');

            // Relationship
            $emergency_contact_info->addElement('select','emergency_relationship', array(
                'label' => 'Relationship',
                'multiOptions' => array(
                    'parent' => 'Parent',
                    'spouse' => 'Spouse',
                    'sibling' =>'Sibling',
                    'friend' => 'Friend',
                    'grandparent' => 'Grandparent',
                    'advisor' => 'Advisor',
                    'other' => 'Other',
                ),
            ));
            
            // Missing Persons Contact?
            $emergency_contact_info->addElement('checkbox','emergency_missing_person', array(
                'label' => 'use as a Missing Person Contact',
                'description' => 'Missing Person Contact Option: On-campus students have the option to designate at least one of their emergency contacts as a "missing person contact" to be notified if a student is determined to be missing for more than 24 hours. We encourage every student to designate at least one emergency contact as a missing person contact, but there is no obligation. If a student is under 18 years of age, Federal Law requires that TAMU notify a parent or guardian within 24 hours of the determination that a student is missing - whether or not a student has designated an emergency contact as a person to be notified.',
            ));
            
            // First Name
            $emergency_contact_info->addElement('text','emergency_firstname', array(
                'label' => 'First Name',
                'required' => true,
            ));

            // Middle Name
            $emergency_contact_info->addElement('text','emergency_middlename', array(
                'label' => 'Middle Name',
            ));

            // Last Name
            $emergency_contact_info->addElement('text','emergency_lastname', array(
                'label' => 'Last Name',
                'required' => true,
            ));
            
            $emergency_contact_info->addElement('text', 'emergency_address', array(
                'label' => 'Address',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency_contact_info->addElement('text', 'emergency_address2', array(
                'label' => 'Address 2',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency_contact_info->addElement('text', 'emergency_city', array(
                'label' => 'City',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency_contact_info->addElement('text', 'emergency_state', array(
                'label' => 'State/Province',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency_contact_info->addElement('text', 'emergency_zip', array(
                'label' => 'Zip/Postal Code',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency_contact_info->addElement('select', 'emergency_country', array(
                'label' => 'Country',
                'multiOptions' => $countries,
            ));
            
            // Phone
            $emergency_contact_info->addElement('text','emergency_phone', array(
                'label' => 'Phone',
                'filters' => array(
                    'Phone',
                    'StringTrim',
                ),
                'validators' => array(
                    'Phone',
                ),
                'required' => true,
            ));
            
        // Emergency Contact 2
        $emergency2_contact_info = new \Zend_Form_SubForm('emergency2_contact_info_subform');
        $emergency2_contact_info->setLegend('Emergency Contact 2');
        $this->addSubForm($emergency2_contact_info, 'emergency2_contact_info_subform');

            // Relationship
            $emergency2_contact_info->addElement('select','emergency2_relationship', array(
                'label' => 'Relationship',
                'multiOptions' => array(
                    'parent' => 'Parent',
                    'spouse' => 'Spouse',
                    'sibling' =>'Sibling',
                    'friend' => 'Friend',
                    'grandparent' => 'Grandparent',
                ),
            ));
            
            // Missing Persons Contact?
            $emergency2_contact_info->addElement('checkbox','emergency2_missing_person', array(
                'label' => 'use as a Missing Person Contact',
            ));
            
            // First Name
            $emergency2_contact_info->addElement('text','emergency2_firstname', array(
                'label' => 'First Name',
            ));

            // Middle Name
            $emergency2_contact_info->addElement('text','emergency2_middlename', array(
                'label' => 'Middle Name',
            ));

            // Last Name
            $emergency2_contact_info->addElement('text','emergency2_lastname', array(
                'label' => 'Last Name',
            ));
            
            $emergency2_contact_info->addElement('text', 'emergency2_address', array(
                'label' => 'Address',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency2_contact_info->addElement('text', 'emergency2_address2', array(
                'label' => 'Address 2',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency2_contact_info->addElement('text', 'emergency2_city', array(
                'label' => 'City',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency2_contact_info->addElement('text', 'emergency2_state', array(
                'label' => 'State/Province',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency2_contact_info->addElement('text', 'emergency2_zip', array(
                'label' => 'Zip/Postal Code',
                'filters' => array(
                    'StringTrim',
                ),
            ));

            $emergency2_contact_info->addElement('select', 'emergency2_country', array(
                'label' => 'Country',
                'multiOptions' => $countries,
            ));
            
            // Phone
            $emergency2_contact_info->addElement('text','emergency2_phone', array(
                'label' => 'Phone',
                'filters' => array(
                    'Phone',
                    'StringTrim',
                ),
                'validators' => array(
                    'Phone',
                ),
            ));

        // Notes
        $notes = new \Zend_Form_SubForm('notes_subform');
        $notes->setLegend('Notes');
        $this->addSubForm($notes, 'notes_subform');

            $notes->addElement('textarea', 'notes', array(
                'attribs' => array(
                    'style' => 'height: 15em;',
                ),
            ));

        
        $this->addElement('submit', 'submit', array(
            'label' => 'Save Changes',
            'attribs' => array(
                'class' => 'ui-button',
            ),
        ));

        if( $this->_resident instanceof Resident )
        {
            if ($this->_resident->user instanceof User)
            {
                $user = $this->_resident->User;
                $default_firstname = $user->firstname;
                $default_lastname = $user->lastname;
                $default_uin = $user->uin;
                $default_email = $user->email;
                $default_phone = $user->phone;

                $compass = new \DF\Service\DoitApi;
                $student_record = $compass->getStudentById($user->uin);
                if ($student_record['classification'])
                {
                    $classification = $student_record['classification'];
                }
                else
                {
                    $classification = NULL;
                }
                if ($student_record['major1'])
                {
                    $major = Major::getOrCreateByCode($student_record['major1']);
                    $major_id = $major->id;
                }
                else
                {
                    $major_id = NULL;
                }
                if ($student_record['college'])
                {
                    $college = College::getOrCreateByCode($student_record['college']);
                    $college_id = $college->id;
                }
                else
                {
                    $college_id = NULL;
                }
            }
            $this->setDefaults(array(
                'principal_resident' => $this->_resident->principal_resident,
                'resident_type_id' => (trim($this->_resident->resident_type_id) != '' ? $this->_resident->resident_type_id : 1),

                'first_name' => trim($this->_resident->first_name) != '' ? $this->_resident->first_name : $default_firstname,
                'middle_name' => $this->_resident->middle_name,
                'surname' => trim($this->_resident->surname) != '' ? $this->_resident->surname : $default_lastname,
                'suffix' => $this->_resident->suffix,
                'uin' => trim($this->_resident->uin) != '' ? $this->_resident->uin : $default_uin,
                'uin_sub_id' => $this->_resident->uin_sub_id,
                'birth_date' => $this->_resident->birth_date,

                'primary_phone' => trim($this->_resident->primary_phone) != '' ? $this->_resident->primary_phone : $default_phone,
                'secondary_phone' => $this->_resident->secondary_phone,
                'voip_phone' => $this->_resident->voip_phone,
                'email' => trim($this->_resident->email) != '' ? $this->_resident->email : $default_email,
                'preferred_email' => $this->_resident->preferred_email,

                'mailing_address' => $this->_resident->mailing_address,
                'mailing_address2' => $this->_resident->mailing_address2,
                'mailing_city' => $this->_resident->mailing_city,
                'mailing_state' => $this->_resident->mailing_state,
                'mailing_zip' => $this->_resident->mailing_zip,
                'mailing_country' => $this->_resident->mailing_country,

                'permanent_address' => $this->_resident->permanent_address,
                'permanent_address2' => $this->_resident->permanent_address2,
                'permanent_city' => $this->_resident->permanent_city,
                'permanent_state' => $this->_resident->permanent_state,
                'permanent_zip' => $this->_resident->permanent_zip,
                'permanent_country' => $this->_resident->permanent_country,

                'forwarding_address' => $this->_resident->forwarding_address,
                'forwarding_address2' => $this->_resident->forwarding_address2,
                'forwarding_city' => $this->_resident->forwarding_city,
                'forwarding_state' => $this->_resident->forwarding_state,
                'forwarding_zip' => $this->_resident->forwarding_zip,
                'forwarding_country' => $this->_resident->forwarding_country,
                'forwarding_comments' => $this->_resident->forwarding_comments,

                'class' => $this->_resident->class,
                'classification' => ($this->_resident->classification ? $this->_resident->classification : $classification),
                'college_id' => ($this->_resident->college_id ? $this->_resident->college_id : $college_id),
                'major_id' => ($this->_resident->major_id ? $this->_resident->major_id : $major_id),

                'meningitis_vaccination' => $this->_resident->meningitis_vaccination,
                'meningitis_vacc_proof' => (trim($this->_resident->meningitis_vacc_proof)!='') ? $this->_resident->meningitis_vacc_proof : null,
                'meningitis_vacc_action' => (trim($this->_resident->meningitis_vacc_action)!='') ? $this->_resident->meningitis_vacc_action : null,

                'birth_country' => $this->_resident->birth_country,
                'gender' => $this->_resident->gender,
                'marital_status' => $this->_resident->marital_status,
                'number_of_children' => $this->_resident->number_of_children,
                'marriage_certificate' => (trim($this->_resident->marriage_certificate)!='') ? $this->_resident->marriage_certificate : null,
                'birth_certificate' => (trim($this->_resident->birth_certificate)!='') ? $this->_resident->birth_certificate : null,
                'laundry_card' => $this->_resident->laundry_card,
                'orientation_date' => (trim($this->_resident->orientation_date)!='') ? $this->_resident->orientation_date : null,

                'emergency_relationship' => $this->_resident->emergency_relationship,
                'emergency_missing_person' => $this->_resident->emergency_missing_person,
                'emergency_firstname' => $this->_resident->emergency_firstname,
                'emergency_middlename' => $this->_resident->emergency_middlename,
                'emergency_lastname' => $this->_resident->emergency_lastname,
                'emergency_address' => $this->_resident->emergency_address,
                'emergency_address2' => $this->_resident->emergency_address2,
                'emergency_city' => $this->_resident->emergency_city,
                'emergency_state' => $this->_resident->emergency_state,
                'emergency_zip' => $this->_resident->emergency_zip,
                'emergency_country' => $this->_resident->emergency_country,
                'emergency_phone' => $this->_resident->emergency_phone,
                
                'emergency2_relationship' => $this->_resident->emergency2_relationship,
                'emergency2_missing_person' => $this->_resident->emergency2_missing_person,
                'emergency2_firstname' => $this->_resident->emergency2_firstname,
                'emergency2_middlename' => $this->_resident->emergency2_middlename,
                'emergency2_lastname' => $this->_resident->emergency2_lastname,
                'emergency2_address' => $this->_resident->emergency2_address,
                'emergency2_address2' => $this->_resident->emergency2_address2,
                'emergency2_city' => $this->_resident->emergency2_city,
                'emergency2_state' => $this->_resident->emergency2_state,
                'emergency2_zip' => $this->_resident->emergency2_zip,
                'emergency2_country' => $this->_resident->emergency2_country,
                'emergency2_phone' => $this->_resident->emergency2_phone,
                
                'notes' => $this->_resident->notes,
            ));
        }
    }
}