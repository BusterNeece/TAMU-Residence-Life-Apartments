<?php
namespace UA\Form;

use \Entity\Resident;
use \Entity\Application;

class Apply extends \DF\Form\Custom
{
    protected $_resident;
    protected $_is_roommate;
    protected $_application;

    public function __construct(Resident $resident, $options = null)
    {
        $this->_resident = $resident;
        parent::__construct($options);
    }
    
    public function getResident()
    {
        return $this->_resident;
    }
    
    public function getApplication()
    {
        return $this->_application;
    }
    
    public function isRoommateRequest()
    {
        return ($this->_is_roommate) ? true : false;
    }
    
    public function isValid($data) {
        $user_uin = \DF\Auth::getLoggedInUser()->uin;
        if ($data['signatures_subform']['uin_signature'] != $user_uin)
        {
            $this->getSubForm('signatures_subform')->getElement('uin_signature')->addError('UIN entered for signature did not match logged in user.');
            return false;
        }
        parent::isValid($data);
    }

    public function save()
    {
        $data = $this->getValues();
        $resident = $this->getResident();
        $application = $this->getApplication();

        // Resident info
        $resident->resident_type_id = $data['resident_info_subform']['resident_type_id'];

        $resident->first_name = $data['resident_info_subform']['first_name'];
        $resident->middle_name = $data['resident_info_subform']['middle_name'];
        $resident->surname = $data['resident_info_subform']['surname'];
        $resident->suffix = $data['resident_info_subform']['suffix'];
        $resident->uin = $data['resident_info_subform']['uin'];

        // Contact info
        $resident->primary_phone = $data['contact_info_subform']['primary_phone'];
        $resident->secondary_phone = $data['contact_info_subform']['secondary_phone'];
        $resident->voip_phone = $data['contact_info_subform']['voip_phone'];
        $resident->email = $data['contact_info_subform']['email'];
        $resident->preferred_email = $data['contact_info_subform']['preferred_email'];

        // Mailing address
        $resident->mailing_address = $data['mailing_address_subform']['mailing_address'];
        $resident->mailing_address2 = $data['mailing_address_subform']['mailing_address2'];
        $resident->mailing_city = $data['mailing_address_subform']['mailing_city'];
        $resident->mailing_state = $data['mailing_address_subform']['mailing_state'];
        $resident->mailing_zip = $data['mailing_address_subform']['mailing_zip'];
        $resident->mailing_country = (trim($data['mailing_address_subform']['mailing_country'])!='null') ? $data['mailing_address_subform']['mailing_country'] : null;

        // Application-specific fields
        $application->desired_move_in = $data['app_specific_fields']['desired_move_in'];
        $application->floor_preference = $data['app_specific_fields']['floor_preference'];
        $application->special_needs = $data['app_specific_fields']['special_needs'];
        $application->military_vet = (trim($data['app_specific_fields']['military_vet']) != '') ? 1 : 0;
        $application->police_fireprotection = (trim($data['app_specific_fields']['police_fireprotection']) != '') ? 1 : 0;
        $resident->birth_country = (trim($data['app_specific_fields']['birth_country']) != 'null') ? $data['app_specific_fields']['birth_country'] : null;
        $resident->international_student = (trim($data['app_specific_fields']['international_student']) != '') ? 1: 0;

        // Emergency Contact 1
        $resident->emergency_missing_person = (trim($data['emergency_contact_info_subform']['emergency_missing_person'])!='') ? 1 : 0;
        $resident->emergency_firstname = $data['emergency_contact_info_subform']['emergency_firstname'];
        $resident->emergency_middlename = $data['emergency_contact_info_subform']['emergency_middlename'];
        $resident->emergency_lastname = $data['emergency_contact_info_subform']['emergency_lastname'];
        $resident->emergency_address = $data['emergency_contact_info_subform']['emergency_address'];
        $resident->emergency_address2 = $data['emergency_contact_info_subform']['emergency_address2'];
        $resident->emergency_city = $data['emergency_contact_info_subform']['emergency_city'];
        $resident->emergency_state = $data['emergency_contact_info_subform']['emergency_state'];
        $resident->emergency_zip = $data['emergency_contact_info_subform']['emergency_zip'];
        $resident->emergency_country = (trim($data['emergency_contact_info_subform']['emergency_country'])!='null') ? $data['emergency_contact_info_subform']['emergency_country'] : null;
        $resident->emergency_phone = $data['emergency_contact_info_subform']['emergency_phone'];
        $resident->emergency_relationship = $data['emergency_contact_info_subform']['emergency_relationship'];

        // Emergency Contact 2
        $resident->emergency2_missing_person = (trim($data['emergency2_contact_info_subform']['emergency2_missing_person'])!='') ? 1 : 0;
        $resident->emergency2_firstname = $data['emergency2_contact_info_subform']['emergency2_firstname'];
        $resident->emergency2_middlename = $data['emergency2_contact_info_subform']['emergency2_middlename'];
        $resident->emergency2_lastname = $data['emergency2_contact_info_subform']['emergency2_lastname'];
        $resident->emergency2_address = $data['emergency2_contact_info_subform']['emergency2_address'];
        $resident->emergency2_address2 = $data['emergency2_contact_info_subform']['emergency2_address2'];
        $resident->emergency2_city = $data['emergency2_contact_info_subform']['emergency2_city'];
        $resident->emergency2_state = $data['emergency2_contact_info_subform']['emergency2_state'];
        $resident->emergency2_zip = $data['emergency2_contact_info_subform']['emergency2_zip'];
        $resident->emergency2_country = (trim($data['emergency2_contact_info_subform']['emergency2_country'])!='null') ? $data['emergency2_contact_info_subform']['emergency2_country'] : null;
        $resident->emergency2_phone = $data['emergency2_contact_info_subform']['emergency2_phone'];
        $resident->emergency2_relationship = $data['emergency2_contact_info_subform']['emergency2_relationship'];
        
        if (!$this->isRoommateRequest())
        {
            $resident->principal_resident = 1;
            // Apartment Style choices
            foreach($data['apartment_types_subform'] as $typeid => $value)
            {
                $type_parts = explode('_', $typeid);
                if (((int)$type_parts[1] > 0) && ((int)$value > 0))
                    $asset_types[(int)$value] = (int)$type_parts[1];
            }
            foreach($asset_types as $choice_rank => $asset_type_id)
            {
                $application->WaitingList[$choice_rank - 1]->choice_asset_type_id = $asset_type_id;
                $application->WaitingList[$choice_rank - 1]->choice_rank = $choice_rank;
            }
        }

        $application->resident_id = $resident->id;

        $application->contract_version = $data['signatures_subform']['contract_version'];
        $application->contract_initials = $data['signatures_subform']['contract_initials'];
        $application->contract_initials_date = DF_TIME;
        
        if ($this->isRoommateRequest())
        {
            $roommate_request = RoommateRequest::fetchByResidentId($resident->id);
            $roommate_request->application_id = $application->id;
            $roommate_request->save();
        }

        try
        {
            $resident->save();
            $application->save();
            
            return true;
        }
        catch( Doctrine_Exception $e )
        {
            $this->setErrors(array(
                $e->getMessage()
            ));
            
            return false;
        }
    }

    public function init()
    {
        $resident = $this->getResident();
        
        // check if there is a roommate request
        $roommate_request = RoommateRequest::fetchByResidentId($resident->id);
        $this->_is_roommate = ($roommate_request) ? true : false;
        
        // application form
        $config = Zend_Registry::get('config');
        $appeditform = new DF_Form($config->forms->application_edit);
        // remove admin-level fields for end users
        $appeditform->removeElement('application_date');
        $appeditform->removeElement('application_status_id');
        $appeditform->removeElement('response_deadline');
        $appeditform->removeElement('submit');
        $appform = new \Zend_Form_SubForm();
        $appform->setLegend('Other')
                ->addElements($appeditform->getDisplayGroup('application_details')->getElements());
        $appform->addElement('select','birth_country',array(
            'label' => 'Country of Origin',
            'multiOptions' => \DF\Service\Geography::getCountries(),
            'required' => true,
        ));
        $appform->addElement('checkbox', 'international_student', array(
            'label' => 'Are you an International Student?',
        ));
        
        $application = new Application();
        $application->application_date = DF_TIME;
        $application->application_status_id = 1;
        $this->_application = $application;

        // get resident form
        $resform = new UA_Custom_Form_Resident_Edit($resident);
        $resinfo = $resform->getSubForm('resident_info_subform');
        $resinfo->removeElement('principal_resident');
        $resinfo->removeElement('laundry_card');
        $resinfo->removeElement('orientation_date');
        $resinfo->removeElement('uin_sub_id');
        $contact = $resform->getSubForm('contact_info_subform');
        $address = $resform->getSubForm('mailing_address_subform');
        $choices = new \Zend_Form_Subform();
        $choices->setLegend('Apartment Styles')
                ->setDescription('Select your first, second, and subsequent choices below:');
        $choices->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
            array('Label', array('tag' => 'span')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div')),
        ));
        foreach(AssetType::fetchApartmentTypes() as $id => $type)
        {
            $choices->addElement('select','type_'.$id, array(
                'label' => $type,
                'multiOptions' => array(
                    '0' => '',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                ),
            ));
        }
        // TEMPORARY: add message to last element
        $choices->getElement('type_'.$id)
                ->setDescription('<p style="font-weight:bold; color:red;">All College Avenue apartments will be demolished in summer 2011</p>')
                ->addDecorator('Description', array('escape' => false));
        // remove above when College Ave is demolished

        if ($application instanceof Application)
        {
            foreach($application->WaitingList as $rank => $asset_type_id)
            {
                $choices->setDefault('type_'.$asset_type_id, $rank + 1);
            }
        }

        $emergency1 = $resform->getSubForm('emergency_contact_info_subform');
        $emergency2 = $resform->getSubForm('emergency2_contact_info_subform');
        
        $signatures = new \Zend_Form_SubForm('signatures_subform');
            $signatures->setLegend('Signature');

            $signatures->addElement('hidden', 'contract_version', array(
                'value' => Settings::getSetting('contract_version'),
            ));

            $signatures->addElement('text', 'uin_signature', array(
                'label' => 'Enter UIN here',
                'required' => true,
            ));
            
            $signatures->addElement('text', 'contract_initials', array(
                'label' => 'Enter initials here',
                'required' => true,
                'description' => 'By entering my UIN and initials here I acknowledge that I have read and agree to the terms of the previously reviewed contract. I understand that this application will be discarded unless online payment is completed on the following screens.',
                'attribs' => array(
                    'size' => 4,
                ),
            ));
            
            $signatures->addElement('hidden', 'contract_version', array(
                'label' => '',
                'value' => Settings::getSetting('application_contract_version'),
            ));

        // add resident subforms to app form
        $this->addSubForm($resinfo, $resinfo->getName());
        $this->addSubForm($contact, $contact->getName());
        $this->addSubForm($address, $address->getName());
        $this->addSubForm($appform, 'app_specific_fields');
        $this->addSubForm($emergency1, 'emergency_contact_info_subform');
        $this->addSubForm($emergency2, 'emergency2_contact_info_subform');
        if (!$this->isRoommateRequest()) $this->addSubForm($choices, 'apartment_types_subform');
        $this->addSubForm($signatures, 'signatures_subform');
        
        $this->addElement('submit', 'submit', array(
            'label' => 'Submit Application',
            'attribs' => array(
                //'helper' => 'formButton',
                'class' => 'ui-button',
            ),
        ));
        
        
    }
}