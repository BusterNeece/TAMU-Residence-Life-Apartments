<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Application
 *
 * @Table()
 * @Entity
 */
class Application extends \DF\Doctrine\Entity
{
    const STATUS_WAITING = 1;
    const STATUS_RENEWAL = 2;
    const STATUS_OFFERED = 6;
    const STATUS_DEPOSIT = 3;

    public function __construct()
    {
        $this->waiting_list = new ArrayCollection;
        $this->application_fee = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="application_date", type="integer", nullable=true) */
    protected $application_date;

    /** @Column(name="application_status_id", type="integer", nullable=true) */
    protected $application_status_id;

    public function getStatusName()
    {
        $names = self::getStatusNames();
        return $names[$this->application_status_id];
    }

    /** @Column(name="floor_preference", type="string", length=1, nullable=true) */
    protected $floor_preference;

    /** @Column(name="special_needs", type="text", nullable=true) */
    protected $special_needs;

    /** @Column(name="application_fee_id", type="decimal", precision=18, scale=2, nullable=true) */
    protected $application_fee_id;

    /** @Column(name="desired_move_in", type="integer", nullable=true) */
    protected $desired_move_in;

    /** @Column(name="military_vet", type="integer", nullable=true) */
    protected $military_vet;

    /** @Column(name="police_fireprotection", type="integer", nullable=true) */
    protected $police_fireprotection;

    /** @Column(name="is_renewal", type="integer", nullable=true) */
    protected $is_renewal;

    /** @Column(name="occupancy_id", type="integer", nullable=true) */
    protected $occupancy_id;

    /** @Column(name="requested_lease_length", type="integer", nullable=true) */
    protected $requested_lease_length;

    /** @Column(name="requested_occupancy_model", type="integer", nullable=true) */
    protected $requested_occupancy_model;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Occupancy")
     * @JoinColumn(name="occupancy_id", referencedColumnName="id")
     */
    protected $occupancy;

    /**
     * @ManyToOne(targetEntity="OccupancyModel")
     * @JoinColumn(name="requested_occupancy_model", referencedColumnName="id")
     */
    protected $occupancy_model;

    /** @OneToMany(targetEntity="WaitingList", mappedBy="application") */
    protected $waiting_list;

    /** @OneToMany(targetEntity="ApplicationFee", mappedBy="application") */
    protected $application_fee;

    /**
     * Static Functions
     */

    public static function getStatusNames()
    {
        return array(
            self::STATUS_WAITING        => 'Waiting',
            self::STATUS_RENEWAL        => 'Renewal/Conversion',
            self::STATUS_OFFERED        => 'Offered',
            self::STATUS_DEPOSIT        => 'Deposit',
        );
    }

    public static function getById($id)
    {
        return self::find($id);
    }

    public function fetchWaitingListTypes()
    {
        $choices = array();
        foreach($this->waiting_list as $choice)
        {
            if ($choice->choice_asset_type_id > 0)
                $choices[$choice->choice_rank] = $choice->choice_asset_type_id;
        }
        asort($choices);
        return $choices;
    }

    public function fetchWaitingListMatches()
    {
        if (!$this->desired_move_in)
            return false;
        
        $choices = $this->fetchWaitingListTypes();

        $matches = Asset::fetchWaitingListMatches($choices, $this->desired_move_in);

        $ranked = array();
        foreach($matches as $match)
        {
            $ranked['type-'.$match['asset_type_id']]['id-'.$match['id']] = $match['name'].' ('.$match['AssetType']['description'].')';
        }
        $offerable = array();
        foreach($choices as $rank => $type)
        {
            if (is_array($ranked['type-'.$type]))
                $offerable = array_merge($offerable, $ranked['type-'.$type]);
        }

        if (count($offerable) > 0)
            return $offerable;
        else
            return false;
    }

    public function calculateRank($type)
    {
        $em = self::getEntityManager();
        $all_records = $em->createQuery('SELECT w, a FROM Entity\Waitinglist w JOIN w.application a WHERE w.choice_asset_type_id = :type AND a.application_status_id = :status AND a.application_date <= :date AND a.desired_move_in <= :move_in ORDER BY a.application_date ASC, a.desired_move_in ASC')
            ->setParameter('type', (int)$type)
            ->setParameter('status', self::STATUS_WAITING)
            ->setParameter('date', $this->application_date)
            ->setParameter('move_in', $this->desired_move_in)
            ->getArrayResult();

        if ($all_records)
        {
            $apps = array();
            foreach($all_records as $record)
            {
                $apps[$record['application_id']] = $record['choice_asset_type_id'];
            }
            $rank = array_search($this->id, array_keys($apps));

            if ($rank !== false)
                return $rank + 1;
        }
        return 1;
    }
}