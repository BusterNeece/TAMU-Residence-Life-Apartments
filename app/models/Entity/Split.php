<?php
namespace Entity;

use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Split
 *
 * @Table()
 * @Entity
 */
class Split extends \DF\Doctrine\Entity
{
    const LEDGER_STANDARD = 0;
    const LEDGER_DEPOSITS = 1;
    
    const RECURRING_INACTIVE = 0;
    const RECURRING_DAILY = 1;
    const RECURRING_MONTHLY = 2;

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(name="ledger_id", type="integer", nullable=true)
     */
    protected $ledger_id;

    /**
     * @Column(name="account_id", type="integer", nullable=true)
     */
    protected $account_id;

    /**
     * @Column(name="resident_id", type="integer", nullable=true)
     */
    protected $resident_id;

    /**
     * @Column(name="credit_or_debit", type="string", length=1, nullable=true)
     */
    protected $credit_or_debit;

    /**
     * @Column(name="user_id", type="integer", nullable=true)
     */
    protected $user_id;

    /**
     * @Column(name="item_id", type="integer", nullable=true)
     */
    protected $item_id;

    /**
     * @Column(name="asset_id", type="integer", nullable=true)
     */
    protected $asset_id;

    /**
     * @Column(name="matching_split_id", type="integer", nullable=true)
     */
    protected $matching_split_id;

    /**
     * @Column(name="payment_method_id", type="integer", nullable=true)
     */
    protected $payment_method_id;

    /**
     * @Column(name="posted", type="integer", nullable=true)
     */
    protected $posted;

    /**
     * @Column(name="resident_received_from_id", type="integer", nullable=true)
     */
    protected $resident_received_from_id;

    /**
     * @Column(name="running_balance", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $running_balance;

    /**
     * @Column(name="split_amount", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $split_amount;

    /**
     * @Column(name="transaction_id", type="integer", nullable=true)
     */
    protected $transaction_id;

    /**
     * @var integer $due_date
     *
     * @Column(name="due_date", type="integer", nullable=true)
     */
    protected $due_date;

    /**
     * @var integer $recurring_type
     *
     * @Column(name="recurring_type", type="boolean", nullable=true)
     */
    protected $recurring_type;

    /**
     * @var integer $recurring_active
     *
     * @Column(name="recurring_active", type="boolean", nullable=true)
     */
    protected $recurring_active;

    /**
     * @var integer $recurring_parent_id
     *
     * @Column(name="recurring_parent_id", type="integer", nullable=true)
     */
    protected $recurring_parent_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;

    /**
     * @ManyToOne(targetEntity="RegisterTransaction")
     * @JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    protected $transaction;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Item")
     * @JoinColumn(name="item_id", referencedColumnName="id")
     */
    protected $item;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    public function getLedgerText()
    {
        $ledger_ids = self::getLedgerIds();
        return $ledger_ids[$this->ledger_id];
    }

    public function preInsert()
    {
        if (!$this->posted)
            $this->posted = time();
        
        if (PHP_SAPI != "cli")
        {
            $user = DF_Auth::getInstance()->getLoggedInUser();
            if ($user instanceof User)
            {
                $log = new SplitLog();
                $log->split_id = $this->id;
                $log->user_id = $user->id;
                $log->timestamp = time();
                $log->changes = 'Transaction item created.';
            }
        }
    }
    
    public function preUpdate()
    {
        $modified_fields = $this->getModified(FALSE, TRUE);
        if ($modified_fields)
        {
            $changes = 'Modified transaction item, updated '.implode(', ', array_keys($modified_fields));
            
            if (PHP_SAPI != "cli")
            {
                $user = DF_Auth::getInstance()->getLoggedInUser();
                if ($user instanceof User)
                {
                    $log = new SplitLog();
                    $log->split_id = $this->id;
                    $log->user_id = $user->id;
                    $log->timestamp = time();
                    $log->changes = $changes;
                }
            }
        }
    }

    /**
     * Static Functions
     */

    public static function getLedgerIds()
    {
        return array(
            self::LEDGER_STANDARD   => 'Standard',
            self::LEDGER_DEPOSITS   => 'Deposits',
        );
    }

    public static function fetchById($id)
    {
        return self::find($id);
    }

    public static function fetchByResidentId($id, $ledger_id = NULL, $outstanding_only = FALSE)
    {
        $em = self::getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('s')
            ->from(__CLASS__, 's')
            ->where('s.resident_id = :id')->setParameter('id', $id)
            ->orderBy('s.id', 'ASC');
        
        if ($outstanding_only)
            $query->andWhere('(s.matching_split_id IS NULL AND s.credit_or_debit = :debit)')->setParameter('debit', 'D');
        
        if (!is_null($ledger_id))
            $query->andWhere('s.ledger_id = :ledger_id')->setParameter('ledger_id', $ledger_id);
        
        return $query->getQuery()->execute();
    }
    
    public static function getBalanceByResidentId($id, $ledger_id = NULL)
    {
        $items = self::fetchByResidentId($id, $ledger_id);
        $balance = self::getBalanceForItems($items);
        
        return (float)$balance;
    }
    
    public static function getBalanceForItems($items)
    {
        $running_total = 0.0;
        foreach($items as $item)
        {
            $running_total += $item->split_amount;
        }
        return round($running_total, 2);
    }
    
    // Update running balance for all items.
    public static function updateRunningBalance($resident_id, $ledger_id)
    {
        $splits = Doctrine_query::create()
            ->from(__CLASS__.' s')
            ->addWhere('s.resident_id = ?', $resident_id)
            ->addWhere('s.ledger_id = ?', (int)$ledger_id)
            ->orderBy('s.posted ASC')
            ->execute();
        
        $running_balance = 0;
        foreach($splits as $split)
        {
            $running_balance += $split->split_amount;
            
            $split->running_balance = $running_balance;
            $split->save();
        }
    }
}