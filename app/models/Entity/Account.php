<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Account
 *
 * @Table()
 * @Entity
 */
class Account extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="description", type="string", length=256, nullable=true) */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}