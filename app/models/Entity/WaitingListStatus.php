<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * WaitingListStatus
 *
 * @Table(name="waiting_list_status")
 * @Entity
 */
class WaitingListStatus extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=1, nullable=true) */
    protected $name;

    /** @Column(name="description", type="string", length=24, nullable=true) */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    public static function fetchSelect($add_blank = FALSE)
    {
        $select = array();

        if ($add_blank)
            $select[''] = 'Select...';

        $records = self::fetchArray('name');
        foreach($records as $record)
        {
            $select[$record['id']] = ucwords(strtolower($record['description'])).' ('.$record['name'].')';
        }

        return $select;
    }
}