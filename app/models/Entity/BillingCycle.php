<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * BillingCycle
 *
 * @Table(name="billing_cycle")
 * @Entity
 */
class BillingCycle extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="date", type="integer", nullable=true) */
    protected $date;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}