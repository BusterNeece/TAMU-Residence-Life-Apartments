<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceCategory
 *
 * @Table(name="maintenance_category")
 * @Entity
 */
class MaintenanceCategory extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}