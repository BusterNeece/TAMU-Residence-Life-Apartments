<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Quiz
 *
 * @Table()
 * @Entity
 */
class Quiz extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->questions = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /** @OneToMany(targetEntity="QuizQuestion", mappedBy="quiz") */
    protected $questions;
    
}