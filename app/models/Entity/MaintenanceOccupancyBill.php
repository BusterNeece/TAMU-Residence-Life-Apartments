<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceOccupancyBill
 *
 * @Table(name="maintenance_occupancy_bill")
 * @Entity
 */
class MaintenanceOccupancyBill extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="occupancy_id", type="integer", nullable=true) */
    protected $occupancy_id;

    /** @Column(name="billable_item_id", type="integer", nullable=true) */
    protected $billable_item_id;

    /** @Column(name="billable_amount", type="decimal", precision=18, scale=2, nullable=true) */
    protected $billable_amount;

    /** @Column(name="item_comment", type="text", nullable=true) */
    protected $item_comment;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="workorder_id", type="integer", nullable=true) */
    protected $workorder_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Occupancy")
     * @JoinColumn(name="occupancy_id", referencedColumnName="id")
     */
    protected $occupancy;

    /**
     * @ManyToOne(targetEntity="MaintenanceBillableItem")
     * @JoinColumn(name="billable_item_id", referencedColumnName="id")
     */
    protected $billable_item;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="MaintenanceWorkorder")
     * @JoinColumn(name="workorder_id", referencedColumnName="id")
     */
    protected $workorder;
}