<?php
namespace Entity;

use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use \DF\Service\Geography;

/**
 * Resident
 *
 * @Table()
 * @Entity
 */
class Resident extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->occupancies = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="first_name", type="string", length=64, nullable=true) */
    protected $first_name;

    /** @Column(name="middle_name", type="string", length=64, nullable=true) */
    protected $middle_name;

    /** @Column(name="surname", type="string", length=64, nullable=true) */
    protected $surname;

    /** @Column(name="uin", type="string", length=16, nullable=true) */
    protected $uin;

    /** @Column(name="resident_type_id", type="integer", nullable=true) */
    protected $resident_type_id;

    /** @Column(name="class", type="string", length=1, nullable=true) */
    protected $class;

    /** @Column(name="primary_phone", type="string", length=32, nullable=true) */
    protected $primary_phone;

    /** @Column(name="secondary_phone", type="string", length=32, nullable=true) */
    protected $secondary_phone;

    /** @Column(name="email", type="string", length=320, nullable=true) */
    protected $email;

    /** @Column(name="birth_country", type="integer", nullable=true) */
    protected $birth_country;

    /** @Column(name="mailing_address", type="string", length=64, nullable=true) */
    protected $mailing_address;

    /** @Column(name="mailing_address2", type="string", length=64, nullable=true) */
    protected $mailing_address2;

    /** @Column(name="mailing_city", type="string", length=32, nullable=true) */
    protected $mailing_city;

    /** @Column(name="mailing_state", type="string", length=32, nullable=true) */
    protected $mailing_state;

    /** @Column(name="mailing_country", type="integer", nullable=true) */
    protected $mailing_country;

    /** @Column(name="mailing_zip", type="string", length=16, nullable=true) */
    protected $mailing_zip;

    /** @Column(name="marital_status", type="string", length=1, nullable=true) */
    protected $marital_status;

    /** @Column(name="gender", type="string", length=1, nullable=true) */
    protected $gender;

    /** @Column(name="suffix", type="string", length=8, nullable=true) */
    protected $suffix;

    /** @Column(name="forwarding_address", type="string", length=32, nullable=true) */
    protected $forwarding_address;

    /** @Column(name="forwarding_address2", type="string", length=32, nullable=true) */
    protected $forwarding_address2;

    /** @Column(name="forwarding_city", type="string", length=32, nullable=true) */
    protected $forwarding_city;

    /** @Column(name="forwarding_state", type="string", length=16, nullable=true) */
    protected $forwarding_state;

    /** @Column(name="forwarding_country", type="integer", nullable=true) */
    protected $forwarding_country;

    /** @Column(name="forwarding_zip", type="string", length=16, nullable=true) */
    protected $forwarding_zip;

    /** @Column(name="number_of_children", type="integer", nullable=true) */
    protected $number_of_children;

    /** @Column(name="principal_resident", type="integer", nullable=true) */
    protected $principal_resident;

    /** @Column(name="college_id", type="integer", nullable=true) */
    protected $college_id;

    /** @Column(name="major_id", type="integer", nullable=true) */
    protected $major_id;

    /** @Column(name="meningitis_vaccination", type="string", length=1, nullable=true) */
    protected $meningitis_vaccination;

    /** @Column(name="meningitis_vaccination_proof", type="integer", nullable=true) */
    protected $meningitis_vaccination_proof;

    /** @Column(name="meningitis_vaccination_action", type="integer", nullable=true) */
    protected $meningitis_vaccination_action;

    /** @Column(name="meningitis_vacc_proof", type="integer", nullable=true) */
    protected $meningitis_vacc_proof;

    /** @Column(name="meningitis_vacc_action", type="integer", nullable=true) */
    protected $meningitis_vacc_action;

    /** @Column(name="uin_sub_id", type="string", length=4, nullable=true) */
    protected $uin_sub_id;

    /** @Column(name="secondary_uin", type="string", length=16, nullable=true) */
    protected $secondary_uin;

    /** @Column(name="permanent_address", type="string", length=32, nullable=true) */
    protected $permanent_address;

    /** @Column(name="permanent_address2", type="string", length=32, nullable=true) */
    protected $permanent_address2;

    /** @Column(name="permanent_city", type="string", length=32, nullable=true) */
    protected $permanent_city;

    /** @Column(name="permanent_state", type="string", length=16, nullable=true) */
    protected $permanent_state;

    /** @Column(name="permanent_country", type="integer", nullable=true) */
    protected $permanent_country;

    /** @Column(name="permanent_zip", type="string", length=16, nullable=true) */
    protected $permanent_zip;

    /** @Column(name="voip_phone", type="string", length=15, nullable=true) */
    protected $voip_phone;

    /** @Column(name="preferred_email", type="string", length=320, nullable=true) */
    protected $preferred_email;

    /** @Column(name="forwarding_comments", type="string", length=256, nullable=true) */
    protected $forwarding_comments;

    /** @Column(name="i_20", type="integer", nullable=true) */
    protected $i_20;

    /** @Column(name="marriage_certificate", type="integer", nullable=true) */
    protected $marriage_certificate;

    /** @Column(name="birth_certificate", type="integer", nullable=true) */
    protected $birth_certificate;

    /** @Column(name="laundry_card", type="string", length=32, nullable=true) */
    protected $laundry_card;

    /** @Column(name="active", type="boolean", nullable=true) */
    protected $active;

    /** @Column(name="classification", type="string", length=20, nullable=true) */
    protected $classification;

    /** @Column(name="orientation_date", type="integer", nullable=true) */
    protected $orientation_date;

    /** @Column(name="notes", type="text", nullable=true) */
    protected $notes;

    /** @Column(name="emergency_relationship", type="string", length=32, nullable=true) */
    protected $emergency_relationship;

    /** @Column(name="emergency_firstname", type="string", length=64, nullable=true) */
    protected $emergency_firstname;

    /** @Column(name="emergency_middlename", type="string", length=64, nullable=true) */
    protected $emergency_middlename;

    /** @Column(name="emergency_lastname", type="string", length=64, nullable=true) */
    protected $emergency_lastname;

    /** @Column(name="emergency_address", type="string", length=32, nullable=true) */
    protected $emergency_address;

    /** @Column(name="emergency_address2", type="string", length=32, nullable=true) */
    protected $emergency_address2;

    /** @Column(name="emergency_city", type="string", length=32, nullable=true) */
    protected $emergency_city;

    /** @Column(name="emergency_state", type="string", length=16, nullable=true) */
    protected $emergency_state;

    /** @Column(name="emergency_country", type="integer", nullable=true) */
    protected $emergency_country;

    /** @Column(name="emergency_zip", type="string", length=16, nullable=true) */
    protected $emergency_zip;

    /** @Column(name="emergency_missing_person", type="boolean", nullable=true) */
    protected $emergency_missing_person;

    /** @Column(name="emergency_phone", type="string", length=32, nullable=true) */
    protected $emergency_phone;

    /** @Column(name="emergency2_relationship", type="string", length=32, nullable=true) */
    protected $emergency2_relationship;

    /** @Column(name="emergency2_firstname", type="string", length=64, nullable=true) */
    protected $emergency2_firstname;

    /** @Column(name="emergency2_middlename", type="string", length=64, nullable=true) */
    protected $emergency2_middlename;

    /** @Column(name="emergency2_lastname", type="string", length=64, nullable=true) */
    protected $emergency2_lastname;

    /** @Column(name="emergency2_address", type="string", length=32, nullable=true) */
    protected $emergency2_address;

    /** @Column(name="emergency2_address2", type="string", length=32, nullable=true) */
    protected $emergency2_address2;

    /** @Column(name="emergency2_city", type="string", length=32, nullable=true) */
    protected $emergency2_city;

    /** @Column(name="emergency2_state", type="string", length=16, nullable=true) */
    protected $emergency2_state;

    /** @Column(name="emergency2_country", type="integer", nullable=true) */
    protected $emergency2_country;

    /** @Column(name="emergency2_zip", type="string", length=16, nullable=true) */
    protected $emergency2_zip;

    /** @Column(name="emergency2_missing_person", type="boolean", nullable=true) */
    protected $emergency2_missing_person;

    /** @Column(name="emergency2_phone", type="string", length=32, nullable=true) */
    protected $emergency2_phone;

    /** @Column(name="block_account", type="integer", nullable=true) */
    protected $block_account;

    /** @Column(name="birth_date", type="integer", nullable=true) */
    protected $birth_date;

    /** @Column(name="international_student", type="boolean", nullable=true) */
    protected $international_student;

    /** @Column(name="ethnicity", type="string", length=5, nullable=true) */
    protected $ethnicity;

    /** @Column(name="exempt_from_late_fees", type="boolean", nullable=true) */
    protected $exempt_from_late_fees;

    /** @Column(name="is_external", type="boolean", nullable=true) */
    protected $is_external;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="resident")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="ResidentType")
     * @JoinColumn(name="resident_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="College")
     * @JoinColumn(name="college_id", referencedColumnName="id")
     */
    protected $college;

    /**
     * @ManyToOne(targetEntity="Major")
     * @JoinColumn(name="major_id", referencedColumnName="id")
     */
    protected $major;

    /** @OneToMany(targetEntity="Occupancy", mappedBy="resident") */
    protected $occupancies;

    public function preSave($event)
    {
        if (!$this->user_id)
        {
            if (trim($this->uin_sub_id) != '')
                $user_uin = $this->uin . '-' . $this->uin_sub_id;
            else
                $user_uin = $this->uin;
            
            $this->User = User::getOrCreate($user_uin, FALSE);
        }
    }

    public function getNameLastFirst()
    {
        $nameparts = array(
            implode(' ',array_filter(array(trim($this->surname), trim($this->suffix)))),
            implode(' ',array_filter(array(trim($this->first_name), trim($this->middle_name))))
        );
        
        $filtered = array_filter($nameparts);
        if (count($filtered) > 0)
            return implode(', ', $filtered);
        else
            return false;
    }

    public function getNameFirstLast()
    {
        $nameparts = array($this->first_name, $this->middle_name, $this->surname, $this->suffix);
        
        $filtered = array_filter($nameparts);
        if (count($filtered) > 0)
            return implode(' ', $filtered);
        else
            return false;
    }

    public function getAddress($type = null)
    {
        switch($type)
        {
            case 'permanent':
                $address = array(
                    'address' => $this->permanent_address,
                    'address2' => $this->permanent_address2,
                    'city' => $this->permanent_city,
                    'state' => $this->permanent_state,
                    'zip' => $this->permanent_zip,
                    'country' => ($this->permanent_country > 0 ? Geography::getCountryName($this->permanent_country) : null),
                );
            break;
            
            case 'forwarding':
                $address = array(
                    'address' => $this->forwarding_address,
                    'address2' => $this->forwarding_address2,
                    'city' => $this->forwarding_city,
                    'state' => $this->forwarding_state,
                    'zip' => $this->forwarding_zip,
                    'country' => ($this->forwarding_country > 0 ? Geography::getCountryName($this->forwarding_country) : null),
                    'comments' => $this->forwarding_comments,
                );
            break;
            
            case 'emergency':
                $address = array(
                    'address' => $this->emergency_address,
                    'address2' => $this->emergency_address2,
                    'city' => $this->emergency_city,
                    'state' => $this->emergency_state,
                    'zip' => $this->emergency_zip,
                    'country' => ($this->emergency_country > 0 ? Geography::getCountryName($this->emergency_country) : null),
                );
            break;
            
            case 'emergency2':
                $address = array(
                    'address' => $this->emergency2_address,
                    'address2' => $this->emergency2_address2,
                    'city' => $this->emergency2_city,
                    'state' => $this->emergency2_state,
                    'zip' => $this->emergency2_zip,
                    'country' => ($this->emergency2_country > 0 ? Geography::getCountryName($this->emergency2_country) : null),
                );
            break;
            
            case 'mailing':
            default:
                $address = array(
                    'address' => $this->mailing_address,
                    'address2' => $this->mailing_address2,
                    'city' => $this->mailing_city,
                    'state' => $this->mailing_state,
                    'zip' => $this->mailing_zip,
                    'country' => ($this->mailing_country > 0 ? Geography::getCountryName($this->mailing_country) : null),
                );
            break;
        }
        
        return $address;
    }

    public function hasNotes()
    {
        if (trim($this->notes) != '')
            return true;
        else
            return false;
    }

    public function isActive()
    {
        $occupancies = Occupancy::getActiveByResidentId($this->id);
        return (count($occupancies) > 0);
    }
    
    public function isExternal()
    {
        $occupancies = Occupancy::getActiveByResidentId($this->id, true);
        
        foreach((array)$occupancies as $o)
        {
            if ($o->isExternal())
                return true;
        }
        
        return false;
    }

    public function getRoommateRequests()
    {
        $roommates = array();
        $occupancies = Occupancy::getActiveByResidentId($this->id);
        
        foreach($occupancies as $occupancy)
        {
            $asset = $occupancy->asset;
            
            foreach(RoommateRequest::fetchByAssetId($asset->id) as $request)
            {
                if ((!$request->isApproved()) && ($request->getApprovals((string)$this->id) === 'pending'))
                {
                    $roommates[] = $request;
                }
            }
        }
        
        return $roommates;
    }
    
    public function getFamilyMembers()
    {
        $em = self::getEntityManager();
        return $em->createQuery('SELECT r FROM '.__CLASS__.' r WHERE r.uin = :uin AND (r.principal_resident IS NULL or r.principal_resident = 0) ORDER BY r.uin_sub_id ASC')
            ->setParameter('uin', $this->uin)
            ->execute();
    }

    public function generateSubId()
    {
        $family = $this->getFamilyMembers();
        $max = 0;
        if ($family)
        {
            foreach($family as $f)
            {
                if ((is_numeric($f->uin_sub_id)) && (intval($f->uin_sub_id) > $max))
                    $max = intval($f->uin_sub_id);
            }
        }
        $max++;
        $sub_id = str_pad($max, 4, '0', STR_PAD_LEFT);
        return $sub_id;
    }

    public function getPendingApplications()
    {
        $em = self::getEntityManager();
        return $em->createQuery('SELECT a FROM Entity\Application a WHERE a.resident_id = :id AND a.application_status_id = :status')
            ->setParameter('id', $this->id)
            ->setParameter('status', Application::STATUS_WAITING)
            ->getArrayResult();
    }

    /**
     * Static Functions
     */

    public static function getFromCurrentUser()
    {
        $auth = \Zend_Registry::get('auth');
        if ($auth->isLoggedIn())
        {
            $user = $auth->getLoggedInUser();

            if (count($user->residents) > 0)
                return $user->residents[0];
            else
                return NULL;
        }
        else
        {
            return NULL;
        }
    }

    public static function fetchSelect($add_blank = FALSE)
    {
        $em = self::getEntityManager();
        $all_records_raw = $em->createQuery('SELECT r FROM '.__CLASS__.' r WHERE r.principal_resident = 1 ORDER BY r.surname ASC, r.first_name ASC')->getArrayResult();
        $all_records = array();
        
        if ($add_blank)
            $all_records['null'] = 'N/A';
        
        foreach($all_records_raw as $record)
        {
            if (($record['surname']) && ($record['first_name']))
                $all_records[$record['id']] = $record['surname'].', '.$record['first_name'];
        }
        return $all_records;
    }

    public static function fetchBlocked($filters = null)
    {
        $em = self::getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('r')
            ->from(__CLASS__, 'r')
            ->where('r.principal_resident = 1')
            ->andWhere('r.block_account > 0')
            ->orderBy('r.surname', 'ASC');

        if ($filters)
        {
            if (!empty($filters['start']))
                $query->andWhere('block_account >= '.$filters['start']);
            if (!empty($filters['end']))
                $query->addWhere('block_account <= '.$filters['end']);
        }

        return $query->getQuery()->getArrayResult();
    }
}