<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * WaitingList
 *
 * @Table(name="waiting_list")
 * @Entity
 */
class WaitingList extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="choice_asset_type_id", type="integer", nullable=true) */
    protected $choice_asset_type_id;

    /** @Column(name="application_id", type="integer", nullable=true) */
    protected $application_id;

    /** @Column(name="choice_rank", type="integer", nullable=true) */
    protected $choice_rank;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Application", inversedBy="waiting_list")
     * @JoinColumn(name="application_id", referencedColumnName="id")
     */
    protected $application;

    /**
     * @ManyToOne(targetEntity="AssetType")
     * @JoinColumn(name="choice_asset_type_id", referencedColumnName="id")
     */
    protected $asset_type;
}