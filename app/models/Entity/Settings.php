<?php
namespace Entity;

use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="settings")
 * @Entity
 */
class Settings extends \DF\Doctrine\Entity
{
    /**
     * @var string $setting_key
     *
     * @Column(name="setting_key", type="string", length=64, nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    protected $setting_key;

    /** @Column(name="setting_value", type="text", nullable=true, nullable=true) */
    protected $setting_value;
    
    /**
     * Static Functions
     */
    
    public static function setSetting($key, $value)
    {
        $record = self::getRepository()->findOneBy(array('setting_key' => $key));
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->setSettingKey($key);
        }
        
        $record->setSettingValue($value);
        $record->save();
    }
    
    public static function getSetting($key, $default_value = NULL)
    {
        $record = self::getRepository()->findOneBy(array('setting_key' => $key));
        
        if ($record instanceof self)
            return $record->getSettingValue();
        else
            return $default_value;
    }
    
    public static function fetchAll()
    {
        $all_records_raw = self::getRepository()->findAll();
        
        $all_records = array();
        foreach($all_records_raw as $record)
        {
            $all_records[$record['setting_key']] = $record['setting_value'];
        }
        return $all_records;
    }
    
    public static function fetchArray()
    {
        $em = \Zend_Registry::get('em');
        $settings_raw = $em->createQuery('SELECT s FROM '.__CLASS__.' s ORDER BY s.setting_key ASC')
            ->getArrayResult();
        
        $settings = array();
        foreach((array)$settings_raw as $setting)
        {
            $settings[$setting['setting_key']] = $setting['setting_value'];
        }
        return $settings;
    }
}