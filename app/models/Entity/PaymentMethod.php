<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * PaymentMethod
 *
 * @Table(name="payment_method")
 * @Entity
 */
class PaymentMethod extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=32, nullable=true)
     */
    protected $name;

    /**
     * @var string $description
     *
     * @Column(name="description", type="string", length=256, nullable=true)
     */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}