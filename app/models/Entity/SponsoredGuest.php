<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * SponsoredGuest
 *
 * @Table(name="sponsored_guest")
 * @Entity
 */
class SponsoredGuest extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $resident_id
     *
     * @Column(name="resident_id", type="integer", nullable=true)
     */
    protected $resident_id;

    /**
     * @var integer $department_id
     *
     * @Column(name="department_id", type="integer", nullable=true)
     */
    protected $department_id;

    /**
     * @var integer $sponsor_id
     *
     * @Column(name="sponsor_id", type="integer", nullable=true)
     */
    protected $sponsor_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Department")
     * @JoinColumn(name="department_id", referencedColumnName="id")
     */
    protected $department;

    /**
     * @ManyToOne(targetEntity="Sponsor")
     * @JoinColumn(name="sponsor_id", referencedColumnName="id")
     */
    protected $sponsor;
}