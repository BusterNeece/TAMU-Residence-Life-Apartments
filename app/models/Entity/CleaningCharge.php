<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * CleaningCharge
 *
 * @Table(name="cleaning_charge")
 * @Entity
 */
class CleaningCharge extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="worker", type="string", length=255, nullable=true) */
    protected $worker;

    /** @Column(name="time_assigned", type="integer", nullable=true) */
    protected $time_assigned;

    /** @Column(name="time_completed", type="integer", nullable=true) */
    protected $time_completed;

    /** @Column(name="status", type="boolean", nullable=true) */
    protected $status;

    /** @Column(name="charge_data", type="text", nullable=true) */
    protected $charge_data;

    /** @Column(name="cost", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost;

    /** @Column(name="notes", type="text", nullable=true) */
    protected $notes;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;
}