<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * OfferAssetStatus
 *
 * @Table(name="offer_asset_status")
 * @Entity
 */
class OfferAssetStatus extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $text
     *
     * @Column(name="text", type="string", length=64, nullable=true)
     */
    protected $text;

    /**
     * @var string $description
     *
     * @Column(name="description", type="string", length=128, nullable=true)
     */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}