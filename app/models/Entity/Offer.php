<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="offers")
 * @Entity
 */
class Offer extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="asset_type_id", type="integer", nullable=true) */
    protected $asset_type_id;

    /** @Column(name="move_out_date", type="integer", nullable=true) */
    protected $move_out_date;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="offer_date", type="integer", nullable=true) */
    protected $offer_date;

    /** @Column(name="response_deadline", type="integer", nullable=true) */
    protected $response_deadline;

    /** @Column(name="offer_asset_status_id", type="integer", nullable=true) */
    protected $offer_asset_status_id;

    /** @Column(name="move_in_date", type="integer", nullable=true) */
    protected $move_in_date;

    /** @Column(name="application_id", type="integer", nullable=true) */
    protected $application_id;

    /** @Column(name="accepted_date", type="integer", nullable=true) */
    protected $accepted_date;

    /** @Column(name="contract_data", type="json", nullable=true) */
    protected $contract_data;

    /** @Column(name="contract_initials", type="string", length=4, nullable=true) */
    protected $contract_initials;

    /** @Column(name="contract_initials_date", type="integer", nullable=true) */
    protected $contract_initials_date;

    /** @Column(name="lease_version_id", type="integer", nullable=true) */
    protected $lease_version_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     * })
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="AssetType")
     * @JoinColumn(name="asset_type_id", referencedColumnName="id")
     */
    protected $asset_type;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="OfferAssetStatus")
     * @JoinColumn(name="offer_asset_status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ManyToOne(targetEntity="Application")
     * @JoinColumn(name="application_id", referencedColumnName="id")
     */
    protected $application;

    /**
     * @ManyToOne(targetEntity="LeaseVersion")
     * @JoinColumn(name="lease_version_id", referencedColumnName="id")
     */
    protected $lease_version;

    /**
     * Static Functions
     */
    
    public static function fetchMostRecentByResidentId($id)
    {
        $em = self::getEntityManager();
        return $em->createQuery('SELECT o FROM '.__CLASS__.' o WHERE o.resident_id = :id ORDER BY o.id DESC')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public static function fetchById($id)
    {
        return self::find($id);
    }

    public static function fetchByApplicationId($id)
    {
        return self::getRepository()->findOneBy(array('application_id' => $id));
    }
}