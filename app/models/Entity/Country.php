<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Country
 *
 * @Table()
 * @Entity
 */
class Country extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $code
     *
     * @Column(name="code", type="string", length=10, nullable=true)
     */
    protected $code;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=64, nullable=true)
     */
    protected $name;

    /**
     * @var decimal $weight
     *
     * @Column(name="weight", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $weight;


}