<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * AssetKey
 *
 * @Table(name="asset_key")
 * @Entity
 */
class AssetKey extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="key_type_id", type="integer", nullable=true) */
    protected $key_type_id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="AssetKeyType")
     * @JoinColumn(name="key_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;
}