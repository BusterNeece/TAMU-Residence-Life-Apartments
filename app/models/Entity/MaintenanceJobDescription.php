<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceJobDescription
 *
 * @Table(name="maintenance_job_description")
 * @Entity
 */
class MaintenanceJobDescription extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=128, nullable=true) */
    protected $name;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}