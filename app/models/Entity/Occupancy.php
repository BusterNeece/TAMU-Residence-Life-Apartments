<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Occupancy
 *
 * @Table()
 * @Entity
 */
class Occupancy extends \DF\Doctrine\Entity
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public function __construct()
    {
        $this->moveout_appointments = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="start_date", type="integer", nullable=true) */
    protected $start_date;

    /** @Column(name="end_date", type="integer", nullable=true) */
    protected $end_date;

    /** @Column(name="checkin_date", type="integer", nullable=true) */
    protected $checkin_date;

    /** @Column(name="checkout_date", type="integer", nullable=true) */
    protected $checkout_date;

    /** @Column(name="occupancy_model_id", type="integer", nullable=true) */
    protected $occupancy_model_id;

    /** @Column(name="department_id", type="integer", nullable=true) */
    protected $department_id;

    /** @Column(name="sponsor_id", type="integer", nullable=true) */
    protected $sponsor_id;

    /** @Column(name="finalbalance", type="decimal", precision=18, scale=2, nullable=true) */
    protected $finalbalance;

    /** @Column(name="finalcharges", type="decimal", precision=18, scale=2, nullable=true) */
    protected $finalcharges;

    /** @Column(name="finalrent", type="decimal", precision=18, scale=2, nullable=true) */
    protected $finalrent;

    /** @Column(name="finalelectric", type="decimal", precision=18, scale=2, nullable=true) */
    protected $finalelectric;

    /** @Column(name="checkoutappointment", type="integer", nullable=true) */
    protected $checkoutappointment;

    /** @Column(name="moveinappointment", type="integer", nullable=true) */
    protected $moveinappointment;

    /** @Column(name="moveindatedesired", type="integer", nullable=true) */
    protected $moveindatedesired;

    /** @Column(name="moveoutdate", type="integer", nullable=true) */
    protected $moveoutdate;

    /** @Column(name="orientation", type="integer", nullable=true) */
    protected $orientation;

    /** @Column(name="occupancy", type="string", length=256, nullable=true) */
    protected $occupancy;

    /** @Column(name="moveout_reason", type="string", length=256, nullable=true) */
    protected $moveout_reason;

    /** @Column(name="deposit", type="decimal", precision=18, scale=2, nullable=true) */
    protected $deposit;

    /** @Column(name="deposit_date", type="integer", nullable=true) */
    protected $deposit_date;

    /** @Column(name="deposit_reference_number", type="string", length=256, nullable=true) */
    protected $deposit_reference_number;

    /** @Column(name="lease_signor", type="string", length=1, nullable=true) */
    protected $lease_signor;

    /** @Column(name="initial_meter_reading", type="decimal", precision=18, scale=2, nullable=true) */
    protected $initial_meter_reading;

    /** @Column(name="legacy_current", type="string", length=1, nullable=true) */
    protected $legacy_current;

    /** @Column(name="application_id", type="integer", nullable=true) */
    protected $application_id;

    /** @Column(name="moveout_form_received", type="integer", nullable=true) */
    protected $moveout_form_received;

    /** @Column(name="skip_walkthrough", type="boolean", nullable=true) */
    protected $skip_walkthrough;

    /** @Column(name="is_external", type="boolean", nullable=true) */
    protected $is_external;

    /** @Column(name="lease_initials", type="string", length=5, nullable=true) */
    protected $lease_initials;

    /** @Column(name="lease_initial_timestamp", type="integer", nullable=true) */
    protected $lease_initial_timestamp;

    /** @Column(name="cost_override", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost_override;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="OccupancyModel")
     * @JoinColumn(name="occupancy_model_id", referencedColumnName="id")
     */
    protected $occupancy_model;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident; 

    /** @OneToMany(targetEntity="MoveoutAppointment", mappedBy="occupancy") */
    protected $moveout_appointments;

    public function getStatus()
    {
        if ($this->start_date != 0 && $this->end_date != 0)
        {
            $start_date = $this->start_date;
            $end_date = $this->end_date;
            
            if (time() >= $start_date && time() <= $end_date)
                return self::STATUS_ACTIVE;
            else
                return self::STATUS_INACTIVE;
        }
        else if ($this->start_date != 0 && $this->end_date == 0)
        {
            if (time() >= $this->start_date)
                return self::STATUS_ACTIVE;
            else
                return self::STATUS_INACTIVE;
        }
        else
        {
            return self::STATUS_ACTIVE;
        }
    }
    
    public function getStatusText()
    {
        $status_codes = self::getStatusCodes();
        $status = $this->getStatus();
        
        return $status_codes[$status];
    }
    
    public function isActive()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE);
    }
        
    public function isExternal()
    {   
        return ((int)$this->is_external != 0);
    }

    public function isVisible()
    {
        $start_date = ((int)$this->checkin_date) ? (int)$this->checkin_date : (int)$this->start_date;
        $end_date = ((int)$this->checkout_date) ? (int)$this->checkout_date : (int)$this->end_date;

        $threshold = (86400 *6); // 6 days.

        if ($start_date != 0 && $start_date-$threshold <= time() && ($end_date == 0 || $end_date+$threshold >= time()))
            return TRUE;
        else
            return FALSE;
    }

    public function canRenew()
    {
        $renewal_default = (DF_APPLICATION_ENV == "production") ? 0 : 1;
        $enable_renewal = Settings::getSetting('enable_renewal', $renewal_default);
        if (!$enable_renewal)
            return false;

        // Temporarily limit renewal only to GP1 apartments.
        if ($this->asset->location->address != "302 Ball St.")
            return false;

        return $this->isEndingSoon(TRUE);
    }

    public function isEndingSoon($include_no_end = FALSE)
    {
        $end_date = max((int)$this->checkout_date, (int)$this->end_date);
        $threshold = (86400*120);

        if ($end_date == 0)
            return $include_no_end;
        elseif ($end_date <= time())
            return false;
        else
            return ($end_date-$threshold <= time());
    }

    public function getCost($timestamp = NULL)
    {
        return array(
            'model'     => $this->asset->getOccupancyModel(),
            'cost'      => $this->_getCost($timestamp),
        );
    }

    protected function _getCost($timestamp = NULL)
    {
        if ($this->cost_override != 0)
            return (float)$this->cost_override;
        else
            return $this->asset->getCost($timestamp);
    }

    /**
     * Static Functions
     */

    public static function getStatusCodes()
    {
        return array(
            self::STATUS_INACTIVE   => 'Inactive',
            self::STATUS_ACTIVE     => 'Active',
        );
    }
    
    public static function fetchById($id)
    {
        return self::find($id);
    }

    public static function fetchByApplicationId($id)
    {
        return self::getRepository()->findOneBy(array('application_id' => $id));
    }
    
    // Iterate through all occupancies associated with a resident and return the first active record.
    public static function getActiveByResidentId($id)
    {
        $em = self::getEntityManager();
        $all_occupancies = $em->createQuery('SELECT o FROM '.__CLASS__.' o WHERE o.resident_id = :id ORDER BY o.id DESC')
            ->setParameter('id', (int)$id)
            ->execute();

        $active = array();

        if (count($all_occupancies) > 0)
        {
            foreach($all_occupancies as $occupancy)
            {
                if ($occupancy->isVisible())
                    $active[] = $occupancy;
            }
        }

        return $active;
    }

    // Get the active residents in an asset by a timestamp.
    public static function getActiveByAsset($asset_id, $start_timestamp = NULL, $end_timestamp = NULL)
    {
        $em = self::getEntityManager();
        $occupancies_in_range = $em->createQuery('SELECT o, r FROM '.__CLASS__.' o INNER JOIN o.resident r WHERE o.asset_id = :id ORDER BY o.id DESC')
            ->setParameter('id', (int)$asset_id)
            ->getArrayResult();
        
        if (count($occupancies_in_range) == 0)
            return array();
        
        if ($start_timestamp && !$end_timestamp)
            $end_timestamp = $start_timestamp;
        else if (!$start_timestamp)
            return (array)$occupancies_in_range;
        
        $occupancies = array();
        foreach($occupancies_in_range as $occupancy)
        {
            $start_date = ((int)$occupancy['checkin_date']) ? (int)$occupancy['checkin_date'] : (int)$occupancy['start_date'];
            $end_date = ((int)$occupancy['checkout_date']) ? (int)$occupancy['checkout_date'] : (int)$occupancy['end_date'];
            
            if ($start_date != 0 && $start_date <= $end_timestamp && ($end_date == 0 || $end_date >= $start_timestamp))
            {
                $occupancies[] = $occupancy;
            }
        }

        return $occupancies;
    }
    
    public static function getFromCurrentUser()
    {
        $resident = Resident::getFromCurrentUser();
        if ($resident instanceof Resident)
            return self::getActiveByResidentId($resident->id);
        else
            return NULL;
    }

    public static function fetchMoveOuts($filters = null)
    {
        $em = self::getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('o, a, r, ma')
            ->from(__CLASS__, 'o')
            ->leftJoin('o.asset', 'a')
            ->leftJoin('o.resident', 'r')
            ->leftJoin('o.moveout_appointments', 'ma')
            ->where('o.end_date > 0')
            ->orderBy('o.end_date', 'ASC');
        
        if ($filters)
        {
            if (!empty($filters['start']))
                $query->andWhere('o.end_date >= '.$filters['start']);
            if (!empty($filters['end']))
                $query->andWhere('o.end_date <= '.$filters['end']);
            if (!empty($filters['searchterms']))
                $query->andWhere('r.uin LIKE :query OR CONCAT(r.first_name, CONCAT(\' \', r.surname)) LIKE :query')->setParameter('query', '%'.$filters['searchterms'].'%');
        }

        return $query->getQuery()->getArrayResult();
    }
    
    public static function fetchMoveIns($filters = null)
    {
        $em = self::getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('o, a, r, ma')
            ->from(__CLASS__, 'o')
            ->leftJoin('o.asset', 'a')
            ->leftJoin('o.resident', 'r')
            ->leftJoin('o.moveout_appointments', 'ma')
            ->where('ma.date > 0')
            ->addWhere('ma.movein = 1')
            ->orderBy('ma.date', 'ASC')
            ->addOrderBy('ma.slot', 'ASC');
        
        if ($filters)
        {
            if (!empty($filters['start']))
                $query->andWhere('ma.date >= '.$filters['start']);
            if (!empty($filters['end']))
                $query->andWhere('ma.date <= '.$filters['end']);
            if (!empty($filters['searchterms']))
                $query->andWhere('r.uin LIKE :query OR CONCAT(r.first_name, CONCAT(\' \', r.surname)) LIKE :query')->setParameter('query', '%'.$filters['searchterms'].'%');
        }

        return $query->getQuery()->getArrayResult();
    }
}