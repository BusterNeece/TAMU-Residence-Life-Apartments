<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Trust
 *
 * @Table()
 * @Entity
 */
class Trust extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $transaction_id
     *
     * @Column(name="transaction_id", type="integer", nullable=true)
     */
    protected $transaction_id;

    /**
     * @var integer $open_date
     *
     * @Column(name="open_date", type="integer", nullable=true)
     */
    protected $open_date;

    /**
     * @var integer $close_date
     *
     * @Column(name="close_date", type="integer", nullable=true)
     */
    protected $close_date;

    /**
     * @var decimal $amount
     *
     * @Column(name="amount", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $amount;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="RegisterTransaction")
     * @JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    protected $transaction;
}