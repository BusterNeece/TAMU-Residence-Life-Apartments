<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Block
 *
 * @Table()
 * @Entity
 */
class Block extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="content", type="text", nullable=true) */
    protected $content;

    public function getContent()
    {
        $block_name = $this->name;

        if ($block_name)
        {
            $template_path = self::getTemplatePath($block_name);

            if (file_exists($template_path))
                return file_get_contents($template_path);
            else
                return $this->content;
        }
        else
        {
            return NULL;
        }
    }
    public function setContent($html)
    {
        $block_name = $this->name;
        $template_path = self::getTemplatePath($block_name);

        @file_put_contents($template_path, $html);

        return $this;
    }

    /**
     * Static Functions
     */

    public static function fetchByName($name)
    {
        return self::getRepository()->findOneBy(array('name' => $name));
    }
    
    public static function render($block_name, $block_vars = array(), $show_title = FALSE)
    {
        if ($block_name instanceof self)
            $block = $block_name;
        else
            $block = self::fetchByName($block_name);
        
        if (!($block instanceof self))
            return NULL;
        
        $content = '';
        
        if ($block->title && $show_title)
            $content .= '<h2 class="title">'.$block->title.'</h2>';
        
        $block_vars = $block_vars + array(
            'request'       => $_REQUEST,
            'get'           => $_GET,
            'post'          => $_POST,
            'server'        => $_SERVER,
        );

        $content .= $block->content;
        $content = self::contentReplace($content, $block_vars);
        
        return $content;
    }
    
    public static function contentReplace($content, $vars, $base='')
    {
        foreach((array)$vars as $var_key => $var_value)
        {
            if (is_array($var_value))
            {
                $content = self::contentReplace($content, $var_value, $var_key.'.');
            }
            else if ($var_value instanceof \Zend_Config)
            {
                $var_value = $var_value->toArray();
                $content = self::contentReplace($content, $var_value, $var_key.'.');
            }
            else if (!is_object($var_value))
            {
                $replace_key = '#'.$base.$var_key.'#';
                $content = str_replace($replace_key, $var_value, $content);
            }
        }
        
        return $content;
    }

    public static function getTemplatePath($block_name)
    {
        return DF_UPLOAD_FOLDER.'/blocks/'.$block_name.'.phtml';
    }
}