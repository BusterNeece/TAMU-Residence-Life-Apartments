<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Item
 *
 * @Table()
 * @Entity
 */
class Item extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="account_id", type="integer", nullable=true) */
    protected $account_id;

    /** @Column(name="description", type="string", length=256, nullable=true) */
    protected $description;

    /** @Column(name="z_report_display_order", type="decimal", precision=18, scale=2, nullable=true) */
    protected $z_report_display_order;

    /** @Column(name="associate_with_asset", type="string", length=1, nullable=true) */
    protected $associate_with_asset;

    /** @Column(name="object_code", type="integer", nullable=true) */
    protected $object_code;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Account")
     * @JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;
}