<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * QuizOption
 *
 * @Table(name="quiz_option")
 * @Entity
 */
class QuizOption extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_question_id", type="integer") */
    protected $quiz_question_id;

    /** @Column(name="option_text", type="string", length=255, nullable=true) */
    protected $option_text;

    /**
     * @ManyToOne(targetEntity="QuizQuestion")
     * @JoinColumn(name="quiz_question_id", referencedColumnName="id")
     */
    protected $question;
}