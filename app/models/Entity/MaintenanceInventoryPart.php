<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceInventoryPart
 *
 * @Table(name="maintenance_inventory_part")
 * @Entity
 */
class MaintenanceInventoryPart extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=512, nullable=true) */
    protected $name;

    /** @Column(name="cost", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost;

    /** @Column(name="date_added", type="integer", nullable=true) */
    protected $date_added;

    /** @Column(name="part_number", type="string", length=64, nullable=true) */
    protected $part_number;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}