<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceWorkorder
 *
 * @Table(name="maintenance_workorder")
 * @Entity
 */
class MaintenanceWorkorder extends \DF\Doctrine\Entity
{
    const STATUS_UNPROCESSED = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_ASSIGNED = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_DEFERRED = 4;
    const STATUS_PARTSONORDER = 6;
    const STATUS_DELETED = 10;

    public function __construct()
    {
        $this->inventory_parts = new ArrayCollection;
        $this->workers = new ArrayCollection;
        $this->transactions = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="location", type="text", nullable=true) */
    protected $location;

    /** @Column(name="status", type="integer", nullable=true) */
    protected $status;

    /** @Column(name="deferred_to", type="string", length=200, nullable=true) */
    protected $deferred_to;

    /** @Column(name="origin", type="string", length=100, nullable=true) */
    protected $origin;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="notes", type="text", nullable=true) */
    protected $notes;

    /** @Column(name="submitter_name", type="string", length=128, nullable=true) */
    protected $submitter_name;

    /** @Column(name="submitter_phone_number", type="string", length=64, nullable=true) */
    protected $submitter_phone_number;

    /** @Column(name="submitter_email", type="string", length=128, nullable=true) */
    protected $submitter_email;

    /** @Column(name="time_reported", type="integer", nullable=true) */
    protected $time_reported;

    /** @Column(name="time_started", type="integer", nullable=true) */
    protected $time_started;

    /** @Column(name="time_done", type="integer", nullable=true) */
    protected $time_done;

    /** @Column(name="time_spent", type="decimal", precision=18, scale=2, nullable=true) */
    protected $time_spent;

    /** @Column(name="workorder_category_id", type="integer", nullable=true) */
    protected $workorder_category_id;

    /** @Column(name="comments", type="text", nullable=true) */
    protected $comments;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="MaintenanceCategory")
     * @JoinColumn(name="workorder_category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ManyToMany(targetEntity="MaintenanceInventoryPart")
     * @JoinTable(name="maintenance_workorder_parts",
     *      joinColumns={@JoinColumn(name="workorder_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="inventory_part_id", referencedColumnName="id")}
     * )
     */
    protected $inventory_parts;

    /**
     * @ManyToMany(targetEntity="MaintenanceWorker")
     * @JoinTable(name="maintenance_workorder_workers",
     *      joinColumns={@JoinColumn(name="workorder_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="worker_id", referencedColumnName="id")}
     * )
     */
    protected $workers;

    /** @OneToMany(targetEntity="RegisterTransaction", mappedBy="workorder") */
    protected $transactions;

    /**
     * Static Functions
     */

    public static function getStatusCodes()
    {
        return array(
            self::STATUS_UNPROCESSED    => 'Customer Request',
            self::STATUS_ACCEPTED       => 'Accepted',
            self::STATUS_ASSIGNED       => 'Assigned to Worker',
            self::STATUS_COMPLETED      => 'Completed',
            self::STATUS_DEFERRED       => 'Deferred',
            self::STATUS_PARTSONORDER   => 'Parts on Order',
            self::STATUS_DELETED        => 'Deleted',
        );
    }
    
    public static function getActiveStatusCodes()
    {
        return array(
            self::STATUS_ACCEPTED,
            self::STATUS_ASSIGNED,
        );
    }
    
    public function getStatusText()
    {
        $status_lookup = self::getStatusCodes();
        return $status_lookup[$this->status];
    }
    
    public function setStatus($new_status)
    {
        $current_status = $this->status;
        
        // Send e-mail notification in case of status change.
        if ($current_status != $new_status)
        {
            DF_Messenger::send(array(
                'to'        => $this->submitter_email,
                'subject'   => 'Maintenance Request Status Changed',
                'template'  => 'maintenance/status',
                'vars'      => array(
                    'record'    => $this,
                ),
            ));
        }
        
        // Handle special status changes.
        /* commented out by Ed: these times are manually recorded
        switch($new_status)
        {
            case self::STATUS_ASSIGNED:
                $this->time_started = time();
            break;
            
            case self::STATUS_COMPLETED:
                $this->time_done = time();
            break;
        }
        */
        
        $this->_set('status', $new_status);
        $this->save();
    }
    
    /**
     * Origin types
     */
    
    public static function getOriginTypes()
    {
        return array(
            'normal'        => 'Normal Request',
            'movein'        => 'Move-in Related',
            'moveout'       => 'Move-out Related',
            'damage'        => 'Damage by Resident',
        );
    }
    
    public function getOrigin()
    {
        $origin_types = self::getOriginTypes();
        return $origin_types[$this->origin];
    }
    
    /**
     * Basic single-record retrieval
     */
    
    public static function fetchById($id)
    {
        return Doctrine_Query::create()
            ->from(__CLASS__.' mw')
            ->addWhere('id = ?', (int) $id)
            ->fetchOne();
    }
    
    /**
     * Bulk record retrieval
     */
    
    public function fetchByAssetId($asset_id, $status = NULL)
    {
        $em = self::getEntityManager();
        $query = $em->createQueryBuilder()
            ->select('mw')
            ->from(__CLASS__, 'mw')
            ->where('mw.asset_id = :id')->setParameter('id', $asset_id)
            ->orderBy('mw.status', 'ASC');
        
        if (!is_null($status))
        {
            $status = (is_array($status)) ? $status : array($status);
            $query->andWhere('mw.status IN (:codes)')->setParameter('codes', $status);
        }

        return $query->getQuery()->execute();
    }

    public function fetchByUserId($user_id)
    {
        return self::getRepository()->findBy(array('user_id' => $user_id));
    }
    
    public static function fetchByStatus($status = NULL)
    {
        static $records_by_status;
        if( !isset($records_by_status[$status]) )
        {
            $records_by_status[$status] = Doctrine_Query::create()
                
                ->from(__CLASS__)
                ->addWhere('status = ?', $status)
                ->orderBy('id ASC')
                ->execute();
        }
        return $records_by_status[$status];
    }

    public static function fetchTotalsByStatus($status = NULL)
    {
        static $totals_by_status;

        if(!isset($totals_by_status))
        {
            $em = self::getEntityManager();
            $active_status_codes = self::getActiveStatusCodes();
            $totals_by_status = array();

            foreach(self::getStatusCodes() as $status_code => $status_text)
            {
                $total = $em->createQuery('SELECT COUNT(mw.id) FROM '.__CLASS__.' mw WHERE mw.status = :status')
                    ->setParameter('status', $status_code)
                    ->getSingleScalarResult();

                $totals_by_status[$status_code] = $total;
                if (in_array($status_code, $active_status_codes))
                    $totals_by_status['active'] += $total;
            }
        }
        
        if (!is_null($status))
            return $totals_by_status[$status];
        else
            return $totals_by_status;
    }

    public static function createFromTemplate($options)
    {
        /*
         * Temporary hard-coded Work Order template
         * TODO: build user-managed template system
         */

        $w_o = new MaintenanceWorkorder();

        $w_o->asset_id = $options['asset_id'];
        $w_o->location = $options['location'];
        $w_o->origin = $options['origin'];
        $w_o->workorder_category_id = $options['workorder_category_id'];
        $w_o->description = $options['description'];
        $w_o->submitter_name = $options['submitter_name'];
        $w_o->submitter_phone_number = $options['submitter_phone_number'];
        $w_o->submitter_email = $options['submitter_email'];
        $w_o->status = self::STATUS_UNPROCESSED;
        $w_o->time_reported = time();

        $tasks = array(
            '59' => 'Check air conditioning/ heating systems & refrigerator',
            '51' => 'Check/repair wall tile as needed',
            '60' => 'Check locks, windows, screens & all other carpentry items',
            '52' => 'Clean/prep all tile floors as needed',
            '61' => 'Check smoke & co2 alarm, lights, elect, telephone & cable',
            '62' => 'Paint interior of apartment as needed',
            '58' => 'Treat vacant apartment for pest control',
            '63' => 'Check/repair all plumbing fixtures',
            '64' => 'Change combination on mailbox   old code:----- new code:-----',
            //'55' => 'Clean apartment as needed',
            '53' => 'Check/repair furniture, drapes & blinds as needed',
        );

        // add tasks
        foreach($tasks as $job_description_id => $job_description)
        {
            $task = new MaintenanceTask();
            $task->job_description_id = intval($job_description_id);
            $task->description = $job_description;

            $w_o->Tasks->add($task);
        }

        return $w_o;
    }

    public function updateTimeSpent()
    {
        $time_spent = $this->time_spent;
        $running_total = 0;
        foreach($this->Tasks as $task)
        {
            $running_total += floatval($task->time_spent);
        }
        if ($time_spent != $running_total)
        {
            $this->time_spent = $running_total;
            $this->save();
        }
    }

    public function getProgress()
    {
        if (count($this->Tasks) > 0)
        {
            $total = count($this->Tasks);
            $completed = 0;
            foreach($this->Tasks as $task)
            {
                if ($task->status == self::STATUS_COMPLETED)
                    $completed++;
            }
            $progress['total'] = $total;
            $progress['completed'] = $completed;
            return $progress;
        }
        return false;
    }
}