<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceTask
 *
 * @Table(name="maintenance_task")
 * @Entity
 */
class MaintenanceTask extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->inventory_parts = new ArrayCollection;
        $this->workers = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="workorder_id", type="integer", nullable=true) */
    protected $workorder_id;

    /** @Column(name="job_description_id", type="integer", nullable=true) */
    protected $job_description_id;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="status", type="integer", nullable=true) */
    protected $status;

    /** @Column(name="comments", type="text", nullable=true) */
    protected $comments;

    /** @Column(name="time_started", type="integer", nullable=true) */
    protected $time_started;

    /** @Column(name="time_done", type="integer", nullable=true) */
    protected $time_done;

    /** @Column(name="time_spent", type="decimal", precision=18, scale=2, nullable=true) */
    protected $time_spent;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="MaintenanceWorkorder")
     * @JoinColumn(name="workorder_id", referencedColumnName="id")
     */
    protected $workorder;

    /**
     * @ManyToOne(targetEntity="MaintenanceJobDescription")
     * @JoinColumn(name="job_description_id", referencedColumnName="id")
     */
    protected $job_description;

    /**
     * @ManyToMany(targetEntity="MaintenanceInventoryPart", mappedBy="MaintenanceTask")
     * @JoinTable(name="maintenance_task_parts",
     *      joinColumns={@JoinColumn(name="maintenance_task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="inventory_part_id", referencedColumnName="id")}
     * )
     */
    protected $inventory_parts;

    /**
     * @ManyToMany(targetEntity="MaintenanceWorker")
     * @JoinTable(name="maintenance_task_workers",
     *      joinColumns={@JoinColumn(name="maintenance_task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="worker_id", referencedColumnName="id")}
     * )
     */
    protected $workers;
}