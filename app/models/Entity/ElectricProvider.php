<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * ElectricProvider
 *
 * @Table(name="electric_provider")
 * @Entity
 */
class ElectricProvider extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=32, nullable=true) */
    protected $name;

    /** @Column(name="cost", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}