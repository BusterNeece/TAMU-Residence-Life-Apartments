<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * QuizQuestion
 *
 * @Table(name="quiz_question")
 * @Entity
 */
class QuizQuestion extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->options = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_id", type="integer") */
    protected $quiz_id;

    /** @Column(name="question", type="string", length=255, nullable=true) */
    protected $question;

    /** @Column(name="correct_answer_id", type="integer", nullable=true) */
    protected $correct_answer_id;

    /**
     * @ManyToOne(targetEntity="Quiz")
     * @JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    protected $quiz;

    /** @OneToMany(targetEntity="QuizOption", mappedBy="question") */
    protected $options;

    /**
     * @ManyToOne(targetEntity="QuizOption")
     * @JoinColumn(name="correct_answer_id", referencedColumnName="id")
     */
    protected $correct_answer;
}