<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * QuizResult
 *
 * @Table(name="quiz_result")
 * @Entity
 */
class QuizResult extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="quiz_id", type="integer") */
    protected $quiz_id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="timestamp", type="integer", nullable=true) */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Quiz")
     * @JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    protected $quiz;
}