<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceBillableItem
 *
 * @Table(name="maintenance_billable_item")
 * @Entity
 */
class MaintenanceBillableItem extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=36, nullable=true) */
    protected $name;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

}