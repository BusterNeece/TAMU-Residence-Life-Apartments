<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Asset
 *
 * @Table()
 * @Entity
 */
class Asset extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->amenities = new ArrayCollection;
        $this->occupancies = new ArrayCollection;
        $this->costs = new ArrayCollection;

        $this->record_creation_date = time();
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="description", type="string", length=256, nullable=true) */
    protected $description;

    /** @Column(name="asset_status_id", type="integer", nullable=true) */
    protected $asset_status_id;

    /** @Column(name="asset_location_id", type="integer", nullable=true) */
    protected $asset_location_id;

    /** @Column(name="asset_type_id", type="integer", nullable=true) */
    protected $asset_type_id;

    /** @Column(name="asset_parent_id", type="integer", nullable=true) */
    protected $asset_parent_id;

    /** @Column(name="asset_virtual_flag", type="boolean", nullable=true) */
    protected $asset_virtual_flag;

    /** @Column(name="cost_override", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost_override;

    /** @Column(name="record_creation_date", type="integer", nullable=true) */
    protected $record_creation_date;

    /** @Column(name="asset_subcode", type="decimal", precision=18, scale=2, nullable=true) */
    protected $asset_subcode;

    /** @Column(name="meter_sort_order", type="decimal", precision=18, scale=2, nullable=true) */
    protected $meter_sort_order;

    /** @Column(name="electric_provider_id", type="integer", nullable=true) */
    protected $electric_provider_id;

    /** @Column(name="is_exempt_electrical", type="integer", nullable=true) */
    protected $is_exempt_electrical;

    /** @Column(name="mailbox_code", type="string", length=10, nullable=true) */
    protected $mailbox_code;

    /** @Column(name="phone_type", type="string", length=25, nullable=true) */
    protected $phone_type;

    /** @Column(name="phone_number", type="string", length=25, nullable=true) */
    protected $phone_number;

    /** @Column(name="num_bedrooms", type="integer", nullable=true) */
    protected $num_bedrooms;

    /** @Column(name="notes", type="text", nullable=true) */
    protected $notes;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="AssetStatus")
     * @JoinColumn(name="asset_status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ManyToOne(targetEntity="AssetLocation")
     * @JoinColumn(name="asset_location_id", referencedColumnName="id")
     */
    protected $location;

    /**
     * @ManyToOne(targetEntity="AssetType")
     * @JoinColumn(name="asset_type_id", referencedColumnName="id")
     */
    protected $type;

    /** @OneToMany(targetEntity="Occupancy", mappedBy="asset") */
    protected $occupancies;

    /** @OneToMany(targetEntity="AssetCost", mappedBy="asset") */
    protected $costs;

    /**
     * @ManyToOne(targetEntity="ElectricProvider")
     * @JoinColumn(name="electric_provider_id", referencedColumnName="id")
     */
    protected $electric_provider;

    /**
     * ManyToOne(targetEntity="Asset")
     * JoinColumn(name="asset_parent_id", referencedColumnName="id")
     * protected $parent;
     */

    /**
     * Static Functions
     */

    public static function fetchSelect($add_blank = FALSE, $is_recursive = TRUE, $recursive_include_parent = FALSE, $include_complex = FALSE, $flatten_at_second_level = TRUE)
    {
        $em = self::getEntityManager();
        $all_records_raw = $em->createQuery('SELECT a FROM Entity\Asset a LEFT JOIN a.status s WHERE s.name != :demo ORDER BY a.name ASC')
            ->setParameter('demo', 'Demolished')
            ->getArrayResult();
        
        $all_records = array();
        if ($is_recursive)
        {
            foreach($all_records_raw as $record)
            {
                $all_records[$record['asset_parent_id']][$record['id']] = $record['name'];
            }
            $all_records = self::_processSelectTree($all_records, 0, $recursive_include_parent);
            
            // Flatten necessary for selects (can't handle multilevel)
            if ($flatten_at_second_level)
            {
                foreach($all_records as &$complex)
                {
                    $complex = self::_flattenSelectTree($complex);
                }
            }
        }
        else
        {
            foreach($all_records_raw as $record)
            {
                $all_records[$record['id']] = $record['name'];
            }
        }
        
        if ($add_blank)
            $all_records = array('null' => 'N/A') + $all_records;
        
        return $all_records;
    }
    
    public static function _flattenSelectTree($list)
    {
        if (!is_array($list))
            return FALSE;
        
        $result = array();
        foreach($list as $key => $value)
        {
            if (is_array($value))
                $result += self::_flattenSelectTree($value);
            else
                $result[$key] = $value;
        }
        
        return $result;
    }
    
    public static function _processSelectTree($items, $parent_id, $include_parent_as_subitem = FALSE)
    {
        $return_array = array();
        
        $relevant_items = $items[$parent_id];
        
        natsort($relevant_items);
        
        foreach($relevant_items as $item_id => $item_name)
        {
            if (isset($items[$item_id]))
            {
                $return_array[$item_name] = array();
                
                if ($include_parent_as_subitem)
                {
                    $return_array[$item_name][$item_id] = $item_name.' (All)';
                }
                
                $return_array[$item_name] += self::_processSelectTree($items, $item_id, $include_parent_as_subitem);
            }
            else
            {
                $return_array[$item_id] = $item_name;
            }
        }
        
        return $return_array;
    }

    public static function fetchAllRentable()
    {
        static $all_records;
        if( !isset($all_records) )
        {
            $all_records = Doctrine_Query::create()
                ->from(__CLASS__.' a')
                ->leftJoin('a.AssetType at')
                ->leftJoin('a.AssetStatus as')
                ->leftJoin('a.Occupancies o')
                ->where('as.name = ?', 'Rentable')
                ->orderBy('a.name ASC')
                ->execute();
        }
        return $all_records;
    }
    
    public static function fetchAllOccupiedAssetIds($move_in_date)
    {
        $em = self::getEntityManager();
        $occupied = array();

        // Rented apartments.
        $rented_apts = $em->createQuery('SELECT o.asset_id FROM Entity\Occupancy o WHERE (o.end_date = 0 OR o.end_date > :time) AND o.occupancy_model_id = :model')
            ->setParameter('time', $move_in_date)
            ->setParameter('model', 3)
            ->getArrayResult();

        foreach($rented_apts as $r)
        {
            $occupied[] = intval($r['asset_id']);
        }

        // Offered apartments.
        $offered_apts = $em->createQuery('SELECT o.asset_id FROM Entity\Offer o LEFT JOIN o.asset_type t WHERE o.response_deadline > :time AND t.occupancy_model_id = :model')
            ->setParameter('time', $move_in_date)
            ->setParameter('model', 3)
            ->getArrayResult();

        foreach($offered_apts as $o)
        {
            $occupied[] = intval($o['asset_id']);
        }

        // Rented and offered rooms.
        $rented_rooms = $em->createQuery('SELECT o.asset_id, COUNT(o.id) AS occ_total, a.num_bedrooms FROM Entity\Occupancy o LEFT JOIN o.asset a WHERE (o.end_date = 0 OR o.end_date > :time) AND o.occupancy_model_id = :model GROUP BY o.asset_id, o.id, a.id, a.num_bedrooms')
            ->setParameter('time', $move_in_date)
            ->setParameter('model', 2)
            ->getArrayResult();

        $offered_rooms = $em->createQuery('SELECT o.asset_id, COUNT(o.id) AS offer_total FROM Entity\Offer o LEFT JOiN o.asset_type t WHERE o.response_deadline > :time AND t.occupancy_model_id = :model GROUP BY o.asset_id, o.id')
            ->setParameter('time', $move_in_date)
            ->setParameter('model', 2)
            ->getArrayResult();

        $offers = array();
        foreach($offered_rooms as $r)
        {
            $offers[$r['asset_id']] = $r['offer_total'];
        }
        foreach($rented_rooms as $r)
        {
            $offered = isset($offers[$r['asset_id']]) ? intval($offers[$r['asset_id']]) : 0;
            if (intval($r['occ_total']) + $offered >= intval($r['num_bedrooms']))
                $occupied[] = intval($r['asset_id']);
        }
        return $occupied;
    }

    public static function fetchWaitingListMatches(array $asset_type_ids, $move_in_date)
    {   
        $rented_ids = self::fetchAllOccupiedAssetIds($move_in_date);

        $all_records = Doctrine_Query::create()
            ->from(__CLASS__.' a')
            ->leftJoin('a.AssetStatus')
            ->leftJoin('a.AssetType')
            ->addWhere('a.asset_status_id = ?', 1)
            ->whereNotIn('a.id', $rented_ids)
            ->whereIn('a.AssetType.id', $asset_type_ids)
            ->orderBy('a.AssetType.id ASC, a.name ASC')
            ->fetchArray();
        return $all_records;
    }

    public static function fetchAllVacant($move_in_date)
    {
        $rented_ids = self::fetchAllOccupiedAssetIds($move_in_date);

        $em = self::getEntityManager();
        return $em->createQuery('SELECT a, s, t FROM '.__CLASS__.' a LEFT JOIN a.status s LEFT JOIN a.type t WHERE a.asset_status_id = :status AND a.id NOT IN (:ids) ORDER BY t.name')
            ->setParameter('status', 1)
            ->setParameter('ids', $rented_ids)
            ->execute();
    }

    public static function fetchVacancyByRange($start, $end)
    {
        $em = self::getEntityManager();

        // Assemble days array.
        $days = array();
        for($i = $start; $i <= $end; $i += 86400)
            $days[$i] = $i;

        // Fetch all rentable apartments.
        $all_apartments = $em->createQuery('SELECT a FROM '.__CLASS__.' a WHERE a.asset_status_id = :status')
            ->setParameter('status', 1)
            ->getArrayResult();
        
        $vacant_apt_days = array();
        $vacant_apt_ids = array();

        foreach($all_apartments as $apt)
        {
            $occupancies_in_range = $em->createQuery('SELECT o FROM Entity\Occupancy o LEFT JOIN o.resident r WHERE o.asset_id = :asset_id AND o.start_date <= :end AND (o.end_date IS NULL OR o.end_date = 0 OR o.end_date >= :start)')
                ->setParameter('asset_id', $apt['id'])
                ->setParameter('start', (int)$start)
                ->setParameter('end', (int)$end)
                ->getArrayResult();
            
            if (count($occupancies_in_range) > 0)
            {
                $days_vacant = 0;

                foreach($days as $i)
                {
                    $occupants_for_day = 0;
                    foreach($occupancies_in_range as $occupant)
                    {
                        if ($occupant['start_date'] <= $i && ($occupant['end_date'] >= $i-50000 || !(int)$occupant['end_date']))
                            $occupants_for_day++;
                    }

                    if ($occupants_for_day == 0)
                        $days_vacant++;
                }
            }
            else
            {
                $days_vacant = count($days);
            }

            if ($days_vacant > 0)
            {
                $vacant_apt_days[$apt['id']] = $days_vacant;
                $vacant_apt_ids[] = $apt['id'];
            }
        }

        $vacancies = array();

        $vacant_apts = $em->createQuery('SELECT a FROM '.__CLASS__.' a WHERE a.id IN (:ids) ORDER BY a.name')
            ->setParameter('ids', $vacant_apt_ids)
            ->execute();
        
        $totals = array(
            'cost' => 0,
            'days' => 0,
        );
        
        if (count($vacant_apts) > 0)
        {
            foreach($vacant_apts as $apt)
            {
                $days_vacant = $vacant_apt_days[$apt['id']];
                
                $cost = $apt->getCost($start);
                $cost_per_day = $cost / 30;
                $vacant_cost = $cost_per_day * $days_vacant;

                $cost = round($vacant_cost, 3);
                $cost = \DF\Utilities::ceiling($cost, 2);

                if ($cost > 0)
                {
                    $totals['cost'] += $cost;

                    $vacancies[] = array(
                        'asset' => $apt,
                        'days' => $days_vacant,
                        'cost' => $cost,
                    );
                }
            }
        }

        $totals['days'] = \DF\Utilities::ceiling($totals['cost'] / count($days));

        return array(
            'totals' => $totals,
            'vacancies' => $vacancies,
        );
    }

    public static function fetchAllVacantByMonth()
    {
        $vacant = Asset::fetchAllVacant(strtotime('+6 month'));
        $all_records = array();
        foreach($vacant as $asset)
        {
            $available_date = $asset->getAvailableDate();
            if ($available_date === 'now')
                $month = 'now';
            else
                $month = date('Y-m',strtotime($available_date));
            $all_records[$month][] = $asset;
        }
        return $all_records;
    }
    
    /**
     * Individual record functions
     */

    public function getOccupancyModel()
    {
        $model = $this->AssetType->OccupancyModel;

        if ($model instanceof OccupancyModel && $model->exists())
            return strtolower($model->name);
        else
            return 'apartment';
    }

    public function getChildren()
    {
        return Doctrine_Query::create()
            ->from(__CLASS__.' a')
            ->addWhere('asset_parent_id = ?', (int)$this->id)
            ->execute();
    }
    
    public function getCost($timestamp = NULL, $period = "monthly")
    {
        $timestamp = (is_null($timestamp)) ? time() : (int)$timestamp;
        
        if ($this->cost_override != 0)
        {
            return (float)$this->cost_override;
        }
        else
        {
            // Check for individual cost records for this item.
            if (count($this->costs) > 0)
            {
                foreach($this->costs as $cost)
                {
                    if ($cost->period == $period && $cost->start_date <= $timestamp && ($cost->end_date >= $timestamp || !(int)$cost->end_date))
                    {
                        return (float)$cost->cost;
                    }
                }
            }
            
            // Check the asset type for cost records.
            if (count($this->type->costs) > 0)
            {
                foreach($this->type->costs as $cost)
                {
                    if ($cost->period == $period && $cost->start_date <= $timestamp && ($cost->end_date >= $timestamp || !(int)$cost->end_date))
                    {
                        return (float)$cost->cost;
                    }
                }
            }
        }
        
        return 0.0;
    }
    
    public function getCostHistory()
    {
        // Pull all costs for all months.
        $all_times = array();
        
        for ($i = date('m')-12; $i <= date('m')+12; $i++)
        {
            $timestamp = mktime(0, 0, 0, $i, 15, date('Y'));
            $cost = $this->getCost($timestamp);
            
            $all_times[] = array(
                'timestamp'     => $timestamp,
                'cost'          => $cost,
            );
        }
        
        $changes = array();
        
        // Isolate times when the price changed.
        foreach($all_times as $index => $time_info)
        {
            if ($index == 0 || $all_times[$index-1]['cost'] != $time_info['cost'])
            {
                // Update the current last item.
                if (count($changes) > 0)
                {
                    $changes[count($changes)-1]['end_timestamp'] = $all_times[$index-1]['timestamp'];
                }
                
                $changes[] = array(
                    'start_timestamp'   => $time_info['timestamp'],
                    'end_timestamp'     => 0,
                    'cost'              => $time_info['cost'],
                );
            }
        }
        $changes[count($changes)-1]['end_timestamp'] = $all_times[count($all_times)-1]['timestamp'];
        
        // Provide clean duration text.
        foreach($changes as &$change)
        {
            $change['range_text'] = date('F Y', $change['start_timestamp']).' to '.date('F Y', $change['end_timestamp']);
        }
        
        return $changes;
    }

    public function getAllResidents()
    {
        $residents = array();
        foreach($this->Occupancies as $occupancy)
        {
            if ($occupancy->isActive())
            {
                $resident = Resident::getById($occupancy->resident_id);
                if ($resident->id)
                {
                    $residents[] = $resident;
                    foreach($resident->getFamilyMembers() as $family)
                    {
                        $residents[] = $family;
                    }
                }
            }
        }
        return $residents;
    }

    public function getPrincipalResidents()
    {
        $residents = array();
        foreach($this->occupancies as $occupancy)
        {
            if ($occupancy->isVisible())
            {
                if ($occupancy->resident->principal_resident)
                    $residents[] = $occupancy->resident;
            }
        }
        return $residents;
    }

    public function getAvailableDate()
    {
        $dates = array();
        foreach($this->occupancies as $occupancy)
        {
            $dates[$occupancy->end_date] = date('Y-m-d',$occupancy->end_date);
        }
        if (count($dates)>0)
        {
            asort($dates);
            $last_date = array_pop($dates);
            if (strtotime($last_date) > strtotime('today'))
            {
                return $last_date;
            }
            else
            {
                return 'now';
            }
        }
        else
        {
            return 'now';
        }
    }
}