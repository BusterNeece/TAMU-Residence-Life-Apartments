<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Touchnet
 *
 * @Table()
 * @Entity
 */
class Touchnet extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $transaction_id
     *
     * @Column(name="transaction_id", type="string", length=200, nullable=true)
     */
    protected $transaction_id;

    /**
     * @var string $local_record_id
     *
     * @Column(name="local_record_id", type="string", length=100, nullable=true)
     */
    protected $local_record_id;

    /**
     * @var integer $time_created
     *
     * @Column(name="time_created", type="integer", nullable=true)
     */
    protected $time_created;

    /**
     * @var integer $time_updated
     *
     * @Column(name="time_updated", type="integer", nullable=true)
     */
    protected $time_updated;

    /**
     * @var string $payment_status
     *
     * @Column(name="payment_status", type="string", length=200, nullable=true)
     */
    protected $payment_status;

    /**
     * @var decimal $payment_amount
     *
     * @Column(name="payment_amount", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $payment_amount;

    /**
     * @var string $payment_card_type
     *
     * @Column(name="payment_card_type", type="string", length=200, nullable=true)
     */
    protected $payment_card_type;

    /**
     * @var string $payment_name
     *
     * @Column(name="payment_name", type="string", length=255, nullable=true)
     */
    protected $payment_name;

    /**
     * @var integer $payment_order_id
     *
     * @Column(name="payment_order_id", type="integer", nullable=true)
     */
    protected $payment_order_id;

    /**
     * @var string $payment_internal_trans_id
     *
     * @Column(name="payment_internal_trans_id", type="string", length=50, nullable=true)
     */
    protected $payment_internal_trans_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ManyToMany(targetEntity="Split", mappedBy="Touchnet")
     */
    protected $Splits;

    public function __construct()
    {
        $this->Splits = new ArrayCollection;
    }
    
}