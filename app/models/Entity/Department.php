<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Department
 *
 * @Table()
 * @Entity
 */
class Department extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="code", type="string", length=10, nullable=true) */
    protected $code;

    /** @Column(name="college_code", type="string", length=10, nullable=true) */
    protected $college_code;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @Column(name="fax", type="string", length=64, nullable=true) */
    protected $fax;

    /** @Column(name="mailstop", type="string", length=8, nullable=true) */
    protected $mailstop;

    /** @Column(name="telephone", type="string", length=64, nullable=true) */
    protected $telephone;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

}