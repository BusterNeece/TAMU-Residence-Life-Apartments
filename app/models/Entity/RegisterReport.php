<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * RegisterReport
 *
 * @Table(name="register_report")
 * @Entity
 */
class RegisterReport extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $register_id
     *
     * @Column(name="register_id", type="integer", nullable=true)
     */
    protected $register_id;

    /**
     * @var string $register_batch
     *
     * @Column(name="register_batch", type="string", length=1, nullable=true)
     */
    protected $register_batch;

    /**
     * @var integer $timestamp
     *
     * @Column(name="timestamp", type="integer", nullable=true)
     */
    protected $timestamp;

    /**
     * @var decimal $amount_cash
     *
     * @Column(name="amount_cash", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $amount_cash;

    /**
     * @var decimal $amount_check
     *
     * @Column(name="amount_check", type="decimal", precision=18, scale=2, nullable=true)
     */
    protected $amount_check;

    /**
     * @var string $notes
     *
     * @Column(name="notes", type="text", nullable=true)
     */
    protected $notes;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}