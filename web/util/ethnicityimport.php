<?php
/**
 * Synchronization file
 */

require_once dirname(__FILE__).'/bootstrap.php';

$application->bootstrap();

// Get all occupants.
$all_assets = Asset::fetchAll();
$occupants_to_process = array();

foreach($all_assets as $asset)
{
	$occupants = $asset->getPrincipalResidents();
	if (count($occupants) > 0)
	{
		foreach($occupants as $occupant)
		{
			if ($occupant->principal_resident)
			{
				$occupants_to_process[$occupant->uin] = $occupant;
			}
		}
	}
}

$uins = array_keys($occupants_to_process);

$compass = new DF_Service_Compass();
$student_records_raw = $compass->getStudentsById($uins);
$student_records = array();
foreach((array)$student_records_raw as $record)
{
	$student_records[$record['uin']] = $record;
}

foreach($occupants_to_process as $uin => $occupant)
{
	if (isset($student_records[$uin]))
	{
		$student_record = $student_records[$uin];
		
		if ($student_record['primary_ethnicity'])
		{
			$occupant->ethnicity = $student_record['primary_ethnicity'];
			$occupant->save();
		}
	}
}

echo 'Done';


DF_Utilities::print_r($student_records);

exit;