<?php
/**
 * TouchNet postback processing file.
 */

require_once dirname(__FILE__).'/bootstrap.php';

$application->bootstrap();

file_put_contents(DF_INCLUDE_TEMP.'/TouchnetPostback.txt', print_r($_POST, TRUE));

$transaction_record = DF_Service_TouchNet::processPostData();

// Transaction record only returned in the case of successful transactions.
if ($transaction_record)
{
    $resident_id = $transaction_record->local_record_id;
    $resident = Resident::fetchById($resident_id);
    
    // Post a new transaction marking the online payment.
    $transaction = new RegisterTransaction();
    $transaction->posted = time();
    $transaction->register_id = 0; // Internet register
    $transaction->date = time();
    $transaction->memo = 'Online Payment';
    $transaction->touchnet_id = $transaction_record->id;
    $transaction->save();
	
    $transaction_id = $transaction->id;
	
	$deposit_item = Item::fetchByName('Security Deposit');
    
    foreach($transaction_record->Splits as $original_split)
    {
        // Post new split for this resident.
        $split = new Split();
        $split->posted = time();
		
		if ($original_split->asset_id)
			$split->asset_id = $original_split->asset_id;
		
        $split->transaction_id = (int)$transaction_id;
        $split->resident_id = (int)$resident->id;
		
		if ($original_split->item_id == $deposit_item->id)
			$split->ledger_id = Split::LEDGER_DEPOSITS;
		else
			$split->ledger_id = (int)$original_split->ledger_id;
		
        $split->credit_or_debit = 'C';
        $split->item_id = (int)$original_split->item_id;
        $split->matching_split_id = (int)$original_split->id;
        $split->split_amount = (float)(0-$original_split->split_amount);
        $split->save();
        
        // Update the original split to list its matching split id.
        $new_split_id = $split->id;
        $original_split->matching_split_id = $new_split_id;
        $original_split->save();
		
        if ($original_split->item_id == $deposit_item->id)
            $original_split->delete();
    }

    // Special processing for application fees and deposits.
    if ($transaction_record->payment_status === 'success')
    {
        foreach($transaction_record->Splits as $TNsplit)
        {
            if (($TNsplit->credit_or_debit === 'D') && ($TNsplit->Transaction->memo === 'Application Fee'))
            {
                foreach($transaction_record->Resident->Applications as $app)
                {
                    if ($app->Status->id == 1) // status 1 = Waiting
                    {
                        $fee = new ApplicationFee();
                        $fee->fee_amount = $TNsplit->split_amount;
                        $fee->fee_date = DF_TIME;
                        $fee->fee_receipt = $ext_trans_id;
                        $fee->application_id = $app->id;
                        $fee->save();
                        $app->application_fee_id = $fee->id;
                        $app->save();
                    }
                }
            }
            else if (($TNsplit->credit_or_debit === 'D') && ($TNsplit->Transaction->memo === 'Security Deposit'))
            {
                foreach($transaction_record->Resident->Applications as $app)
                {
                    if ($app->Status->id == 3) // status 3 = DEPOSIT
                    {
                        $occupancy = Occupancy::fetchByApplicationId($app->id);
                        $occupancy->deposit = $TNsplit->split_amount;
                        $occupancy->deposit_date = DF_TIME;
                        $occupancy->deposit_reference_number = $ext_trans_id;
                        $occupancy->save();
                        $app->application_status_id = 4; // status 4 = ASSIGNED
                        $app->save();
                    }
                }
            }
        }
    }

    // Update user balances.
    Split::updateRunningBalance($resident->id, Split::LEDGER_STANDARD);
    Split::updateRunningBalance($resident->id, Split::LEDGER_DEPOSITS);

    // Send e-mail confirmation of receipt.
    DF_Messenger::send(array(
        'to'        => array($resident->email, $resident->preferred_email),
        'subject'   => 'Payment Received',
        'template'  => 'billing/received',
        'vars'      => array(
            'transaction' => $transaction_record,
        ),
    ));
}